/* ---------------------------------------------------------------- */
/*																	*/
/*							エフェクト処理							*/
/*																	*/
/* ---------------------------------------------------------------- */

#include "Common.h"

/* ---------------------------------------- */
/*											*/
/*				インストラクタ				*/
/*											*/
/* ---------------------------------------- */
FireStrong::FireStrong()
{
	printf( "強被弾作成された\n" ) ;
	pos = VGet( 0.0f, 0.0f, 0.0f ) ;	// --- 座標
	AnimNowTime[PLAYER1_EFFECT] = 0.0f ;
	AnimNowTime[PLAYER2_EFFECT] = 0.0f ;
	spd.x = 0.0f ;
	angle = 180.0f ;
	AlphValue = 0.5f ;
	gc_Fire_Strong[PLAYER1_EFFECT].strong_flg = FALSE ;
	gc_Fire_Strong[PLAYER2_EFFECT].strong_flg = FALSE ;

}

/* ---------------------------------------- */
/*											*/
/*				デストラクタ				*/
/*											*/
/* ---------------------------------------- */
FireStrong::~FireStrong()
{
}

/* ---------------------------------------- */
/*											*/
/*				初期化処理					*/
/*											*/
/* ---------------------------------------- */
int FireStrong::EffectInit()
{
	// --- 強被弾エフェクトを読み込み格納(プレイヤー１)
	anim_firestrong[PLAYER1_EFFECT] = MV1LoadModel( PATH_FIRE_STRONG_ANIM ) ;
	AnimAttachIndex[PLAYER1_EFFECT] = MV1AttachAnim( gc_Fire_Strong[PLAYER1_EFFECT].GetModelHandle(),0,anim_firestrong[PLAYER1_EFFECT]) ;
	AnimTotalTime[PLAYER1_EFFECT] = MV1GetAttachAnimTotalTime(gc_Fire_Strong[PLAYER1_EFFECT].GetModelHandle(),AnimAttachIndex[PLAYER1_EFFECT]) ;

	// --- 強被弾エフェクトを読み込み格納(プレイヤー２)
	anim_firestrong[PLAYER2_EFFECT] = MV1LoadModel( PATH_FIRE_STRONG_ANIM ) ;
	AnimAttachIndex[PLAYER2_EFFECT] = MV1AttachAnim( gc_Fire_Strong[PLAYER2_EFFECT].GetModelHandle(),0,anim_firestrong[PLAYER2_EFFECT]) ;
	AnimTotalTime[PLAYER2_EFFECT] = MV1GetAttachAnimTotalTime(gc_Fire_Strong[PLAYER2_EFFECT].GetModelHandle(),AnimAttachIndex[PLAYER2_EFFECT]) ;

	FrameIndex[PLAYER1_EFFECT] = MV1SearchFrame( gc_Fire_Strong[PLAYER1_EFFECT].GetModelHandle(), "root" ) ;
	MV1SetFrameUserLocalMatrix(gc_Fire_Strong[PLAYER1_EFFECT].GetModelHandle(),FrameIndex[PLAYER1_EFFECT],MGetIdent()) ;

	return 0 ;

}

	
/* ---------------------------------------- */
/*											*/
/*				モデル表示					*/
/*											*/
/* ---------------------------------------- */
void FireStrong::SetModel( TCHAR Epath[] )
{
	hEModel = MV1LoadModel( Epath ) ;
}


/* ---------------------------------------- */
/*											*/
/*				エフェクト処理				*/
/*											*/
/* ---------------------------------------- */
void FireStrong::HitFire()
{
	// --- プレイヤー２から出現
	if ( gc_Fire_Strong[PLAYER2_EFFECT].strong_flg == TRUE )
	{
		// --- アニメーションが再生し終わったら、モデルを消す
		MV1SetVisible( gc_Fire_Strong[PLAYER2_EFFECT].GetModelHandle() , TRUE ) ;
		AnimNowTime[PLAYER2_EFFECT] += 1.5f ;
		if ( AnimNowTime[PLAYER2_EFFECT] > AnimTotalTime[PLAYER2_EFFECT] )
		{
			// --- アニメーションを再生し終わるまで描画
			MV1SetVisible( gc_Fire_Strong[PLAYER2_EFFECT].GetModelHandle() , FALSE ) ;
			// --- ブーストゲージがある時、ゲージを減らしアニメーションも最初に戻す
			AnimNowTime[PLAYER2_EFFECT] = 0.0f ;
			gc_Fire_Strong[PLAYER2_EFFECT].strong_flg = FALSE ;
		}
	}

	// --- プレイヤー１から出現
	if (gc_Fire_Strong[PLAYER1_EFFECT].strong_flg == TRUE )
	{
		// --- アニメーションが再生し終わったら、モデルを消す
		MV1SetVisible( gc_Fire_Strong[PLAYER1_EFFECT].GetModelHandle() , TRUE ) ;
		AnimNowTime[PLAYER1_EFFECT] += 1.5f ;
		if ( AnimNowTime[PLAYER1_EFFECT] > AnimTotalTime[PLAYER1_EFFECT] )
		{
			// --- ブーストゲージがある時、ゲージを減らしアニメーションも最初に戻す
			AnimNowTime[PLAYER1_EFFECT] = 0.0f ;
			gc_Fire_Strong[PLAYER1_EFFECT].strong_flg = FALSE ;
		}
	}

	MV1SetAttachAnimTime( gc_Fire_Strong[PLAYER1_EFFECT].GetModelHandle() , AnimAttachIndex[PLAYER1_EFFECT] , AnimNowTime[PLAYER1_EFFECT] ) ;
	MV1SetAttachAnimTime( gc_Fire_Strong[PLAYER2_EFFECT].GetModelHandle() , AnimAttachIndex[PLAYER2_EFFECT] , AnimNowTime[PLAYER2_EFFECT] ) ;

}


int FireStrong::UpdatePosDir( int Eno )
{
	// --- サイズ調整
	MV1SetScale( hEModel , VGet(0.04f,0.02f,0.04f)) ;		// --- 強フライドポテト

	// --- プレイヤー１の時
	if ( Eno == PLAYER_1 )
	{
		if ( (gc_Player[PLAYER_2].GetDirection() == D_DOWN) && (gc_Player[PLAYER_2].GetDirinfo() == D_RIGHT) )
		{
			gc_Fire_Strong[PLAYER1_EFFECT].pos = gc_Player[PLAYER_1].GetPosition() ;
			MV1SetRotationXYZ( gc_Fire_Strong[PLAYER1_EFFECT].GetModelHandle() , VGet( 0.0f , 1.57f * 2 , 0.0f ) ) ;
			pos.x -= 50 ;
			pos.y += 80 ;
			pos.z -= 70 ;
			MV1SetPosition( gc_Fire_Strong[PLAYER1_EFFECT].GetModelHandle(), pos ) ;
		}

		if ( (gc_Player[PLAYER_2].GetDirection() == D_DOWN) && (gc_Player[PLAYER_2].GetDirinfo() == D_LEFT) )
		{
			gc_Fire_Strong[PLAYER1_EFFECT].pos = gc_Player[PLAYER_1].GetPosition() ;
			MV1SetRotationXYZ( gc_Fire_Strong[PLAYER1_EFFECT].GetModelHandle() , VGet( 0.0f , 1.57f * 0 , 0.0f ) ) ;
			pos.x += 50 ;
			pos.y += 80 ;
			pos.z -= 70 ;
			MV1SetPosition( gc_Fire_Strong[PLAYER1_EFFECT].GetModelHandle(), pos ) ;
		}
	}

	// --- プレイヤー２の時
	if ( Eno == PLAYER_2 )
	{
		if ( (gc_Player[PLAYER_1].GetDirection() == D_DOWN) && (gc_Player[PLAYER_1].GetDirinfo() == D_RIGHT) )
		{
			gc_Fire_Strong[PLAYER2_EFFECT].pos = gc_Player[PLAYER_2].GetPosition() ;
			MV1SetRotationXYZ( gc_Fire_Strong[PLAYER2_EFFECT].GetModelHandle() , VGet( 0.0f , 1.57f * 2 , 0.0f ) ) ;
			pos.x -= 50 ;
			pos.y += 80 ;
			pos.z -= 70 ;
			MV1SetPosition( gc_Fire_Strong[PLAYER2_EFFECT].GetModelHandle(), pos ) ;
		}
		if ( (gc_Player[PLAYER_1].GetDirection() == D_DOWN) && (gc_Player[PLAYER_1].GetDirinfo() == D_LEFT) )
		{
			gc_Fire_Strong[PLAYER2_EFFECT].pos = gc_Player[PLAYER_2].GetPosition() ;
			MV1SetRotationXYZ( gc_Fire_Strong[PLAYER2_EFFECT].GetModelHandle() , VGet( 0.0f , 1.57f * 0 , 0.0f ) ) ;
			pos.x += 50 ;
			pos.y += 80 ;
			pos.z -= 70 ;
			MV1SetPosition( gc_Fire_Strong[PLAYER2_EFFECT].GetModelHandle(), pos ) ;
		}
	}

	if ( gc_Fire_Strong[PLAYER2_EFFECT].strong_flg == FALSE )
	{
		// --- アニメーションを再生し終わるまで描画
		MV1SetVisible( gc_Fire_Strong[PLAYER1_EFFECT].GetModelHandle() , FALSE ) ;
	}

	if ( gc_Fire_Strong[PLAYER1_EFFECT].strong_flg == FALSE )
	{
		// --- アニメーションを再生し終わるまで描画
		MV1SetVisible( gc_Fire_Strong[PLAYER2_EFFECT].GetModelHandle() , FALSE ) ;
	}


	return( TRUE ) ;
}

// --- モデルハンドルと位置の取得
int FireStrong::GetModelHandle( void ){ return hEModel ; }



