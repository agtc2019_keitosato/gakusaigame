/* ============================================================ +/ 

	LastEdit : ----/--/--		NAME:
	LastEdit : ----/--/--		NAME:
	LastEdit : 2020/09/23		NAME:宮崎櫻
	___________________________________________________________

		Object.cpp
	___________________________________________________________

		オブジェクトクラスの実装部

/+ ============================================================ */
#include <Windows.h>
#include <DxLib.h>

#include "Common.h"

/* ------------------------------------------------------------ +/

	コンストラクタ
 + ------------------------------------------------------------ +
		-引数-
			void
		-返り値-
			void
/+ ------------------------------------------------------------ */
Object::Object()
{
	hModel = 0 ;
	PosRot = 0.0f ;
	pos = VGet( 0.0f, -20.0f, 0.0f ) ;
}

/* ------------------------------------------------------------ +/

	デスクトラクタ
 + ------------------------------------------------------------ +
		-引数-
			void
		-返り値-
			void
/+ ------------------------------------------------------------ */
Object::~Object()
{}

/* ------------------------------------------------------------ +/

	ステージハンドルセット＆座標更新
 + ------------------------------------------------------------ +
		-引数-
			TCHAR[]	モデルファイルパス
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Object::SetModelHandle( TCHAR mpath[] )
{
	hModel = MV1LoadModel( mpath ) ;
	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	座標セット
 + ------------------------------------------------------------ +
		-引数-
			VECTOR プレイヤー座標
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Object::SetPosition( VECTOR ps )
{
	pos = ps ;
	MV1SetPosition( hModel, pos ) ;
	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	回転座標セット
 + ------------------------------------------------------------ +
		-引数-
			VECTOR プレイヤー座標
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Object::SetRotPosition( VECTOR Pos )
{
	PosRot += 0.05f ;
	if ( PosRot >= 360.0f )
	{
		PosRot = 0.0f ;
	}

	Pos.y = 0.0f ;
	MV1SetPosition( hModel, Pos ) ;
	MV1SetRotationXYZ( hModel, VGet(0.0f, PosRot, 0.0f) ) ;

	return( TRUE ) ;
}



/* ------------------------------------------------------------ +/

	オブジェクトの大きさセット
 + ------------------------------------------------------------ +
		-引数-
			VECTOR プレイヤー座標
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Object::SetModelScale( VECTOR ps )
{
	MV1SetScale( hModel, ps ) ;
	return( TRUE ) ;
}

int Object::CheckHit()
{
	if( HitCheck_Capsule_Capsule(pos,pos,
		(float)(ITEM_WIDTH / 2),
		VAdd(gc_Player[PLAYER_1].GetPosition() , gc_Player[PLAYER_1].GetSpeed()),
		VAdd(gc_Player[PLAYER_1].GetPosition() , gc_Player[PLAYER_1].GetSpeed()),
		(float)(gc_Player[PLAYER_1].GetModelWidth() / 2)) == TRUE)
	{
		hitcheckflg = 0x01 ;
	}

	if( HitCheck_Capsule_Capsule(pos,pos,
		(float)(ITEM_WIDTH / 2),
		VAdd(gc_Player[PLAYER_2].GetPosition() , gc_Player[PLAYER_2].GetSpeed()),
		VAdd(gc_Player[PLAYER_2].GetPosition() , gc_Player[PLAYER_2].GetSpeed()),
		(float)(gc_Player[PLAYER_2].GetModelWidth() / 2)) == TRUE)
	{
		hitcheckflg = 0x02 ;
	}

	return( hitcheckflg ) ;
}
/* ============================================================ +/
/+																+/
/+	ゲッター													+/
/+																+/
/+ ============================================================ */
// --- オブジェクトモデルハンドルの取得
int Object::GetModelHandle(){	return( hModel ) ;	}

VECTOR Object::GetPosition(){	return( pos ) ;		}


/*- [EOF] -*/
