/* ============================================================ +/ 

	LastEdit : ----/--/--		NAME:
	LastEdit : ----/--/--		NAME:
	LastEdit : 2020/09/23		NAME:宮崎櫻
	___________________________________________________________

		Object.h
	___________________________________________________________

		オブジェクトの表示管理クラス

/+ ============================================================ */

class Object{
	public :
		Object() ;								// --- コンストラクタ
		~Object() ;								// --- デストラクタ
		int SetModelHandle( TCHAR [] ) ;		// --- ステージハンドルセット＆座標更新
		int SetPosition( VECTOR ) ;				// --- ポジションセット
		int SetRotPosition( VECTOR ) ;			// --- 回転する＆ポジションセット
		int SetModelScale( VECTOR ) ;			// --- スケールセット
		int CheckHit() ;

		int GetModelHandle() ;					// --- ステージハンドル取得
		VECTOR GetPosition() ;					// --- ポジション取得
	private :
		int hModel ;							// --- ステージモデルのハンドル
		VECTOR pos ;							// --- ステージの座標
		float PosRot ;							// --- モデルの回転＆座標
		int hitcheckflg ;						// --- 
} ;

/*- [EOF] -*/
