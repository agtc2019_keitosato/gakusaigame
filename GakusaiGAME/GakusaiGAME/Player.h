/* ============================================================ +/ 

	LastEdit : 2020/09/25		NAME:佐藤圭桃
	LastEdit : ----/--/--		NAME:
	LastEdit : 2020/10/16		NAME:
	___________________________________________________________

		Player.h
	___________________________________________________________

		プレイヤーのシーン別操作管理クラス

/+ ============================================================ */

class Player {
	public :
		Player( ) ;		// --- コンストラクタ
		~Player( ) ;	// --- デスクトラクタ

		int SelectPlayer	( BOOL ) ;			// --- プレイヤー番号セット
		int SetModelHandle	( TCHAR [], TCHAR [] ) ;		// --- モデルハンドルセット
		int SetModelWidth	( float ) ;			// --- モデル横幅セット
		int SetModelNumber	( int ) ;			// --- モデルナンバーセット
		int SetPosition		( VECTOR, VECTOR ) ;// --- 座標セット
		int SetKillcount	( void ) ;			// --- キル数セット
		int SetHpDecrease	( void ) ;			// --- ＨＰ減少セット
		int SetStamDecrease	( void ) ;			// --- スタミナ増加セット
		int SetChargeCnt	( int ) ;			// --- 溜めカウント
		int SetStatus		( int ) ;			// --- 状態セット
		int SetDirinfo		( int ) ;			// --- 向き情報セット
		int SetGdflg		( BOOL ) ;			// ---
		int MainAction		( void ) ;			// --- アクション
		int UpdatePosDir	( void ) ;			// --- 座標、向き更新

		int GetModelHandle	( void ) ;			// --- モデルハンドル取得
		int GetModelNumber	( void ) ;			// --- モデルナンバー取得
		float GetModelWidth	( void ) ;			// --- モデル横幅取得
		VECTOR GetPosition	( void ) ;			// --- 座標取得
		VECTOR GetSpeed		( void ) ;			// --- 速度取得
		int GetPlayerStatus	( void ) ;			// --- ステータス取得
		int GetPlayerHP		( void ) ;			// --- HP取得
		float GetPlayerStamina( void ) ;		// --- スタミナ取得
		int GetPlayerKillcnt( void ) ;			// --- キル数取得
		int GetChargeCnt	( void ) ;			// --- 溜めカウント取得
		BOOL GetHitFlg		( void ) ;			// --- ヒット判定フラグ取得
		int GetDirinfo		( void ) ;
		int GetDirection	( void ) ;
		int GetLaneNo		( void ) ;
		BOOL GetInvinFlg	( void ) ;

		int ActStay			( void ) ;			// --- アクション　待機
		int ActBeforeAttack	( void ) ;			// --- アクション　攻撃前
		int ActFightened	( void ) ;			// --- アクション　怯み
		int ActKnockBack	( BOOL ) ;			// --- アクション　ノックバック
		int ActDown			( void ) ;			// --- アクション　ダウン開始
		int ActRun			( void ) ;			// --- アクション　走り
		int ActJump			( void ) ;			// --- アクション　ジャンプ
		int ActLaneChange	( int ) ;			// --- アクション　レーン切り替え
		int ActBst			( void ) ;			// --- アクション　ブースト
		int ActGuard		( void ) ;			// --- アクション　ガード
		int ActHissatsu1	( void ) ;			// --- アクション　小必殺
		int ActHissatsu2	( void ) ;			// --- アクション　大必殺

		int CheckKeyAction	( void ) ;			// --- キー情報別アクション
		int CheckHit		( void ) ;			// --- ヒットチェック
		int HpReset			( void ) ;			// --- ＨＰチェック
		int StaminaReset	( void ) ;			// --- ＨＰチェック


	private :
		int		hModel ;		// --- モデルハンドル
		int		hGModel ;		// --- モデルハンドル
		int		ModelNo ;		// --- モデルナンバー
		float	ModelWidth ;	// --- モデル横幅
		VECTOR	pos, tmp_pos ;	// --- 座標
		VECTOR	spd, tmp_spd ;	// --- 速度
		int		direction ;		// --- 向き
		int		dirinfo ;		// --- 向き(キャラを回転させたくないとき用)
		BOOL	playno ;		// --- プレイヤー番号
		int		statusflg ;		// --- 状態フラグ
		int		laneflg ;		// --- レーンフラグ
		int		laneno ;		// --- 現在レーン番号
		int		chargecnt ;		// --- 溜め時間
		int		atktype ;		// --- 攻撃タイプ
		BOOL	inputflg ;		// --- 入力可能フラグ
		BOOL	waitflg ;		// --- キー入力一時待ちフラグ
		BOOL	hitflg ;		// --- ヒットチェックフラグ
		int		hp ;			// --- ＨＰ
		float	stamina ;		// --- スタミナ
		int		killcount ;		// --- キル数
		BOOL	gdflg ;			// --- 大必殺フラグ
		BOOL	invinflg ;		// --- 無敵フラグ
		float	invintime ;		// --- 無敵時間
		int		gbreakflg ;		// --- ガードブレイクフラグ
		int		downflg ;		// --- 蓄積ダウン
		float	down_resettim ;	// --- 蓄積ダウンリセット時間
		float	down_maxtim ;	// --- 最大ダウン時間
} ;

/*- [EOF] -*/
