/* ============================================================ +/ 

	LastEdit : 2020/09/18		NAME:佐藤圭桃
	LastEdit : ----/--/--		NAME:
	LastEdit : ----/--/--		NAME:
	___________________________________________________________

		Common.cpp
	___________________________________________________________

		汎用性のある変数の管理・関数の処理

/+ ============================================================ */
#include "Common.h"

/* ============================================================ +/
/+																+/
/+	グローバル変数　＆　オブジェクトの実体宣言					+/
/+																+/
/+ ============================================================ */
// --- オブジェクト
Player			gc_Player[MAX_PLAYER] ;					// --- プレイヤーオブジェクト
Object			gf_Stage ;								// --- ステージオブジェクト
Object			gf_Undersig[MAX_PLAYER] ;				// --- 足元オブジェクト
Object			gc_Item ;								// --- アイテムオブジェクト
Object			gc_winlose[MAX_PLAYER] ;

// --- エフェクトオブジェクト
Lazer			gc_Lazer[MAX_EFFECT] ;
Gard			gc_Gard[MAX_EFFECT] ;					// --- ガードエフェクト
FireWeak		gc_Fire_Weak[MAX_EFFECT] ;
FireStrong      gc_Fire_Strong[MAX_EFFECT] ;
Accelerated     gc_Accelerated[MAX_EFFECT] ;
Sound			gc_Sound[MAX_PLAYER][MAX_SOUND] ;
Sound			gc_Main_Sound[MAX_BS] ;

// --- 変数
int				gv_WindowMode = 0 ;						// --- ウィンドウモード
int				gv_PadStatus[MAX_PLAYER] ;				// --- パッド状態
int				gv_Gamemode = GM_Start ;				// --- シーン管理

int				gv_rootflm[MAX_PLAYER] ;				// --- ルートフレーム
int				gv_hAnim1P[MAX_ANIM] ;					// --- アニメーションハンドル　1P
int				gv_hAnim2P[MAX_ANIM] ;					// --- アニメーションハンドル　2P
int				gv_attachidx[MAX_PLAYER] ;				// --- アタッチインデックス
float			gv_anim_totaltime[MAX_PLAYER] = {0} ;	// --- アニメーション総時間
float			gv_anim_timestock1P[MAX_ANIM] = {0} ;	// --- アニメーション別総時間ストック　1P
float			gv_anim_timestock2P[MAX_ANIM] = {0} ;	// --- アニメーション別総時間ストック　2P
float			gv_anim_playtime[MAX_PLAYER]  = {0} ;	// --- アニメーション進行時間

int			gv_rootflmwl[MAX_PLAYER] ;				// --- ルートフレーム
int			gv_hAnimwl[MAX_ANIM] ;					// --- アニメーションハンドル　1P
int			gv_attachidxwl[MAX_PLAYER] ;				// --- アタッチインデックス
float		gv_anim_totaltimewl[MAX_PLAYER] ;			// --- アニメーション総時間
float		gv_anim_timestockwl[MAX_ANIM] ;			// --- アニメーション別総時間ストック　1P
float		gv_anim_playtimewl[MAX_PLAYER]  ;			// --- アニメーション進行時間

// --- オブジェクト用アニメーション変数
int			gv_rootflmobj[MAX_OBJECT] ;					// --- ルートフレーム
int			gv_hAnimobj[MAX_ANIM] ;						// --- アニメーションハンドル
int			gv_attachidxobj[MAX_OBJECT] ;				// --- アタッチインデックス
float		gv_anim_totaltimeobj[MAX_OBJECT] = {0} ;			// --- アニメーション総時間
float		gv_anim_timestockobj[MAX_ANIM] = {0} ;			// --- アニメーション別総時間ストック
float		gv_anim_playtimeobj[MAX_OBJECT] = {0} ;			// --- アニメーション進行時間

int				gv_win_width ;							// --- ウィンドウ横幅
int				gv_win_height ;							// --- ウィンドウ高さ
int				gv_gnbackimg ;							// --- ゲーム中背景画像
int				gv_gnselectbackimg ;					// --- 
int				gv_gnfadeimg ;							// --- フェードアウト・イン用画像
int				gv_gnkillimg ;							// --- キルカウント背景画像
int				gv_numimg[12] ;							// --- 番号画像
int				gv_hpframeimg[2] ;						// --- ＨＰバーのフレーム
int				gv_hpgage[1450] ;						// --- ＨＰバーの中身
int				gv_stmngage[1010] ;						// --- スタミナゲージ
int				gv_selectarrowLimg[2] ;					// ---
int				gv_selectarrowRimg[2] ;					// ---
int				gv_selectcursor1P ;						// ---
int				gv_selectcursor2P ;						// ---
int				gv_selectok1P ;							// ---
int				gv_selectok2P ;							// ---
int				gv_selectyesno ;						// ---
int				gv_selectready ;						// --- 
int				gv_titleimg ;							// --- タイトル文字画像
int				gv_anybuttonimg ;						// --- "PRESS ANY BUTTON TO START"画像
int			gv_gnkillcover ;						// --- キルカウンターのカバー 
int			gv_win ;
int			gv_lose ;
int			gv_selectplayer[MAX_PLAYER] ;						// --- セレクト画面　プレイヤー番号
int			gv_lastkill ;

BOOL			gv_gamestarttim = FALSE ;				// --- ゲームスタートタイマー		
DATEDATA		gv_nowtimer ;							// --- 現在時間の取得
int				gv_limittime ;							// --- タイマーのリミット
int				gv_tmptimer ;							// --- 一時的に時間を確保する
int				gv_hpmax[MAX_PLAYER] = {720,720} ;		// --- プレイヤーのＨＰ上限
TCHAR			debug_str[256] ;

BOOL reflg = FALSE ;
BOOL reendflg = FALSE ;

/* ============================================================ +/
/+																+/
/+	関数														+/
/+																+/
/+ ============================================================ */

/* ------------------------------------------------------------ +/

	SetAnimation	アニメーションデータセット
		アニメーションハンドル
		アニメーション総時間の取得
 + ------------------------------------------------------------ +
		-引数-
			int pno		   --> プレイヤー番号
			int anim_type  --> アニメーションタイプ　STAYとかRUN
			int *anim_path --> アニメーションファイルのパス
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int SetAnimation( int pno, int anim_type, TCHAR anim_path[] )
{	
	int tmp_attach ;	// --- 一時取得用

	/* ------------------------------------------------------------ +/
		アニメーション読み込み
	/+ ------------------------------------------------------------ */
	// --- プレイヤー１
	if ( pno == PLAYER_1 ){
		gv_hAnim1P[anim_type] = MV1LoadModel( anim_path ) ;		// --- 配列にハンドルを格納
		if ( gv_hAnim1P[anim_type] == -1 ) printf( "1P アニメ　読み込み失敗\n" ) ;	// --- 失敗時処理

		tmp_attach = MV1AttachAnim( gc_Player[pno].GetModelHandle(), 0, gv_hAnim1P[anim_type] ) ;	// --- アニメ総時間取得のために一時的にアタッチインデックスに格納
		gv_anim_timestock1P[anim_type] =
			MV1GetAttachAnimTotalTime( gc_Player[pno].GetModelHandle(), tmp_attach ) ; // --- 総時間取得
	}
	// --- プレイヤー２
	else if ( pno == PLAYER_2 ){
		gv_hAnim2P[anim_type] = MV1LoadModel( anim_path ) ;		// --- 配列にハンドルを格納
		if ( gv_hAnim2P[anim_type] == -1 ) printf( "2P アニメ　読み込み失敗\n" ) ;	// --- 失敗時処理

		tmp_attach = MV1AttachAnim( gc_Player[pno].GetModelHandle(), 0, gv_hAnim2P[anim_type] ) ;	// --- アニメ総時間取得のために一時的にアタッチインデックスに格納
		gv_anim_timestock2P[anim_type] =
			MV1GetAttachAnimTotalTime( gc_Player[pno].GetModelHandle(), tmp_attach ) ; // --- 総時間取得
	}
	else{	// --- プレイヤー番号の引数が間違っていた場合
		printf( "想定外のプレイヤー番号\n" ) ;
	}
	MV1DetachAnim( gc_Player[pno].GetModelHandle(), tmp_attach ) ;	// --- ハンドルから一時アタッチ用インデックスをデタッチ

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	SetObjAnimation	その他オブジェクトアニメーションデータセット
		アニメーションハンドル
		アニメーション総時間の取得
 + ------------------------------------------------------------ +
		-引数-
			int anim_type  --> アニメーションタイプ　[A_???_???]とかいうやつ
			int *anim_path --> アニメーションファイルのパス
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int SetObjAnimation( int anim_type, TCHAR anim_path[] )
{	
	int tmp_attach ;	// --- 一時取得用

	/* ------------------------------------------------------------ +/
		アニメーション読み込み
	/+ ------------------------------------------------------------ */
	gv_hAnimobj[anim_type] = MV1LoadModel( anim_path ) ;
	if ( gv_hAnimobj[anim_type] == -1 ) printf( "アニメ　読み込み失敗\n" ) ;

		tmp_attach = MV1AttachAnim( gc_Item.GetModelHandle(), 0, gv_hAnimobj[anim_type] ) ;
		gv_anim_timestockobj[anim_type] =
			MV1GetAttachAnimTotalTime( gc_Item.GetModelHandle(), tmp_attach ) ; // --- 総時間取得

	return( TRUE ) ;
}

int SetWinloseAnimation( int pno, int anim_type, TCHAR anim_path[] )
{	
	int tmp_attach ;	// --- 一時取得用

	/* ------------------------------------------------------------ +/
		アニメーション読み込み
	/+ ------------------------------------------------------------ */
	gv_hAnimwl[anim_type] = MV1LoadModel( anim_path ) ;
	if ( gv_hAnimwl[anim_type] == -1 ) printf( "アニメ　読み込み失敗\n" ) ;

		tmp_attach = MV1AttachAnim( gc_winlose[pno].GetModelHandle(), 0, gv_hAnimwl[anim_type] ) ;
		gv_anim_timestockwl[anim_type] =
			MV1GetAttachAnimTotalTime( gc_Item.GetModelHandle(), tmp_attach ) ; // --- 総時間取得

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	PlayAnim		アニメーション再生
 + ------------------------------------------------------------ +
		-引数-
			int pno --> プレイヤー番号
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int PlayAnim( int pno )
{
	gv_anim_playtime[pno] += 0.5f ;	// --- アニメーション進行
	if( gv_anim_playtime[pno] > gv_anim_totaltime[pno] ){	// --- 総時間より進行時間が大きくなったら最初に戻す
		gv_anim_playtime[pno] = 0.0f ;
	}

	// --- キャラモデルにアタッチされているアニメにアニメ進行時間を与える
	MV1SetAttachAnimTime( gc_Player[pno].GetModelHandle(), gv_attachidx[pno], gv_anim_playtime[pno] ) ;

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	PlayAnim		アニメーション再生
 + ------------------------------------------------------------ +
		-引数-
			int pno --> プレイヤー番号
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int PlayObjAnim( int ono )
{
	gv_anim_playtimeobj[ono] += 0.5f ;
	if( gv_anim_playtimeobj[ono] > gv_anim_totaltimeobj[ono] ){
		gv_anim_playtimeobj[ono] = 0.0f ;
	}

	// キャラモデルにアタッチされているアニメにアニメ進行時間を与える
	MV1SetAttachAnimTime( gc_Item.GetModelHandle(), gv_attachidxobj[ono], gv_anim_playtimeobj[ono] ) ;

	return( TRUE ) ;
}

int PlayWinloseAnim( int wlno )
{
	gv_anim_playtimewl[wlno] += 0.5f ;
	if( gv_anim_playtimewl[wlno] > gv_anim_totaltimewl[wlno] ){
		gv_anim_playtimewl[wlno] = 0.0f ;
	}

	// キャラモデルにアタッチされているアニメにアニメ進行時間を与える
	MV1SetAttachAnimTime( gc_Item.GetModelHandle(), gv_attachidxwl[wlno], gv_anim_playtimewl[wlno] ) ;

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	SelectAnim		アニメーション選択
				アタッチされているアニメーションを切り替える
 + ------------------------------------------------------------ +
		-引数-
			int playno --> プレイヤー番号
			int animno --> アニメーション番号
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int SelectAnim( int playno, int animno )
{
	gv_anim_playtime[playno] = 0.1f ;
	// --- プレイヤー１
	if ( playno == PLAYER_1 ){
		gv_attachidx[playno] = MV1AttachAnim( gc_Player[playno].GetModelHandle(), 0, gv_hAnim1P[animno] ) ;
		gv_anim_totaltime[playno] = gv_anim_timestock1P[animno] ;
	}
	// --- プレイヤー２
	if ( playno == PLAYER_2 ){
		gv_attachidx[playno] = MV1AttachAnim( gc_Player[playno].GetModelHandle(), 0, gv_hAnim2P[animno] ) ;
		gv_anim_totaltime[playno] = gv_anim_timestock2P[animno] ;
	}
	MV1SetAttachAnimTime( gc_Player[playno].GetModelHandle(), gv_attachidx[playno], 0.0f ) ;

	return animno ;
}

/* ------------------------------------------------------------ +/

	SetCameraPos		カメラ移動
 + ------------------------------------------------------------ +
		-引数-
			void
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
VECTOR pos		= VGet(0.0f, 100.0f, -350.0f) ;	// --- 位置
VECTOR target	= VGet(0.0f, 90.0f, 100.0f) ;	// --- 注視点
int SetCameraPos( void )
{
	// --- F1キーで近づく
	if ( CheckHitKey( KEY_INPUT_F1 ) > 0 ){
		pos.z += 10 ;
	}
	// --- F2キーで離れる
	if ( CheckHitKey( KEY_INPUT_F2 ) > 0 ){
		pos.z -= 10 ;
	}
	if ( CheckHitKey( KEY_INPUT_F3 ) > 0 ){
		pos.z += 10 ;
		pos.x += 10 ;
	}
	if ( CheckHitKey( KEY_INPUT_F4 ) > 0 ){
		pos.z -= 10 ;
		pos.x -= 10 ;
	}


	// --- カメラの諸々のセット
 	SetCameraPositionAndTargetAndUpVec( pos, target, VGet(0.0f,0.0f,1.0f) ) ;

	return( TRUE ) ;
}

/*- [EOF] -*/
