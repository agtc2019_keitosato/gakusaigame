/* ---------------------------------------------------------------- */
/*																	*/
/*							エフェクト処理							*/
/*																	*/
/* ---------------------------------------------------------------- */

#include "Common.h"

/* ---------------------------------------- */
/*											*/
/*				インストラクタ				*/
/*											*/
/* ---------------------------------------- */
Gard::Gard()
{
	printf( "ガードが作成された\n" ) ;
	pos = VGet( 0.0f, 0.0f, 0.0f ) ;		// --- 座標
	spd.x = 0.0f ;
	angle = 180.0f ;
	AlphValue = 0.5f ;
	gc_Gard[PLAYER1_EFFECT].gflg = FALSE ;
	gc_Gard[PLAYER2_EFFECT].gflg = FALSE ;
	gc_Gard[PLAYER1_EFFECT].gardflg = TRUE ;
	gc_Gard[PLAYER2_EFFECT].gardflg = TRUE ;
}

/* ---------------------------------------- */
/*											*/
/*				デストラクタ				*/
/*											*/
/* ---------------------------------------- */
Gard::~Gard()
{
}
	
/* ---------------------------------------- */
/*											*/
/*				モデル表示					*/
/*											*/
/* ---------------------------------------- */
void Gard::SetModel( TCHAR Epath[] )
{
	hEModel_Gard = MV1LoadModel( Epath ) ;
}

/* ---------------------------------------- */
/*											*/
/*				エフェクト処理				*/
/*											*/
/* ---------------------------------------- */
int Gard::UpdatePosDir( int Eno )
{
	// --- サイズ調整
	MV1SetScale( hEModel_Gard , VGet(0.15f,0.15f,0.15f)) ;

	// --- プレイヤー１の時
	if ( Eno == PLAYER_1 )
	{
		if ( ( (gc_Player[PLAYER_1].GetDirection() == D_DOWN) && (gc_Player[PLAYER_1].GetDirinfo() == D_RIGHT) ) || (gc_Player[PLAYER_1].GetDirection() == D_RIGHT))
		{
			gc_Gard[PLAYER1_EFFECT].pos = gc_Player[PLAYER_1].GetPosition() ;
			pos.x += 40 ;
			pos.y += 65 ;
			pos.z -= 60 ;
			MV1SetPosition( gc_Gard[PLAYER1_EFFECT].GetModelHandle(), pos ) ;
 			MV1SetRotationXYZ( gc_Gard[PLAYER1_EFFECT].GetModelHandle() , VGet( 0.0f , 90.0f * DX_PI_F / 45.0f , 0.0f) ) ;
//MV1SetRotationZYAxis( gc_Gard[PLAYER1_EFFECT].GetModelHandle(), VGet( -0.5f, 0.5f, 0.0f ), VGet( 0.5f, 0.5f, 0.0f ), 0.0f ) ;
		}
		if ( ( (gc_Player[PLAYER_1].GetDirection() == D_DOWN) && (gc_Player[PLAYER_1].GetDirinfo() == D_LEFT) ) || (gc_Player[PLAYER_1].GetDirection() == D_LEFT))
		{
			gc_Gard[PLAYER1_EFFECT].pos = gc_Player[PLAYER_1].GetPosition() ;
			pos.x -= 40 ;
			pos.y += 65 ;
			pos.z -= 60 ;
			MV1SetPosition( gc_Gard[PLAYER1_EFFECT].GetModelHandle(), pos ) ;
			MV1SetRotationXYZ( gc_Gard[PLAYER1_EFFECT].GetModelHandle() , VGet( 0.0f , 90.0f * DX_PI_F / 45.0f , 0.0f) ) ;
//			MV1SetRotationXYZ( gc_Gard[PLAYER1_EFFECT].GetModelHandle() , VGet( 0.0f , 90.0f * DX_PI_F / 360.0f , 0.0f) ) ;
		}
	}


	// --- プレイヤー２の時
	if ( Eno == PLAYER_2 )
	{
		if ( ( (gc_Player[PLAYER_2].GetDirection() == D_DOWN) && (gc_Player[PLAYER_2].GetDirinfo() == D_RIGHT) ) || (gc_Player[PLAYER_2].GetDirection() == D_RIGHT))
		{
			gc_Gard[PLAYER2_EFFECT].pos = gc_Player[PLAYER_2].GetPosition() ;
			pos.x += 40 ;
			pos.y += 65 ;
			pos.z -= 60 ;
 			MV1SetRotationXYZ( gc_Gard[PLAYER1_EFFECT].GetModelHandle() , VGet( 0.0f , 90.0f * DX_PI_F / 45.0f , 0.0f) ) ;
//			MV1SetRotationXYZ( gc_Gard[PLAYER2_EFFECT].GetModelHandle() , VGet( 0.0f , 90.0f * DX_PI_F / 180.0f , 0.0f) ) ;
			MV1SetPosition( gc_Gard[PLAYER2_EFFECT].GetModelHandle(), pos ) ;
		}
		if ( ( (gc_Player[PLAYER_2].GetDirection() == D_DOWN) && (gc_Player[PLAYER_2].GetDirinfo() == D_LEFT) ) || (gc_Player[PLAYER_2].GetDirection() == D_LEFT))
		{
			gc_Gard[PLAYER2_EFFECT].pos = gc_Player[PLAYER_2].GetPosition() ;
			pos.x -= 40 ;
			pos.y += 65 ;
			pos.z -= 60 ;
 			MV1SetRotationXYZ( gc_Gard[PLAYER1_EFFECT].GetModelHandle() , VGet( 0.0f , 90.0f * DX_PI_F / 45.0f , 0.0f) ) ;
//			MV1SetRotationXYZ( gc_Gard[PLAYER2_EFFECT].GetModelHandle() , VGet( 0.0f , 90.0f * DX_PI_F / 180.0f , 0.0f) ) ;
			MV1SetPosition( gc_Gard[PLAYER2_EFFECT].GetModelHandle(), pos ) ;
		}
	}

	if ( gc_Gard[PLAYER1_EFFECT].gflg == FALSE )
	{
		MV1SetVisible( gc_Gard[PLAYER1_EFFECT].GetModelHandle(), FALSE ) ;
	}
	if ( gc_Gard[PLAYER2_EFFECT].gflg == FALSE )
	{
		MV1SetVisible( gc_Gard[PLAYER2_EFFECT].GetModelHandle(), FALSE ) ;
	}


	return( TRUE ) ;

}

// --- モデルハンドルと位置の取得
int Gard::GetModelHandle( void ){ return hEModel_Gard ; }



