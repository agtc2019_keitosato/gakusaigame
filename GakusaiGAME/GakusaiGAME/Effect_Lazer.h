/* ---------------------------------------------------------------- */
/*																	*/
/*																	*/
/*																	*/
/* ---------------------------------------------------------------- */

class Lazer {

	public :
		Lazer() ;
		~Lazer() ;
		int  EffectInit() ;				// --- 初期化用
		void SetModel( TCHAR [] ) ;		// --- モデル読み込み
		void HitFire() ;				// --- ヒット時のアニメーション用
		int GetModelHandle() ;
		int UpdatePosDir( int ) ;
		float	AlphValue ;				// --- 透明度用
		VECTOR  pos ;					// --- 場所用
		int  FrameIndex[2] ;
		BOOL lflg ;

	private :
		int	    hEModel ;	// --- モデルハンドル
		float   AnimTotalTime[2] ;			// --- アニメーションの総時間
		float   AnimNowTime[2] ;			// --- アニメーション時間
		int     AnimAttachIndex[2] ;		// --- アニメーションアタッチ
		int     anim_lazer[2] ;				// --- エフェクト用
		float   angle ;					// --- 向きを変更する用
		VECTOR  spd ;
		float   rot ;

} ;
