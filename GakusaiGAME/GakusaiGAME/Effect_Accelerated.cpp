/* ---------------------------------------------------------------- */
/*																	*/
/*							エフェクト処理							*/
/*																	*/
/* ---------------------------------------------------------------- */

#include "Common.h"

/* ---------------------------------------- */
/*											*/
/*				インストラクタ				*/
/*											*/
/* ---------------------------------------- */
Accelerated::Accelerated()
{
	printf( "作成された\n" ) ;
	pos = VGet( 0.0f, 0.0f, 0.0f ) ;	// --- 座標
	AnimNowTime[PLAYER1_EFFECT] = 0.0f ;
	AnimNowTime[PLAYER2_EFFECT] = 0.0f ;
	spd.x = 0.0f ;
	angle = 180.0f ;
	AlphValue = 0.5f ;
	gc_Accelerated[PLAYER1_EFFECT].accel_flg = FALSE ;
	gc_Accelerated[PLAYER2_EFFECT].accel_flg = FALSE ;
}

/* ---------------------------------------- */
/*											*/
/*				デストラクタ				*/
/*											*/
/* ---------------------------------------- */
Accelerated::~Accelerated()
{
}

/* ---------------------------------------- */
/*											*/
/*				初期化処理					*/
/*											*/
/* ---------------------------------------- */
int Accelerated::EffectInit()
{
	// --- ブーストエフェクトを読み込み格納(プレイヤー１)
	anim_Accelerated[PLAYER1_EFFECT] = MV1LoadModel( PATH_FIRE_ACCELERATED_ANIM ) ;
	AnimAttachIndex[PLAYER1_EFFECT] = MV1AttachAnim( gc_Accelerated[PLAYER1_EFFECT].GetModelHandle() ,0,anim_Accelerated[PLAYER1_EFFECT]) ;
	AnimTotalTime[PLAYER1_EFFECT] = MV1GetAttachAnimTotalTime(gc_Accelerated[PLAYER1_EFFECT].GetModelHandle(),AnimAttachIndex[PLAYER1_EFFECT]) ;

	// --- ブーストエフェクトを読み込み格納(プレイヤー２)
	anim_Accelerated[PLAYER2_EFFECT] = MV1LoadModel( PATH_FIRE_ACCELERATED_ANIM ) ;
	AnimAttachIndex[PLAYER2_EFFECT] = MV1AttachAnim( gc_Accelerated[PLAYER2_EFFECT].GetModelHandle() ,0,anim_Accelerated[PLAYER2_EFFECT]) ;
	AnimTotalTime[PLAYER2_EFFECT] = MV1GetAttachAnimTotalTime(gc_Accelerated[PLAYER2_EFFECT].GetModelHandle(),AnimAttachIndex[PLAYER2_EFFECT]) ;

	FrameIndex = MV1SearchFrame( hEModel, "root" ) ;
	MV1SetFrameUserLocalMatrix(hEModel,FrameIndex,MGetIdent()) ;


	return 0 ;

}

	
/* ---------------------------------------- */
/*											*/
/*				モデル表示					*/
/*											*/
/* ---------------------------------------- */
void Accelerated::SetModel( TCHAR Epath[] )
{
	hEModel = MV1LoadModel( Epath ) ;
}

/* ---------------------------------------- */
/*											*/
/*				エフェクト処理				*/
/*											*/
/* ---------------------------------------- */
void Accelerated::HitFire()
{

	if ( gc_Accelerated[PLAYER1_EFFECT].accel_flg == TRUE )
	{
		// --- アニメーションが再生し終わったら、モデルを消す
		MV1SetVisible( gc_Accelerated[PLAYER1_EFFECT].GetModelHandle() , TRUE ) ;
		AnimNowTime[PLAYER1_EFFECT] += 0.7f ;
		if ( AnimNowTime[PLAYER1_EFFECT] > AnimTotalTime[PLAYER1_EFFECT] )
		{
			// --- アニメーションを再生し終わるまで描画
			MV1SetVisible( gc_Accelerated[PLAYER1_EFFECT].GetModelHandle() , FALSE ) ;
			// --- ブーストゲージがある時、ゲージを減らしアニメーションも最初に戻す
			AnimNowTime[PLAYER1_EFFECT] = 0.0f ;
			gc_Accelerated[PLAYER1_EFFECT].accel_flg = FALSE ;
		}
	}

	if ( gc_Accelerated[PLAYER2_EFFECT].accel_flg == TRUE )
	{
		// --- アニメーションが再生し終わったら、モデルを消す
		MV1SetVisible( gc_Accelerated[PLAYER2_EFFECT].GetModelHandle() , TRUE ) ;
		AnimNowTime[PLAYER2_EFFECT] += 0.7f ;
		if ( AnimNowTime[PLAYER2_EFFECT] > AnimTotalTime[PLAYER2_EFFECT] )
		{
			// --- アニメーションを再生し終わるまで描画
			MV1SetVisible( gc_Accelerated[PLAYER2_EFFECT].GetModelHandle() , FALSE ) ;
			// --- ブーストゲージがある時、ゲージを減らしアニメーションも最初に戻す
			AnimNowTime[PLAYER2_EFFECT] = 0.0f ;
			gc_Accelerated[PLAYER2_EFFECT].accel_flg = FALSE ;
		}
	}

	MV1SetAttachAnimTime( gc_Accelerated[PLAYER1_EFFECT].GetModelHandle() , AnimAttachIndex[PLAYER1_EFFECT] , AnimNowTime[PLAYER1_EFFECT] ) ;
	MV1SetAttachAnimTime( gc_Accelerated[PLAYER2_EFFECT].GetModelHandle() , AnimAttachIndex[PLAYER2_EFFECT] , AnimNowTime[PLAYER2_EFFECT] ) ;

}

int Accelerated::UpdatePosDir( int Eno )
{
	// --- サイズ調整
	MV1SetScale( hEModel , VGet(0.03f,0.020f,0.03f)) ;

	// --- プレイヤー１の時
	if ( Eno == PLAYER_1 )
	{
		if ( ( (gc_Player[PLAYER_1].GetDirection() == D_DOWN) && (gc_Player[PLAYER_1].GetDirinfo() == D_RIGHT) ) || (gc_Player[PLAYER_1].GetDirection() == D_RIGHT) )
		{
			gc_Accelerated[PLAYER1_EFFECT].pos = gc_Player[PLAYER_1].GetPosition() ;
			MV1SetRotationXYZ( gc_Accelerated[PLAYER1_EFFECT].GetModelHandle() , VGet( 1.57f * 0.25f , 1.57f * 1.0f , 0.0f ) ) ;
			pos.y += 100 ;
			pos.z += 10 ;
			MV1SetPosition( gc_Accelerated[PLAYER1_EFFECT].GetModelHandle(), pos ) ;
		}
		if ( ( (gc_Player[PLAYER_1].GetDirection() == D_DOWN) && (gc_Player[PLAYER_1].GetDirinfo() == D_LEFT) ) || (gc_Player[PLAYER_1].GetDirection() == D_LEFT) )
		{
			gc_Accelerated[PLAYER1_EFFECT].pos = gc_Player[PLAYER_1].GetPosition() ;
			MV1SetRotationXYZ( gc_Accelerated[PLAYER1_EFFECT].GetModelHandle() , VGet( 1.57f * 3.75f , 1.57f * 1.0f , 0.0f ) ) ;
			pos.y += 100 ;
			pos.z += 10 ;
			MV1SetPosition( gc_Accelerated[PLAYER1_EFFECT].GetModelHandle(), pos ) ;
		}
	}

	// --- プレイヤー２の時
	if ( Eno == PLAYER_2 )
	{
		if ( ( (gc_Player[PLAYER_2].GetDirection() == D_DOWN) && (gc_Player[PLAYER_2].GetDirinfo() == D_RIGHT) ) || (gc_Player[PLAYER_2].GetDirection() == D_RIGHT) )
		{
			gc_Accelerated[PLAYER2_EFFECT].pos = gc_Player[PLAYER_2].GetPosition() ;
			MV1SetRotationXYZ( gc_Accelerated[PLAYER2_EFFECT].GetModelHandle() , VGet( 1.57f * 0.25f , 1.57f * 1.0f , 0.0f ) ) ;
			pos.y += 100 ;
			pos.z += 10 ;
			MV1SetPosition( gc_Accelerated[PLAYER2_EFFECT].GetModelHandle(), pos ) ;
		}
		if ( ( (gc_Player[PLAYER_2].GetDirection() == D_DOWN) && (gc_Player[PLAYER_2].GetDirinfo() == D_LEFT) ) || (gc_Player[PLAYER_2].GetDirection() == D_LEFT) )
		{
			gc_Accelerated[PLAYER2_EFFECT].pos = gc_Player[PLAYER_2].GetPosition() ;
			MV1SetRotationXYZ( gc_Accelerated[PLAYER2_EFFECT].GetModelHandle() , VGet( 1.57f * 3.75f , 1.57f * 1.0f , 0.0f ) ) ;
			pos.y += 100 ;
			pos.z += 10 ;
			MV1SetPosition( gc_Accelerated[PLAYER2_EFFECT].GetModelHandle(), pos ) ;
		}
	}

	return( TRUE ) ;
}

// --- モデルハンドルと位置の取得
int Accelerated::GetModelHandle( void ){ return hEModel ; }




