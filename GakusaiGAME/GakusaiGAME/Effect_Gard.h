/* ---------------------------------------------------------------- */
/*																	*/
/*																	*/
/*																	*/
/* ---------------------------------------------------------------- */

class Gard {

	public :
		Gard() ;
		~Gard() ;
		void SetModel( TCHAR [] ) ;		// --- モデル読み込み
		int GetModelHandle() ;
		int UpdatePosDir( int ) ;
		float	AlphValue ;				// --- 透明度用
		VECTOR  pos ;					// --- 場所用
		BOOL gflg ;
		int   rot ;
		BOOL gardflg ;

	private :
		int	    hEModel_Gard ;			// --- モデルハンドル
		float   angle ;					// --- 向きを変更する用
		VECTOR  spd ;

} ;
