/* ============================================================ +/ 

	LastEdit : 2020/09/25		NAME:佐藤圭桃
	LastEdit : ----/--/--		NAME:
	LastEdit : 2020/10/14		NAME:宮崎櫻
	___________________________________________________________

		WinMain.cpp
	___________________________________________________________

		メインプログラム

/+ ============================================================ */
#include "Common.h"

int WINAPI WinMain( HINSTANCE hI, HINSTANCE hP, LPSTR lpC, int nC ){

	/* ============================================================ +/

		変数

	/+ ============================================================ */
	RECT rc;				// --- 画面サイズ取得用


	/* ============================================================ +/

		ウィンドウ設定

	/+ ============================================================ */

	// --- ウィンドウモード分岐
	gv_WindowMode = MessageBox( NULL, "フルスクリーンモードでプレイしますか？", "確認", MB_YESNO ) ;
	if ( gv_WindowMode == IDYES ){
		gv_WindowMode = WINDOW_MODE_FULL ;	// --- フルスクリーンモード
	}
	else{
		gv_WindowMode = WINDOW_MODE_WIN ;	// --- ウィンドウモード
	}
	ChangeWindowMode( gv_WindowMode ) ;					// --- モード切り替え
	SetGraphMode( WINDOW_SIZE_W, WINDOW_SIZE_H, 32 ) ;	// --- サイズ切り替え

	/* ============================================================ +/

		DXライブラリ

	/+ ============================================================ */

	// --- DXライブラリの初期化
	if( DxLib_Init( ) == -1 ) return -1 ;

	// --- 裏画面の描画設定
	SetDrawScreen( DX_SCREEN_BACK ) ;

	GetWindowRect(GetDesktopWindow(), &rc);
	gv_win_width = rc.right-rc.left;
	int gv_win_height = rc.bottom-rc.top ;

	// --- ライト
//		ChangeLightTypeDir( VGet(0.0f, -1.0f, 1.0f) ) ; // --- ライトの方向を変更
	SetUseLighting( FALSE );	// --- ライティング処理なし


	// --- カメラ

	/* ------------------------------------------------------------ +/
		初期位置セット
	/+ ------------------------------------------------------------ */

	gv_gnbackimg = LoadGraph( "..\\Release\\Models\\UI\\BackGround2.png" ) ;
	gv_gnkillcover = LoadGraph( "..\\Release\\Models\\UI\\KillCountCover.png" ) ;
	gv_gnselectbackimg = LoadGraph( "..\\Release\\Models\\UI\\BackGround_CharaSelect.png" ) ;

	gv_gnkillimg = LoadGraph( "..\\Release\\Models\\UI\\KillCounterImage.png" ) ;
	gv_gnfadeimg = LoadGraph( "..\\Release\\Models\\UI\\GameFade.png" ) ;
	gv_selectarrowLimg[PLAYER_1] = LoadGraph( "..\\Release\\Models\\UI\\SelectArrow_L.png" ) ;
	gv_selectarrowRimg[PLAYER_1] = LoadGraph( "..\\Release\\Models\\UI\\SelectArrow_R.png" ) ;
	gv_selectarrowLimg[PLAYER_2] = LoadGraph( "..\\Release\\Models\\UI\\SelectArrow_L.png" ) ;
	gv_selectarrowRimg[PLAYER_2] = LoadGraph( "..\\Release\\Models\\UI\\SelectArrow_R.png" ) ;
	gv_selectcursor1P = LoadGraph( "..\\Release\\Models\\UI\\SelectCursor_1P.png" ) ;
	gv_selectcursor2P = LoadGraph( "..\\Release\\Models\\UI\\SelectCursor_2P.png" ) ;
	gv_selectok1P = LoadGraph( "..\\Release\\Models\\UI\\OK_1P.png" ) ;
	gv_selectok2P = LoadGraph( "..\\Release\\Models\\UI\\OK_2P.png" ) ;
	gv_selectyesno = LoadGraph( "..\\Release\\Models\\UI\\YesNo2.png" ) ;
	gv_selectready = LoadGraph( "..\\Release\\Models\\UI\\READY_Question.png" ) ;
	gv_titleimg = LoadGraph( "..\\Release\\Models\\UI\\Title.png" ) ;
	gv_anybuttonimg = LoadGraph( "..\\Release\\Models\\UI\\PRESS_ANY_BUTTON.png" ) ;
	gv_win = LoadGraph( "..\\Release\\Models\\UI\\WIN.png" ) ;
	gv_lose = LoadGraph( "..\\Release\\Models\\UI\\LOSE.png" ) ;
	gv_selectplayer[PLAYER_1] = LoadGraph( "..\\Release\\Models\\UI\\1P.png" ) ;						// --- セレクト画面　プレイヤー番号
	gv_selectplayer[PLAYER_2] = LoadGraph( "..\\Release\\Models\\UI\\2P.png" ) ;						// --- セレクト画面　プレイヤー番号

	LoadDivGraph( "..\\Release\\Models\\UI\\NumberImage.png", 10, 5, 2, 100, 120, gv_numimg ) ;

	LoadDivGraph( "..\\Release\\Models\\UI\\HPFrame2.png", 2, 1, 2, 740, 125, gv_hpframeimg ) ;

	LoadDivGraph( "..\\Release\\Models\\UI\\HPGage.png", 1440, 720, 2, 1, 76, gv_hpgage ) ;	

	LoadDivGraph( "..\\Release\\Models\\UI\\StaminaGage.png", 1004, 502, 2, 1, 35, gv_stmngage ) ;


	// --- プレイヤー２用のサウンド
	gc_Sound[PLAYER_1][0].SetModel(PATH_SOUND_SE_PUNCH_SWING) ;		// --- 殴(素振り)
	gc_Sound[PLAYER_1][1].SetModel(PATH_SOUND_SE_PUNCH_HIT_1) ;		// --- 殴ヒット時			
	gc_Sound[PLAYER_1][2].SetModel(PATH_SOUND_SE_KICK_SWING) ;		// --- 蹴(素振り)
	gc_Sound[PLAYER_1][3].SetModel(PATH_SOUND_SE_KICK_HIT) ;		// --- 蹴ヒット時
	gc_Sound[PLAYER_1][4].SetModel(PATH_SOUND_SE_JUMP) ;			// --- ジャンプ時
	gc_Sound[PLAYER_1][5].SetModel(PATH_SOUND_SE_JUMP_LANDING) ;	// --- 着地
	gc_Sound[PLAYER_1][6].SetModel(PATH_SOUND_SE_WALK_1) ;			// --- 歩き
	gc_Sound[PLAYER_1][7].SetModel(PATH_SOUND_SE_PLAYER_DOWN) ;		// --- ダウン
	gc_Sound[PLAYER_1][8].SetModel(PATH_SOUND_SE_GARD) ;			// --- 盾
	gc_Sound[PLAYER_1][9].SetModel(PATH_SOUND_SE_GLASS_CRACK) ;		// --- 盾半壊
	gc_Sound[PLAYER_1][10].SetModel(PATH_SOUND_SE_GLASS_BREAK) ;	// --- 盾破壊
	gc_Sound[PLAYER_1][11].SetModel(PATH_SOUND_SE_B) ;				// --- 死亡サウンド
	gc_Sound[PLAYER_1][12].SetModel(PATH_SOUND_SE_G) ;				// --- 死亡サウンド
	gc_Sound[PLAYER_1][13].SetModel(PATH_SOUND_SE_LAZER) ;			// --- 小必殺
	gc_Sound[PLAYER_1][14].SetModel(PATH_SOUND_SE_SELECT) ;			// --- キャラクター選択(左右移動)
	gc_Sound[PLAYER_1][15].SetModel(PATH_SOUND_SE_SELECT_1) ;		// --- キャラクター選択(キャラクター決定時)
	gc_Sound[PLAYER_1][16].SetModel(PATH_SOUND_SE_CANCEL_MAIN) ;	// --- ゲーム画面に行く前のキャンセル時
	gc_Sound[PLAYER_1][17].SetModel(PATH_SOUND_SE_CANCEL_1) ;		// --- キャラクター選択中のキャンセル時
	gc_Sound[PLAYER_1][18].SetModel(PATH_SOUND_SE_CANCEL_2) ;		// --- 予備キャンセル
	gc_Sound[PLAYER_1][19].SetModel(PATH_SOUND_SE_STARTBUTTON) ;	// --- スタートボタン押下

	// --- プレイヤー２用のサウンド
	gc_Sound[PLAYER_2][0].SetModel(PATH_SOUND_SE_PUNCH_SWING) ;		// --- 殴(素振り)
	gc_Sound[PLAYER_2][1].SetModel(PATH_SOUND_SE_PUNCH_HIT_1) ;		// --- 殴ヒット時			
	gc_Sound[PLAYER_2][2].SetModel(PATH_SOUND_SE_KICK_SWING) ;		// --- 蹴(素振り)
	gc_Sound[PLAYER_2][3].SetModel(PATH_SOUND_SE_KICK_HIT) ;		// --- 蹴ヒット時
	gc_Sound[PLAYER_2][4].SetModel(PATH_SOUND_SE_JUMP) ;			// --- ジャンプ時
	gc_Sound[PLAYER_2][5].SetModel(PATH_SOUND_SE_JUMP_LANDING) ;	// --- 着地
	gc_Sound[PLAYER_2][6].SetModel(PATH_SOUND_SE_WALK_1) ;			// --- 歩き
	gc_Sound[PLAYER_2][7].SetModel(PATH_SOUND_SE_PLAYER_DOWN) ;		// --- ダウン
	gc_Sound[PLAYER_2][8].SetModel(PATH_SOUND_SE_GARD) ;			// --- 盾
	gc_Sound[PLAYER_2][9].SetModel(PATH_SOUND_SE_GLASS_CRACK) ;		// --- 盾半壊
	gc_Sound[PLAYER_2][10].SetModel(PATH_SOUND_SE_GLASS_BREAK) ;	// --- 盾破壊
	gc_Sound[PLAYER_2][11].SetModel(PATH_SOUND_SE_B) ;				// --- 死亡サウンド
	gc_Sound[PLAYER_2][12].SetModel(PATH_SOUND_SE_G) ;				// --- 死亡サウンド
	gc_Sound[PLAYER_2][13].SetModel(PATH_SOUND_SE_LAZER) ;			// --- 小必殺
	gc_Sound[PLAYER_2][14].SetModel(PATH_SOUND_SE_SELECT) ;			// --- キャラクター選択(左右移動)
	gc_Sound[PLAYER_2][15].SetModel(PATH_SOUND_SE_SELECT_1) ;		// --- キャラクター選択(キャラクター決定時)
	gc_Sound[PLAYER_2][16].SetModel(PATH_SOUND_SE_CANCEL_MAIN) ;	// --- ゲーム画面に行く前のキャンセル時
	gc_Sound[PLAYER_2][17].SetModel(PATH_SOUND_SE_CANCEL_1) ;		// --- キャラクター選択中のキャンセル時
	gc_Sound[PLAYER_2][18].SetModel(PATH_SOUND_SE_CANCEL_2) ;		// --- 予備キャンセル
	gc_Sound[PLAYER_2][19].SetModel(PATH_SOUND_SE_STARTBUTTON) ;	// --- スタートボタン押下
	gc_Sound[PLAYER_1][20].SetModel(PATH_SOUND_SE_POWERUP) ;		// --- アイテムを取った時
	gc_Sound[PLAYER_1][21].SetModel(PATH_SOUND_SE_BOOST) ;			// --- ブースト音
	gc_Sound[PLAYER_2][20].SetModel(PATH_SOUND_SE_POWERUP) ;		// --- アイテムを取った時
	gc_Sound[PLAYER_2][21].SetModel(PATH_SOUND_SE_BOOST) ;			// --- ブースト音

	gc_Main_Sound[0].SetModel( PATH_SOUND_BGM_START ) ;					// --- スタート画面
	gc_Main_Sound[1].SetModel( PATH_SOUND_BGM_CHARASELECT ) ;			// --- キャラクター選択画面
	gc_Main_Sound[2].SetModel( PATH_SOUND_BGM_GAMEPLAY ) ;				// --- ゲーム画面
	gc_Main_Sound[3].SetModel( PATH_SOUND_BGM_RESULT ) ;				// --- リザルト画面
	gc_Main_Sound[4].SetModel( PATH_SOUND_SE_STARTBUTTON_DECISION ) ;	// --- スタートボタン押下
	gc_Main_Sound[5].SetModel( PATH_SOUND_SE_SELECT_END ) ;				// --- キャラクター選択終了時
	gc_Main_Sound[6].SetModel( PATH_SOUND_SE_RESULT_1 ) ;				// --- ドラム音
	gc_Main_Sound[7].SetModel( PATH_SOUND_SE_RESULT_END ) ;				// --- 歓声

//	gc_Sound[12].SetModel( PATH_SOUND_SE_WALK_2 ) ;						// --- 歩き

	SetCameraPositionAndTargetAndUpVec( VGet(0.0f, 100.0f, -350.0f), VGet(0.0f, 90.0f, 100.0f), VGet(0.0f,0.0f,1.0f) ) ;
	int tmp_img = LoadGraph( "..\\Release\\Models\\UI\\LOSE_Big.png" ) ;
	/* ============================================================ +/

		メインループ

	/+ ============================================================ */
	while( ProcessMessage( ) == 0 && CheckHitKey(KEY_INPUT_ESCAPE) == 0 ){

		gv_PadStatus[PLAYER_1] = GetJoypadInputState( DX_INPUT_KEY_PAD1 ) ;	// --- プレイヤー１　パッド情報
		gv_PadStatus[PLAYER_2] = GetJoypadInputState( DX_INPUT_PAD2 ) ;		// --- プレイヤー２　パッド情報
		
		ClearDrawScreen( ) ;	// --- 画面のリセット

		SetCameraPos( ) ;		// --- カメラ移動

		SceneMain( ) ;			// --- シーンループ

		DrawString( 0, 32, debug_str, GetColor(255, 255, 255) ) ;
		//DrawGraph( 0, 0, tmp_img, TRUE ) ;

		ScreenFlip( ) ;			// --- 表画面と裏画面切り替え
	}

	// --- DXライブラリの終了
	DxLib_End( ) ;
	return 0 ;
}


/*- [EOF] -*/
