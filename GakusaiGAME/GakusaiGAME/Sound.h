class Sound {

	public :
		Sound() ;
		~Sound() ;
		void SoundInit() ;				// --- 初期化用
		void SetModel( TCHAR [] ) ;		// --- モデル読み込み
		int GetModelHandle() ;
		int UpdatePosDir() ;
		int cnt ;

	private :
		int	    hSModel ;		// --- モデルハンドル
		VECTOR  pos ;					// --- 場所用

} ;
