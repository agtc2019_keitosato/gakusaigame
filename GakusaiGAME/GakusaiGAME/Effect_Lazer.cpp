/* ---------------------------------------------------------------- */
/*																	*/
/*							エフェクト処理							*/
/*																	*/
/* ---------------------------------------------------------------- */

#include "Common.h"

/* ---------------------------------------- */
/*											*/
/*				インストラクタ				*/
/*											*/
/* ---------------------------------------- */
Lazer::Lazer()
{
	printf( "レーザーが作成された\n" ) ;
	pos = VGet( 0.0f, 0.0f, 0.0f ) ;	// --- 座標
	AnimNowTime[PLAYER1_EFFECT] = 0.0f ;
	AnimNowTime[PLAYER2_EFFECT] = 0.0f ;
	spd.x = 0.0f ;
	angle = 180.0f ;
	AlphValue = 0.5f ;
	rot = 0.3f ;
	lflg = FALSE ;
}

/* ---------------------------------------- */
/*											*/
/*				デストラクタ				*/
/*											*/
/* ---------------------------------------- */
Lazer::~Lazer()
{
}

/* ---------------------------------------- */
/*											*/
/*				初期化処理					*/
/*											*/
/* ---------------------------------------- */
int Lazer::EffectInit()
{
	// --- レーザーエフェクトを読み込み格納(プレイヤー１)
	anim_lazer[PLAYER1_EFFECT] = MV1LoadModel( PATH_FIRE_ANIM ) ;
	AnimAttachIndex[PLAYER1_EFFECT] = MV1AttachAnim(gc_Lazer[PLAYER1_EFFECT].GetModelHandle(),0,anim_lazer[PLAYER1_EFFECT]) ;
	AnimTotalTime[PLAYER1_EFFECT] = MV1GetAttachAnimTotalTime(gc_Lazer[PLAYER1_EFFECT].GetModelHandle(),AnimAttachIndex[PLAYER1_EFFECT]) ;

	// --- レーザーエフェクトを読み込み格納(プレイヤー２)
	anim_lazer[PLAYER2_EFFECT] = MV1LoadModel( PATH_FIRE_ANIM ) ;
	AnimAttachIndex[PLAYER2_EFFECT] = MV1AttachAnim(gc_Lazer[PLAYER2_EFFECT].GetModelHandle(),0,anim_lazer[PLAYER2_EFFECT]) ;
	AnimTotalTime[PLAYER2_EFFECT] = MV1GetAttachAnimTotalTime(gc_Lazer[PLAYER2_EFFECT].GetModelHandle(),AnimAttachIndex[PLAYER2_EFFECT]) ;

	FrameIndex[PLAYER1_EFFECT] = MV1SearchFrame( hEModel, "root" ) ;
	MV1SetFrameUserLocalMatrix(hEModel,FrameIndex[PLAYER1_EFFECT],MGetIdent()) ;


	return 0 ;

}

/* ---------------------------------------- */
/*											*/
/*				モデル表示					*/
/*											*/
/* ---------------------------------------- */
void Lazer::SetModel( TCHAR Epath[] )
{
	hEModel = MV1LoadModel( Epath ) ;
}

/* ---------------------------------------- */
/*											*/
/*				エフェクト処理				*/
/*											*/
/* ---------------------------------------- */
void Lazer::HitFire()
{
	if ( lflg == TRUE )
	{
		AnimNowTime[PLAYER1_EFFECT] += 0.5f ;
		if ( AnimNowTime[PLAYER1_EFFECT] > AnimTotalTime[PLAYER1_EFFECT] )
		{
			// --- アニメーションが再生し終わったら、モデルを消す
			MV1SetVisible( gc_Lazer[PLAYER1_EFFECT].GetModelHandle() , FALSE ) ;
			// --- ブーストゲージがある時、ゲージを減らしアニメーションも最初に戻す
			AnimNowTime[PLAYER1_EFFECT] = 0.0f ;
			lflg = FALSE ;
		}else{
			// --- アニメーションを再生し終わるまで描画
			MV1SetVisible( gc_Lazer[PLAYER1_EFFECT].GetModelHandle() , TRUE ) ;
		}

		AnimNowTime[PLAYER2_EFFECT] += 0.5f ;
		if ( AnimNowTime[PLAYER2_EFFECT] > AnimTotalTime[PLAYER2_EFFECT] )
		{
			// --- アニメーションが再生し終わったら、モデルを消す
			MV1SetVisible( gc_Lazer[PLAYER2_EFFECT].GetModelHandle() , FALSE ) ;
			// --- ブーストゲージがある時、ゲージを減らしアニメーションも最初に戻す
			AnimNowTime[PLAYER2_EFFECT] = 0.0f ;
			lflg = FALSE ;
		}else{
			// --- アニメーションを再生し終わるまで描画
			MV1SetVisible( gc_Lazer[PLAYER2_EFFECT].GetModelHandle() , TRUE ) ;
		}
	}else{
		MV1SetVisible( gc_Lazer[PLAYER1_EFFECT].GetModelHandle() , FALSE ) ;
		MV1SetVisible( gc_Lazer[PLAYER2_EFFECT].GetModelHandle() , FALSE ) ;
	}

	MV1SetAttachAnimTime( gc_Lazer[PLAYER1_EFFECT].GetModelHandle() , AnimAttachIndex[PLAYER1_EFFECT] , AnimNowTime[PLAYER1_EFFECT] ) ;
	MV1SetAttachAnimTime( gc_Lazer[PLAYER2_EFFECT].GetModelHandle() , AnimAttachIndex[PLAYER2_EFFECT] , AnimNowTime[PLAYER2_EFFECT] ) ;

}

int Lazer::UpdatePosDir( int Eno )
{
//	MV1SetRotationXYZ( hEModel , VGet( 0.0f , 1.57f * rot , 0.0f ) ) ;

	// --- サイズ調整
	MV1SetScale( hEModel , VGet(0.5f,0.5f,0.5f)) ;		// --- 

	// --- プレイヤー１の時
	if ( Eno == PLAYER_1 )
	{
		if ( ( (gc_Player[PLAYER_1].GetDirection() == D_DOWN) && (gc_Player[PLAYER_1].GetDirinfo() == D_RIGHT) ) || (gc_Player[PLAYER_1].GetDirection() == D_RIGHT) )
		{
			gc_Lazer[PLAYER1_EFFECT].pos = gc_Player[PLAYER_1].GetPosition() ;
			MV1SetRotationXYZ( gc_Lazer[PLAYER1_EFFECT].GetModelHandle() , VGet( 0.0f , 1.57f * 0 , 0.0f ) ) ;
			pos.x += 75 ;
			pos.y += 100 ;
			MV1SetPosition( gc_Lazer[PLAYER1_EFFECT].GetModelHandle(), pos ) ;
		}

		if ( ( (gc_Player[PLAYER_1].GetDirection() == D_DOWN) && (gc_Player[PLAYER_1].GetDirinfo() == D_LEFT) ) || (gc_Player[PLAYER_1].GetDirection() == D_LEFT) )
		{
			gc_Lazer[PLAYER1_EFFECT].pos = gc_Player[PLAYER_1].GetPosition() ;
			MV1SetRotationXYZ( gc_Lazer[PLAYER1_EFFECT].GetModelHandle() , VGet( 0.0f , 1.57f * 2.0f , 0.0f ) ) ;
			pos.x -= 75 ;
			pos.y += 100 ;
			MV1SetPosition( gc_Lazer[PLAYER1_EFFECT].GetModelHandle(), pos ) ;
		}
	}

	// --- プレイヤー２の時
	if ( Eno == PLAYER_2 )
	{
		if ( ( (gc_Player[PLAYER_2].GetDirection() == D_DOWN) && (gc_Player[PLAYER_2].GetDirinfo() == D_RIGHT) ) || (gc_Player[PLAYER_2].GetDirection() == D_RIGHT) )
		{
			gc_Lazer[PLAYER2_EFFECT].pos = gc_Player[PLAYER_2].GetPosition() ;
			MV1SetRotationXYZ( gc_Lazer[PLAYER2_EFFECT].GetModelHandle() , VGet( 0.0f , 1.57f * 2 , 0.0f ) ) ;
			pos.x += 75 ;
			pos.y += 100 ;
			MV1SetPosition( gc_Lazer[PLAYER2_EFFECT].GetModelHandle(), pos ) ;
		}
		if ( ( (gc_Player[PLAYER_2].GetDirection() == D_DOWN) && (gc_Player[PLAYER_2].GetDirinfo() == D_LEFT) ) || (gc_Player[PLAYER_2].GetDirection() == D_LEFT) )
		{
			gc_Lazer[PLAYER2_EFFECT].pos = gc_Player[PLAYER_2].GetPosition() ;
			MV1SetRotationXYZ( gc_Lazer[PLAYER2_EFFECT].GetModelHandle() , VGet( 0.0f , 1.57f * 2 , 0.0f ) ) ;
			pos.x -= 75 ;
			pos.y += 100 ;
			MV1SetPosition( gc_Lazer[PLAYER2_EFFECT].GetModelHandle(), pos ) ;
		}
	}

	return( TRUE ) ;
}

// --- モデルハンドルと位置の取得
int Lazer::GetModelHandle( void ){ return hEModel ; }




