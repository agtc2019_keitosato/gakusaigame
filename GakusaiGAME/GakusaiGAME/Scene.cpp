/* ============================================================ +/ 

	LastEdit : 2020/09/16		NAME:佐藤圭桃
	LastEdit : ----/--/--		NAME:
	LastEdit : 2020/10/14		NAME:宮崎櫻
	___________________________________________________________

		Scene.h
	___________________________________________________________

		シーン管理

/+ ============================================================ */

#include "Common.h"

/* -------------------------------- */
/*		スタート関数				*/
/* -------------------------------- */
void SceneMain()
{
	switch( gv_Gamemode )
	{
		case GM_Start		:		Scene_Start();		break ;
		case GM_CharSelect	:		Scene_CharSelect(); break ;
		case GM_GameInitSet	:		Scene_GameInitSet() ;	break ;
		case GM_Playgame	:		Scene_Playgame();	break ;
		case GM_Result		:		Scene_Result();		break ;
		case GM_End			:		Scene_End();		break ;
	}
}

int scenefade = 0 ;
BOOL fadeflg = FALSE ;
int	gv_hSelectModel_S[2] = { 0, 0 } ;						// --- キャラクター選択用モデルハンドル
int	gv_hSelectModel_M[2] = { 0, 0 } ;						// --- キャラクター選択用モデルハンドル
int	gv_hSelectModel_U[2] = { 0, 0 } ;						// --- キャラクター選択用モデルハンドル
unsigned char blinkflg = 0x00 ;								// --- 点滅フラグ
unsigned char blinktime = 0x20 ;							// --- 点滅間隔
int Scene_Start()
{
	// --- スタート画面のサウンドを再生
	while( CheckSoundMem( gc_Main_Sound[0].GetModelHandle() ) == 0 ) {
		PlaySoundMem( gc_Main_Sound[0].GetModelHandle() , DX_PLAYTYPE_LOOP , TRUE ) ;
	}

	DrawGraph( 0, 0, gv_gnbackimg, FALSE ) ;
	DrawGraph( 0, 0, gv_titleimg, TRUE ) ;
	blinkflg++ ;
	if ( blinkflg & blinktime ){
		DrawGraph( (gv_win_width/2)-640, 900, gv_anybuttonimg, TRUE ) ;
	}

	if( (gv_PadStatus[PLAYER_1] >= 1) || (fadeflg == TRUE) )
	{
		blinktime = 0x08 ;
		fadeflg = TRUE ;
		scenefade += 5 ;
		SetDrawBlendMode( DX_BLENDMODE_ALPHA, scenefade ) ;
		DrawGraph( 0, 0, gv_gnfadeimg, FALSE );
		SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;

	// --- スタートボタン押下のサウンド
	while( CheckSoundMem( gc_Main_Sound[4].GetModelHandle() ) == 0 ) {
		PlaySoundMem( gc_Main_Sound[4].GetModelHandle(), DX_PLAYTYPE_BACK ) ;
	}
	// --- スタート画面のサウンドを止める
	StopSoundMem(gc_Main_Sound[0].GetModelHandle() ) ;

	if ( scenefade >= 255 )
		{
			gv_hSelectModel_S[PLAYER_1] = MV1LoadModel( PATH_STANDERD_MODEL ) ;
			gv_hSelectModel_U[PLAYER_1] = MV1LoadModel( PATH_USAGI_MODEL ) ;
			gv_hSelectModel_M[PLAYER_1] = MV1LoadModel( PATH_MASSIVE_MODEL ) ;

			gv_hSelectModel_S[PLAYER_2] = MV1LoadModel( PATH_STANDERD_MODEL ) ;
			gv_hSelectModel_U[PLAYER_2] = MV1LoadModel( PATH_USAGI_MODEL ) ;
			gv_hSelectModel_M[PLAYER_2] = MV1LoadModel( PATH_MASSIVE_MODEL ) ;

			gv_Gamemode = GM_CharSelect ;		// --- キャラセレクト画面へ移動
		}
	}

	return TRUE ;
}

int modelno[MAX_PLAYER] = { STANDERD_NUMBER, STANDERD_NUMBER } ;
int selecthModel[MAX_PLAYER] ;
POINT selectarrowL[MAX_PLAYER] = { {175, 175}, {175, 175} } ;
POINT selectarrowR[MAX_PLAYER] = { {175, 175}, {175, 175} } ;
int selecttim[MAX_PLAYER] = { 0, 0 } ;
int selectoktim[MAX_PLAYER] = { 0, 0 } ;
int selectyestim[MAX_PLAYER] = { 0, 0 } ;
TCHAR selectcharflg[MAX_PLAYER] = { 0x00, 0x00 } ;
float PosRot[MAX_PLAYER] = { 0.0f, 0.0f } ;
/* ------------------------------------------------------------ +/

	Scene_CharSelect	キャラ選択

	selectcharflg -> 0x01 = キャラ選択時左ボタン押下
					 0x02 = キャラ選択時右ボタン押下
					 0x04 = ×ボタン押下
					 0x08 = 〇ボタン押下
					 0x10 = キャラ選択完了フラグ
					 0x20 = ＹＥＳ選択時
					 0x80 = 準備完了フラグ

 + ------------------------------------------------------------ +
		-引数-
			void
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Scene_CharSelect()
{
	if( CheckHitKey( KEY_INPUT_F5 ) > 0 )
	{
		selectcharflg[PLAYER_2] |= 0xf0 ;
	}
	/* +++ キャラ選択 +++ */
	DrawGraph( 0, 0, gv_gnselectbackimg, FALSE ) ;

	gc_Player[PLAYER_1].SelectPlayer( PLAYER_1 ) ;
	gc_Player[PLAYER_2].SelectPlayer( PLAYER_2 ) ;

	// --- キャラクター選択のサウンドを再生
	ChangeVolumeSoundMem( 255 / 2, gc_Main_Sound[1].GetModelHandle() ) ;
	while( CheckSoundMem( gc_Main_Sound[1].GetModelHandle() ) == 0 ) {
		PlaySoundMem( gc_Main_Sound[1].GetModelHandle(), DX_PLAYTYPE_LOOP ) ;
	}

	// --- 1P用モデル表示
	switch( modelno[PLAYER_1] )
	{
		case STANDERD_NUMBER :
			selecthModel[PLAYER_1] = STANDERD_NUMBER ;
			MV1SetPosition( gv_hSelectModel_S[PLAYER_1], VGet( -110, -10, -140 ) ) ;
			MV1SetRotationXYZ( gv_hSelectModel_S[PLAYER_1], VGet(0.0f, PosRot[PLAYER_1], 0.0f) ) ;
			MV1DrawModel( gv_hSelectModel_S[PLAYER_1] ) ;
			break ;

		case MASSIVE_NUMBER :
			selecthModel[PLAYER_1] = MASSIVE_NUMBER ;
			MV1SetPosition( gv_hSelectModel_M[PLAYER_1], VGet( -110, -10, -140 ) ) ;
			MV1SetRotationXYZ( gv_hSelectModel_M[PLAYER_1], VGet(0.0f, PosRot[PLAYER_1], 0.0f) ) ;
			MV1DrawModel( gv_hSelectModel_M[PLAYER_1] ) ;
			break ;

		case USAGI_NUMBER :
			selecthModel[PLAYER_1] = USAGI_NUMBER ;
			MV1SetPosition( gv_hSelectModel_U[PLAYER_1], VGet( -110, -10, -140 ) ) ;
			MV1SetRotationXYZ( gv_hSelectModel_U[PLAYER_1], VGet(0.0f, PosRot[PLAYER_1], 0.0f) ) ;
			MV1DrawModel( gv_hSelectModel_U[PLAYER_1] ) ;
			break ;
	}

	// --- 2P用モデル表示
	switch( modelno[PLAYER_2] )
	{
		case STANDERD_NUMBER :
			selecthModel[PLAYER_2] = STANDERD_NUMBER ;
			MV1SetPosition( gv_hSelectModel_S[PLAYER_2], VGet( 110, -10, -140) ) ;
			MV1SetRotationXYZ( gv_hSelectModel_S[PLAYER_2], VGet(0.0f, PosRot[PLAYER_2], 0.0f) ) ;
			MV1DrawModel( gv_hSelectModel_S[PLAYER_2] ) ;
			break ;

		case MASSIVE_NUMBER :
			selecthModel[PLAYER_2] = MASSIVE_NUMBER ;
			MV1SetPosition( gv_hSelectModel_M[PLAYER_2], VGet( 110, -10, -140 ) ) ;
			MV1SetRotationXYZ( gv_hSelectModel_M[PLAYER_2], VGet(0.0f, PosRot[PLAYER_2], 0.0f) ) ;
			MV1DrawModel( gv_hSelectModel_M[PLAYER_2] ) ;
			break ;

		case USAGI_NUMBER :
			selecthModel[PLAYER_2] = USAGI_NUMBER ;
			MV1SetPosition( gv_hSelectModel_U[PLAYER_2], VGet( 110, -10, -140 ) ) ;
			MV1SetRotationXYZ( gv_hSelectModel_U[PLAYER_2], VGet(0.0f, PosRot[PLAYER_2], 0.0f) ) ;
			MV1DrawModel( gv_hSelectModel_U[PLAYER_2] ) ;
			break ;
	}

	if ( scenefade >= 0 )
	{
		scenefade -= 3 ;
		SetDrawBlendMode( DX_BLENDMODE_ALPHA, scenefade ) ;
		DrawGraph( 0, 0, gv_gnfadeimg, FALSE );
		SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;
	}
	else
	{
		// --- 1P
		if ( (selectcharflg[PLAYER_1] & 0x10) == FALSE )
		{
			PosRot[PLAYER_1] += 0.02f ;
			if ( PosRot[PLAYER_1] >= 6.3f )
			{
				PosRot[PLAYER_1] = 0.0f ;
			}

			if ( (selectcharflg[PLAYER_1] & 0x03) == FALSE )		// --- ＬＲボタンフラグチェック
			{
				if ( gv_PadStatus[PLAYER_1] & PAD_INPUT_LEFT ){		selectcharflg[PLAYER_1] |= 0x01 ;	}
				if ( gv_PadStatus[PLAYER_1] & PAD_INPUT_RIGHT ){	selectcharflg[PLAYER_1] |= 0x02 ;	}
			}
			else
			{
				// --- 左右キータイムラグ(加算)
				selecttim[PLAYER_1]++ ;

	// --- 左右移動時のサウンド
	while( CheckSoundMem( gc_Sound[PLAYER_1][14].GetModelHandle() ) == 0 ) {
	PlaySoundMem( gc_Sound[PLAYER_1][14].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE ) ;
	}
				// --- 左右キータイムラグチェック
				if(selecttim[PLAYER_1] >= 20)
				{
					// --- モデルチェンジ
					if( selectcharflg[PLAYER_1] & 0x01 )
					{
						modelno[PLAYER_1]++ ;
						PosRot[PLAYER_1] = 5.5f ;
					}
					if( selectcharflg[PLAYER_1] & 0x02 )
					{
						modelno[PLAYER_1]-- ;
						PosRot[PLAYER_1] = 5.5f ;
					}

					if ( modelno[PLAYER_1] > 3 ){	modelno[PLAYER_1] = 1 ;	}
					if ( modelno[PLAYER_1] < 1 ){	modelno[PLAYER_1] = 3 ;	}
					selecttim[PLAYER_1] = 0 ;
					selectcharflg[PLAYER_1] &= 0x0c ;
				}

				if( (selectcharflg[PLAYER_1] & 0x01) && (selectarrowL[PLAYER_1].x < 213) )
				{
					selectarrowL[PLAYER_1].x += 2 ;
					selectarrowL[PLAYER_1].y += 2 ;
				}
				if( (selectcharflg[PLAYER_1] & 0x02) && (selectarrowR[PLAYER_1].x < 213) )
				{
					selectarrowR[PLAYER_1].x += 2 ;
					selectarrowR[PLAYER_1].y += 2 ;
				}
			}

			// --- 矢印(元に戻る)左
			if( ((selectcharflg[PLAYER_1] & 0x01) == FALSE) && (selectarrowL[PLAYER_1].x > 175) )
			{
				selectarrowL[PLAYER_1].x -= 2 ;
				selectarrowL[PLAYER_1].y -= 2 ;
			}
			// --- 矢印(元に戻る)右
			if( ((selectcharflg[PLAYER_1] & 0x02) == FALSE) && (selectarrowR[PLAYER_1].x > 175) )
			{
				selectarrowR[PLAYER_1].x -= 2 ;
				selectarrowR[PLAYER_1].y -= 2 ;
			}
			// --- 横やじるし[1P]
			DrawExtendGraph( 55, 456, 55 + selectarrowL[PLAYER_1].x, 456 + selectarrowL[PLAYER_1].y, gv_selectarrowLimg[PLAYER_1], TRUE  ) ;
			DrawExtendGraph( 670, 456, 670 + selectarrowR[PLAYER_1].x, 456 + selectarrowR[PLAYER_1].y, gv_selectarrowRimg[PLAYER_1], TRUE  ) ;
		}
		else
		{
			PosRot[PLAYER_1] = 0.0f ;
		}

		// --- 2P
		if ( (selectcharflg[PLAYER_2] & 0x10) == FALSE )
		{
			PosRot[PLAYER_2] += 0.02f ;
			if ( PosRot[PLAYER_2] >= 6.3f )
			{
				PosRot[PLAYER_2] = 0.0f ;
			}

			if ( (selectcharflg[PLAYER_2] & 0x03) == FALSE )
			{
				if ( gv_PadStatus[PLAYER_2] & PAD_INPUT_LEFT ){		selectcharflg[PLAYER_2] |= 0x01 ;	}
				if ( gv_PadStatus[PLAYER_2] & PAD_INPUT_RIGHT ){	selectcharflg[PLAYER_2] |= 0x02 ;	}
			}
			else
			{
				// --- 左右キータイムラグ(加算)
				selecttim[PLAYER_2]++ ;

				// --- 左右移動時のサウンド
				while( CheckSoundMem( gc_Sound[PLAYER_2][14].GetModelHandle() ) == 0 ) {
				PlaySoundMem( gc_Sound[PLAYER_2][14].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE ) ;
}
				// --- 左右キータイムラグチェック
				if(selecttim[PLAYER_2] >= 20)
				{
					// --- モデルチェンジ
					if( (selectcharflg[PLAYER_2] & 0x01) )
					{
						modelno[PLAYER_2]++ ;
						PosRot[PLAYER_2] = 5.5f ;
					}
					if( (selectcharflg[PLAYER_2] & 0x02) )
					{
						modelno[PLAYER_2]-- ;
						PosRot[PLAYER_2] = 5.5f ;
					}

					if ( modelno[PLAYER_2] > 3 ){	modelno[PLAYER_2] = 1 ;	}
					if ( modelno[PLAYER_2] < 1 ){	modelno[PLAYER_2] = 3 ;	}
					selecttim[PLAYER_2] = 0 ;
					selectcharflg[PLAYER_2] &= 0x0c ;
				}

				if( selectcharflg[PLAYER_2] & 0x01 && (selectarrowL[PLAYER_2].x < 213) )
				{
					selectarrowL[PLAYER_2].x += 2 ;
					selectarrowL[PLAYER_2].y += 2 ;
				}
				if( selectcharflg[PLAYER_2] & 0x02 && (selectarrowR[PLAYER_2].x < 213) )
				{
					selectarrowR[PLAYER_2].x += 2 ;
					selectarrowR[PLAYER_2].y += 2 ;
				}
			}

			// --- 矢印(元に戻る)左
			if( ((selectcharflg[PLAYER_2] & 0x01) == FALSE) && (selectarrowL[PLAYER_2].x > 175) )
			{
				selectarrowL[PLAYER_2].x -= 2 ;
				selectarrowL[PLAYER_2].y -= 2 ;
			}
			// --- 矢印(元に戻る)右
			if( ((selectcharflg[PLAYER_2] & 0x02) == FALSE) && (selectarrowR[PLAYER_2].x > 175) )
			{
				selectarrowR[PLAYER_2].x -= 2 ;
				selectarrowR[PLAYER_2].y -= 2 ;
			}
			// --- 横やじるし[2P]
			DrawExtendGraph( 1075, 456, 1075 + selectarrowL[PLAYER_2].x, 456 + selectarrowL[PLAYER_2].y, gv_selectarrowLimg[PLAYER_2], TRUE  ) ;
			DrawExtendGraph( 1690, 456, 1690 + selectarrowR[PLAYER_2].x, 456 + selectarrowR[PLAYER_2].y, gv_selectarrowRimg[PLAYER_2], TRUE  ) ;
		}
		else
		{
			PosRot[PLAYER_2] = 0.0f ;
		}

		if( (selectcharflg[PLAYER_1] & 0x0c) == FALSE )
		{
			// --- 〇ボタン
			if( gv_PadStatus[PLAYER_1] & PAD_CIRCLE ){	
				selectcharflg[PLAYER_1] |= 0x08 ;

// --- プレイヤー１の〇ボタン時のサウンド
if ( selectcharflg[PLAYER_1] == 0x08 )
{
	while( CheckSoundMem( gc_Sound[PLAYER_1][15].GetModelHandle() ) == 0 ) {
	PlaySoundMem( gc_Sound[PLAYER_1][15].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE ) ;
	}
}
			}
			// --- ×ボタン
			if( gv_PadStatus[PLAYER_1] & PAD_CROSS ){
				selectcharflg[PLAYER_1] |= 0x04 ;	

// --- プレイヤー１の×ボタン時のサウンド
if ( selectcharflg[PLAYER_1] != 0x04 )
{
	while( CheckSoundMem( gc_Sound[PLAYER_1][17].GetModelHandle() ) == 0 ) {
	PlaySoundMem( gc_Sound[PLAYER_1][17].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE ) ;
	}
}
			}
		}
		if( (selectcharflg[PLAYER_2] & 0x0c) == FALSE )
		{
			// --- 〇ボタン
			if( gv_PadStatus[PLAYER_2] & PAD_CIRCLE ){
				selectcharflg[PLAYER_2] |= 0x08 ;	

	// --- プレイヤー２の◯ボタン時のサウンド
	if ( selectcharflg[PLAYER_2] == 0x08 )
	{
		while( CheckSoundMem( gc_Sound[PLAYER_2][15].GetModelHandle() ) == 0 ) {
		PlaySoundMem( gc_Sound[PLAYER_2][15].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE ) ;
		}
	}
			}			
			// --- ×ボタン
			if( gv_PadStatus[PLAYER_2] & PAD_CROSS ){
				selectcharflg[PLAYER_2] |= 0x04 ;	

				// --- プレイヤー２の×ボタン時のサウンド
				if ( selectcharflg[PLAYER_2] != 0x04 )
				{
					while( CheckSoundMem( gc_Sound[PLAYER_2][17].GetModelHandle() ) == 0 ) {
					PlaySoundMem( gc_Sound[PLAYER_2][17].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE ) ;
					}
				}
			}
		}

		// --- 1P 
		if( selectcharflg[PLAYER_1] & 0x10 ){	DrawGraph( 0, 0, gv_selectok1P, TRUE );	}	// --- ＯＫの表示
		else{ 	DrawGraph( 340, -50, gv_selectplayer[PLAYER_1], TRUE );		}					// --- プレイヤー番号表示	

		// --- 2P
		if( selectcharflg[PLAYER_2] & 0x10 ){	DrawGraph( 1370, 0, gv_selectok2P, TRUE );}		// --- ＯＫの表示
		else{	DrawGraph( 1300, -50, gv_selectplayer[PLAYER_2], TRUE );	}					// --- プレイヤー番号表示

		// --- 
		if( ((selectcharflg[PLAYER_1] & 0x10) == FALSE) 
										|| ((selectcharflg[PLAYER_2] & 0x10) == FALSE) )
		{
			if( selectyestim[PLAYER_1] != 0 )
			{
				selectyestim[PLAYER_1]++ ;
				selectcharflg[PLAYER_1] &= 0xf3 ;
				if(selectyestim[PLAYER_1] >= 5){	selectyestim[PLAYER_1] = 0 ;	}
			}
			if( selectyestim[PLAYER_2] != 0 )
			{
				selectyestim[PLAYER_2]++ ;
				selectcharflg[PLAYER_2] &= 0xf3 ;
				if(selectyestim[PLAYER_2] >= 5){	selectyestim[PLAYER_2] = 0 ;	}
			}

			// --- ＯＫフラグ[1P]
			if(selectoktim[PLAYER_1] == 0)
			{
				if ( selectcharflg[PLAYER_1] & 0x08 )
				{
					selectcharflg[PLAYER_1] |= 0x10 ;					
				}
				else if( selectcharflg[PLAYER_1] & 0x04 )
				{
					selectcharflg[PLAYER_1] &= 0xef ;
				}
			}
			// --- ＯＫフラグ[2P]
			if(selectoktim[PLAYER_2] == 0)
			{
				if ( selectcharflg[PLAYER_2] & 0x08 )
				{
					selectcharflg[PLAYER_2] |= 0x10 ;
					
				}
				if( selectcharflg[PLAYER_2] & 0x04 )
				{
					selectcharflg[PLAYER_2] &= 0xef ;
				}
			}

			selectoktim[PLAYER_1]++ ;
			if(selectoktim[PLAYER_1] >= 10)
			{
				selectoktim[PLAYER_1] = 0 ;
				selectcharflg[PLAYER_1] &= 0xf3 ;
			}

			selectoktim[PLAYER_2]++ ;
			if(selectoktim[PLAYER_2] >= 10)
			{
				selectoktim[PLAYER_2] = 0 ;
				selectcharflg[PLAYER_2] &= 0xf3 ;
			}
		}
		else	// --- プレイヤーが二人ともＯＫになったら
		{
			if( selectoktim[PLAYER_1] != 0 )
			{
				selectoktim[PLAYER_1]++ ;
				selectcharflg[PLAYER_1] &= 0xf3 ;
				if(selectoktim[PLAYER_1] >= 10){	selectoktim[PLAYER_1] = 0 ;	}
			}
			if( selectoktim[PLAYER_2] != 0 )
			{
				selectoktim[PLAYER_2]++ ;
				selectcharflg[PLAYER_2] &= 0xf3 ;
				if(selectoktim[PLAYER_2] >= 10){	selectoktim[PLAYER_2] = 0 ;	}
			}

			DrawGraph( gv_win_width / 2 - 370, 30, gv_selectready, TRUE ) ;			// --- ＲＥＡＤＹ
			DrawGraph( 760, 280, gv_selectyesno, TRUE ) ;							// --- ＹＥＳＮＯ

			// --- 上キー
			if( gv_PadStatus[PLAYER_1] & PAD_UP ){
				selectcharflg[PLAYER_1] |= 0x20 ;

				// --- プレイヤー１の上キー時のサウンド
				while( CheckSoundMem( gc_Sound[PLAYER_1][19].GetModelHandle() ) == 0 ) {
					PlaySoundMem( gc_Sound[PLAYER_1][19].GetModelHandle(), DX_PLAYTYPE_BACK ) ;
				}
			}
			// --- 下キー
			if( gv_PadStatus[PLAYER_1] & PAD_DOWN ){
				selectcharflg[PLAYER_1] &= 0xdf ;	
				// --- プレイヤー１の下キー時のサウンド
				while( CheckSoundMem( gc_Sound[PLAYER_1][19].GetModelHandle() ) == 0 ) {
					PlaySoundMem( gc_Sound[PLAYER_1][19].GetModelHandle(), DX_PLAYTYPE_BACK ) ;
				}
			}

			// --- 上キー
			if( gv_PadStatus[PLAYER_2] & PAD_UP ){
				selectcharflg[PLAYER_2] |= 0x20 ;
				// --- プレイヤー２の上キー時のサウンド
				while( CheckSoundMem( gc_Sound[PLAYER_2][19].GetModelHandle() ) == 0 ) {
					PlaySoundMem( gc_Sound[PLAYER_2][19].GetModelHandle(), DX_PLAYTYPE_BACK ) ;
				}
			}
			// --- 下キー
			if( gv_PadStatus[PLAYER_2] & PAD_DOWN ){
				selectcharflg[PLAYER_2] &= 0xdf ;
				// --- プレイヤー２の下キー時のサウンド
				while( CheckSoundMem( gc_Sound[PLAYER_2][19].GetModelHandle() ) == 0 ) {
					PlaySoundMem( gc_Sound[PLAYER_2][19].GetModelHandle(), DX_PLAYTYPE_BACK ) ;
				}
			}

			if( selectcharflg[PLAYER_1] & 0x20 ){	DrawGraph( 695, 252, gv_selectcursor1P, TRUE );		}		// --- YES 1P
			else{	DrawGraph( 695, 602, gv_selectcursor1P, TRUE );	}											// --- NO 1P

			if( selectcharflg[PLAYER_2] & 0x20 ){	DrawGraph( 1095, 252, gv_selectcursor2P, TRUE );	}		// --- YES 2P
			else{	DrawGraph( 1095, 602, gv_selectcursor2P, TRUE );}											// --- NO 2P

			// --- 〇ボタンが押されていたら[1P]
			if( selectcharflg[PLAYER_1] & 0x0c )
			{
				if(selectyestim[PLAYER_1] == 0)
				{
					if( selectcharflg[PLAYER_1] & 0x08 )
					{
						if( selectcharflg[PLAYER_1] & 0x20 )
						{
							selectcharflg[PLAYER_1] |= 0x80 ;
						}
						else
						{
							selectcharflg[PLAYER_1] &= 0xcf ;
							selectcharflg[PLAYER_2] &= 0xcf ;
						}
					}
				}
			}

			// --- 〇ボタンが押されていたら[2P]
			if( selectcharflg[PLAYER_2] & 0x0c )
			{
				if(selectyestim[PLAYER_2] == 0)
				{
					if( selectcharflg[PLAYER_2] & 0x08 )
					{
						if( selectcharflg[PLAYER_2] & 0x20 )
						{
							selectcharflg[PLAYER_2] |= 0x80 ;
						}
						else
						{
							selectcharflg[PLAYER_1] &= 0xcf ;
							selectcharflg[PLAYER_2] &= 0xcf ;
						}
					}
				}
			}

			// --- キータイムラグ[1P]
			selectyestim[PLAYER_1]++ ;
			if(selectyestim[PLAYER_1] >= 10)
			{
				selectyestim[PLAYER_1] = 0 ;
				selectcharflg[PLAYER_1] &= 0xf3 ;
			}
			// --- キータイムラグ[2P]
			selectyestim[PLAYER_2]++ ;
			if(selectyestim[PLAYER_2] >= 10)
			{
				selectyestim[PLAYER_2] = 0 ;
				selectcharflg[PLAYER_2] &= 0xf3 ;
			}
		}
	}

	if( (selectcharflg[PLAYER_1] & 0x80) && (selectcharflg[PLAYER_2] & 0x80) )
	{
		// --- プレイヤー１、２が両方共YESの時のサウンド
		while( CheckSoundMem( gc_Main_Sound[5].GetModelHandle() ) == 0 ) {
			PlaySoundMem( gc_Main_Sound[5].GetModelHandle(), DX_PLAYTYPE_BACK ) ;
		}
		// --- キャラクター選択のサウンドを止める
		StopSoundMem(gc_Main_Sound[1].GetModelHandle() ) ;
		gv_Gamemode = GM_GameInitSet ;
	}
	return TRUE ;
}

int Scene_GameInitSet()
{
	switch( selecthModel[PLAYER_1] )
	{
		case STANDERD_NUMBER :
			gc_Player[PLAYER_1].SetModelHandle( PATH_STANDERD_MODEL , PATH_STANDERD_MODELWITHSWORD ) ;	// --- プレイヤー１のモデルパス
			gc_Player[PLAYER_1].SetModelNumber( STANDERD_NUMBER ) ;										// --- プレイヤー１のモデルナンバー
			gc_Player[PLAYER_1].SetModelWidth( STANDERD_WIDTH ) ;

			// --- アニメーション
			SetAnimation( PLAYER_1, A_RUN_1P, PATH_ANIM_RUN_STANDERD ) ;					// --- スタンダード　移動
			SetAnimation( PLAYER_1, A_STAYL_1P, PATH_ANIM_STAYL_STANDERD ) ;				// --- スタンダード　左待機
			SetAnimation( PLAYER_1, A_STAYR_1P, PATH_ANIM_STAYR_STANDERD ) ;				// ---			 右待機
			SetAnimation( PLAYER_1, A_JUMPL_1P, PATH_ANIM_JUMPL_STANDERD ) ;
			SetAnimation( PLAYER_1, A_JUMPR_1P, PATH_ANIM_JUMPR_STANDERD ) ;
			SetAnimation( PLAYER_1, A_HATTACKL_1P, PATH_ANIM_HATTACKL_STANDERD ) ;
			SetAnimation( PLAYER_1, A_HATTACKR_1P, PATH_ANIM_HATTACKR_STANDERD ) ;
			SetAnimation( PLAYER_1, A_LATTACKL_1P, PATH_ANIM_LATTACKL_STANDERD ) ;
			SetAnimation( PLAYER_1, A_LATTACKR_1P, PATH_ANIM_LATTACKR_STANDERD ) ;
			SetAnimation( PLAYER_1, A_GUARDL_1P, PATH_ANIM_GUARDL_STANDERD ) ;
			SetAnimation( PLAYER_1, A_GUARDR_1P, PATH_ANIM_GUARDR_STANDERD ) ;
			SetAnimation( PLAYER_1, A_FIGHTENEDL_1P, PATH_ANIM_FIGHTENEDL_STANDERD ) ;
			SetAnimation( PLAYER_1, A_FIGHTENEDR_1P, PATH_ANIM_FIGHTENEDR_STANDERD ) ;
			SetAnimation( PLAYER_1, A_DOWNL_1P, PATH_ANIM_DOWNL_STANDERD ) ;
			SetAnimation( PLAYER_1, A_DOWNR_1P, PATH_ANIM_DOWNR_STANDERD ) ;
			SetAnimation( PLAYER_1, A_BOOSTL_1P, PATH_ANIM_BOOSTL_STANDERD ) ;
			SetAnimation( PLAYER_1, A_BOOSTR_1P, PATH_ANIM_BOOSTR_STANDERD ) ;

			gc_Player[PLAYER_1].SetGdflg( TRUE ) ;
			SetAnimation( PLAYER_1, A_GREATDEATHL_1P, PATH_ANIM_GREATDEATHL_STANDERD ) ;	// --- マッシブ　大必殺左
			SetAnimation( PLAYER_1, A_GREATDEATHR_1P, PATH_ANIM_GREATDEATHR_STANDERD ) ;	// ---			 大必殺右
			gc_Player[PLAYER_1].SetGdflg( FALSE ) ;

			gc_winlose[PLAYER_1].SetModelHandle( PATH_STANDERD_MODEL ) ;	// --- プレイヤー１のモデルパス
			SetWinloseAnimation( PLAYER_1, A_WIN_PLAY1, PATH_ANIM_HATTACKR_STANDERD ) ;
			SetWinloseAnimation( PLAYER_1, A_LOSE_PLAY1, PATH_ANIM_DOWNR_STANDERD ) ;
			break ;

		case MASSIVE_NUMBER :
			// --- メッシュ
			gc_Player[PLAYER_1].SetModelHandle( PATH_MASSIVE_MODEL , PATH_MASSIVE_MODELWITHSWORD ) ;	// --- プレイヤー１のモデルパス
			gc_Player[PLAYER_1].SetModelNumber( MASSIVE_NUMBER ) ;										// --- プレイヤー１のモデルナンバー
			gc_Player[PLAYER_1].SetModelWidth( MASSIVE_WIDTH ) ;

			// --- アニメーション
			SetAnimation( PLAYER_1, A_RUN_1P, PATH_ANIM_RUN_MASSIVE ) ;					// --- マッシブ　移動
			SetAnimation( PLAYER_1, A_STAYL_1P, PATH_ANIM_STAYL_MASSIVE ) ;				// --- マッシブ　左待機
			SetAnimation( PLAYER_1, A_STAYR_1P, PATH_ANIM_STAYR_MASSIVE ) ;				// ---			 右待機
			SetAnimation( PLAYER_1, A_JUMPL_1P, PATH_ANIM_JUMPL_MASSIVE ) ;				// --- マッシブ　左ジャンプ
			SetAnimation( PLAYER_1, A_JUMPR_1P, PATH_ANIM_JUMPR_MASSIVE ) ;				// ---			 右ジャンプ
			SetAnimation( PLAYER_1, A_HATTACKL_1P, PATH_ANIM_HATTACKL_MASSIVE ) ;		// --- マッシブ　攻撃中左
			SetAnimation( PLAYER_1, A_HATTACKR_1P, PATH_ANIM_HATTACKR_MASSIVE ) ;		// ---			 攻撃中右
			SetAnimation( PLAYER_1, A_LATTACKL_1P, PATH_ANIM_LATTACKL_MASSIVE ) ;		// --- マッシブ　脚攻撃中左
			SetAnimation( PLAYER_1, A_LATTACKR_1P, PATH_ANIM_LATTACKR_MASSIVE ) ;		// ---			 脚攻撃中右
			SetAnimation( PLAYER_1, A_GUARDL_1P, PATH_ANIM_GUARDL_MASSIVE ) ;			// --- マッシブ　ガード左
			SetAnimation( PLAYER_1, A_GUARDR_1P, PATH_ANIM_GUARDR_MASSIVE ) ;			// ---			 ガード右
			SetAnimation( PLAYER_1, A_FIGHTENEDL_1P, PATH_ANIM_FIGHTENEDL_MASSIVE ) ;	// --- マッシブ　ひるみ左
			SetAnimation( PLAYER_1, A_FIGHTENEDR_1P, PATH_ANIM_FIGHTENEDR_MASSIVE ) ;	// ---			 ひるみ右
			SetAnimation( PLAYER_1, A_DOWNL_1P, PATH_ANIM_DOWNL_MASSIVE ) ;	// --- マッシブ　ダウン開始左
			SetAnimation( PLAYER_1, A_DOWNR_1P, PATH_ANIM_DOWNR_MASSIVE ) ;	// ---			 ダウン開始右
			SetAnimation( PLAYER_1, A_BOOSTL_1P, PATH_ANIM_BOOSTL_MASSIVE ) ;
			SetAnimation( PLAYER_1, A_BOOSTR_1P, PATH_ANIM_BOOSTR_MASSIVE ) ;

			gc_Player[PLAYER_1].SetGdflg( TRUE ) ;
			SetAnimation( PLAYER_1, A_GREATDEATHL_1P, PATH_ANIM_GREATDEATHL_MASSIVE ) ;	// --- マッシブ　大必殺左
			SetAnimation( PLAYER_1, A_GREATDEATHR_1P, PATH_ANIM_GREATDEATHR_MASSIVE ) ;	// ---			 大必殺右
			gc_Player[PLAYER_1].SetGdflg( FALSE ) ;

			gc_winlose[PLAYER_1].SetModelHandle( PATH_MASSIVE_MODEL ) ;	// --- プレイヤー１のモデルパス
			SetWinloseAnimation( PLAYER_1, A_WIN_PLAY1, PATH_ANIM_HATTACKR_MASSIVE ) ;
			SetWinloseAnimation( PLAYER_1, A_LOSE_PLAY1, PATH_ANIM_DOWNR_MASSIVE ) ;
			break ;

		case USAGI_NUMBER :
			gc_Player[PLAYER_1].SetModelHandle( PATH_USAGI_MODEL , PATH_USAGI_MODELWITHSWORD ) ;	// --- プレイヤー１のモデルパス
			gc_Player[PLAYER_1].SetModelNumber( USAGI_NUMBER ) ;									// --- プレイヤー１のモデルナンバー
			gc_Player[PLAYER_1].SetModelWidth( USAGI_WIDTH ) ;

			// --- アニメーション
			SetAnimation( PLAYER_1, A_RUN_1P, PATH_ANIM_RUN_USAGI ) ;					// --- ウサギ　移動
			SetAnimation( PLAYER_1, A_STAYL_1P, PATH_ANIM_STAYL_USAGI ) ;				// --- ウサギ　左待機
			SetAnimation( PLAYER_1, A_STAYR_1P, PATH_ANIM_STAYR_USAGI ) ;				// ---		   右待機
			SetAnimation( PLAYER_1, A_JUMPL_1P, PATH_ANIM_JUMPL_USAGI ) ;				// ---
			SetAnimation( PLAYER_1, A_JUMPR_1P, PATH_ANIM_JUMPR_USAGI ) ;				// ---
			SetAnimation( PLAYER_1, A_HATTACKL_1P, PATH_ANIM_HATTACKL_USAGI ) ;				// ---
			SetAnimation( PLAYER_1, A_HATTACKR_1P, PATH_ANIM_HATTACKR_USAGI ) ;				// ---
			SetAnimation( PLAYER_1, A_LATTACKL_1P, PATH_ANIM_LATTACKL_USAGI ) ;				// ---
			SetAnimation( PLAYER_1, A_LATTACKR_1P, PATH_ANIM_LATTACKR_USAGI ) ;				// ---
			SetAnimation( PLAYER_1, A_GUARDL_1P, PATH_ANIM_GUARDL_USAGI ) ;				// ---
			SetAnimation( PLAYER_1, A_GUARDR_1P, PATH_ANIM_GUARDR_USAGI ) ;				// ---
			SetAnimation( PLAYER_1, A_FIGHTENEDL_1P, PATH_ANIM_FIGHTENEDL_USAGI ) ;				// ---
			SetAnimation( PLAYER_1, A_FIGHTENEDR_1P, PATH_ANIM_FIGHTENEDR_USAGI ) ;				// ---
			SetAnimation( PLAYER_1, A_DOWNL_1P, PATH_ANIM_DOWNL_USAGI ) ;				// ---
			SetAnimation( PLAYER_1, A_DOWNR_1P, PATH_ANIM_DOWNR_USAGI ) ;				// ---
			SetAnimation( PLAYER_1, A_BOOSTL_1P, PATH_ANIM_BOOSTL_USAGI ) ;
			SetAnimation( PLAYER_1, A_BOOSTR_1P, PATH_ANIM_BOOSTR_USAGI ) ;

			gc_Player[PLAYER_1].SetGdflg( TRUE ) ;
			SetAnimation( PLAYER_1, A_GREATDEATHL_1P, PATH_ANIM_GREATDEATHL_USAGI ) ;	// --- マッシブ　大必殺左
			SetAnimation( PLAYER_1, A_GREATDEATHR_1P, PATH_ANIM_GREATDEATHR_USAGI ) ;	// ---			 大必殺右
			gc_Player[PLAYER_1].SetGdflg( FALSE ) ;

			gc_winlose[PLAYER_1].SetModelHandle( PATH_USAGI_MODEL ) ;	// --- プレイヤー１のモデルパス
			SetWinloseAnimation( PLAYER_1, A_WIN_PLAY1, PATH_ANIM_HATTACKR_USAGI ) ;
			SetWinloseAnimation( PLAYER_1, A_LOSE_PLAY1, PATH_ANIM_DOWNR_USAGI ) ;
			break ;
	}

	switch( selecthModel[PLAYER_2] )
	{
		case STANDERD_NUMBER :
			gc_Player[PLAYER_2].SetModelHandle( PATH_STANDERD_MODEL , PATH_STANDERD_MODELWITHSWORD ) ;	// --- プレイヤー１のモデルパス
			gc_Player[PLAYER_2].SetModelNumber( STANDERD_NUMBER ) ;										// --- プレイヤー１のモデルナンバー
			gc_Player[PLAYER_2].SetModelWidth( STANDERD_WIDTH ) ;

			// --- アニメーション
			SetAnimation( PLAYER_2, A_RUN_2P, PATH_ANIM_RUN_STANDERD ) ;					// --- スタンダード　移動
			SetAnimation( PLAYER_2, A_STAYL_2P, PATH_ANIM_STAYL_STANDERD ) ;				// --- スタンダード　左待機
			SetAnimation( PLAYER_2, A_STAYR_2P, PATH_ANIM_STAYR_STANDERD ) ;				// ---				 Z右待機
			SetAnimation( PLAYER_2, A_JUMPL_2P, PATH_ANIM_JUMPL_STANDERD ) ;
			SetAnimation( PLAYER_2, A_JUMPR_2P, PATH_ANIM_JUMPR_STANDERD ) ;
			SetAnimation( PLAYER_2, A_HATTACKL_2P, PATH_ANIM_HATTACKL_STANDERD ) ;
			SetAnimation( PLAYER_2, A_HATTACKR_2P, PATH_ANIM_HATTACKR_STANDERD ) ;
			SetAnimation( PLAYER_2, A_LATTACKL_2P, PATH_ANIM_LATTACKL_STANDERD ) ;
			SetAnimation( PLAYER_2, A_LATTACKR_2P, PATH_ANIM_LATTACKR_STANDERD ) ;
			SetAnimation( PLAYER_2, A_GUARDL_2P, PATH_ANIM_GUARDL_STANDERD ) ;
			SetAnimation( PLAYER_2, A_GUARDR_2P, PATH_ANIM_GUARDR_STANDERD ) ;
			SetAnimation( PLAYER_2, A_FIGHTENEDL_2P, PATH_ANIM_FIGHTENEDL_STANDERD ) ;
			SetAnimation( PLAYER_2, A_FIGHTENEDR_2P, PATH_ANIM_FIGHTENEDR_STANDERD ) ;
			SetAnimation( PLAYER_2, A_DOWNL_2P, PATH_ANIM_DOWNL_STANDERD ) ;
			SetAnimation( PLAYER_2, A_DOWNR_2P, PATH_ANIM_DOWNR_STANDERD ) ;
			SetAnimation( PLAYER_2, A_BOOSTL_2P, PATH_ANIM_BOOSTL_STANDERD ) ;
			SetAnimation( PLAYER_2, A_BOOSTR_2P, PATH_ANIM_BOOSTR_STANDERD ) ;

			gc_Player[PLAYER_2].SetGdflg( TRUE ) ;
			SetAnimation( PLAYER_2, A_GREATDEATHL_2P, PATH_ANIM_GREATDEATHL_STANDERD ) ;	// --- マッシブ　大必殺左
			SetAnimation( PLAYER_2, A_GREATDEATHR_2P, PATH_ANIM_GREATDEATHR_STANDERD ) ;	// ---			 大必殺右
			gc_Player[PLAYER_2].SetGdflg( FALSE ) ;

			gc_winlose[PLAYER_2].SetModelHandle( PATH_STANDERD_MODEL ) ;	// --- プレイヤー１のモデルパス
			SetWinloseAnimation( PLAYER_2, A_WIN_PLAY2, PATH_ANIM_HATTACKL_STANDERD ) ;
			SetWinloseAnimation( PLAYER_2, A_LOSE_PLAY2, PATH_ANIM_DOWNL_STANDERD ) ;
			break ;

		case MASSIVE_NUMBER :
			// --- メッシュ
			gc_Player[PLAYER_2].SetModelHandle( PATH_MASSIVE_MODEL , PATH_MASSIVE_MODELWITHSWORD ) ;	// --- プレイヤー１のモデルパス
			gc_Player[PLAYER_2].SetModelNumber( MASSIVE_NUMBER ) ;										// --- プレイヤー１のモデルナンバー
			gc_Player[PLAYER_2].SetModelWidth( MASSIVE_WIDTH ) ;

			// --- アニメーション
			SetAnimation( PLAYER_2, A_STAYL_2P, PATH_ANIM_STAYL_MASSIVE ) ;				// --- マッシブ　左待機
			SetAnimation( PLAYER_2, A_STAYR_2P, PATH_ANIM_STAYR_MASSIVE ) ;				// ---			 右待機
			SetAnimation( PLAYER_2, A_RUN_2P, PATH_ANIM_RUN_MASSIVE ) ;					// --- マッシブ　移動
			SetAnimation( PLAYER_2, A_JUMPL_2P, PATH_ANIM_JUMPL_MASSIVE ) ;				// --- マッシブ　左ジャンプ
			SetAnimation( PLAYER_2, A_JUMPR_2P, PATH_ANIM_JUMPR_MASSIVE ) ;				// ---			 右ジャンプ
			SetAnimation( PLAYER_2, A_HATTACKL_2P, PATH_ANIM_HATTACKL_MASSIVE ) ;		// --- マッシブ　攻撃中左
			SetAnimation( PLAYER_2, A_HATTACKR_2P, PATH_ANIM_HATTACKR_MASSIVE ) ;		// ---			 攻撃中右
			SetAnimation( PLAYER_2, A_GUARDL_2P, PATH_ANIM_GUARDL_MASSIVE ) ;			// --- マッシブ　ガード左
			SetAnimation( PLAYER_2, A_GUARDR_2P, PATH_ANIM_GUARDR_MASSIVE ) ;			// ---			 ガード右
			SetAnimation( PLAYER_2, A_LATTACKL_2P, PATH_ANIM_LATTACKL_MASSIVE ) ;
			SetAnimation( PLAYER_2, A_LATTACKR_2P, PATH_ANIM_LATTACKR_MASSIVE ) ;
			SetAnimation( PLAYER_2, A_FIGHTENEDL_2P, PATH_ANIM_FIGHTENEDL_MASSIVE ) ;
			SetAnimation( PLAYER_2, A_FIGHTENEDR_2P, PATH_ANIM_FIGHTENEDR_MASSIVE ) ;
			SetAnimation( PLAYER_2, A_DOWNL_2P, PATH_ANIM_DOWNL_MASSIVE ) ;	// --- マッシブ　ダウン開始左
			SetAnimation( PLAYER_2, A_DOWNR_2P, PATH_ANIM_DOWNR_MASSIVE ) ;	// ---			 ダウン開始右
			SetAnimation( PLAYER_2, A_BOOSTL_2P, PATH_ANIM_BOOSTL_MASSIVE ) ;
			SetAnimation( PLAYER_2, A_BOOSTR_2P, PATH_ANIM_BOOSTR_MASSIVE ) ;

			gc_Player[PLAYER_2].SetGdflg( TRUE ) ;
			SetAnimation( PLAYER_2, A_GREATDEATHL_2P, PATH_ANIM_GREATDEATHL_MASSIVE ) ;	// --- マッシブ　大必殺左
			SetAnimation( PLAYER_2, A_GREATDEATHR_2P, PATH_ANIM_GREATDEATHR_MASSIVE ) ;	// ---			 大必殺右
			gc_Player[PLAYER_2].SetGdflg( FALSE ) ;

			gc_winlose[PLAYER_2].SetModelHandle( PATH_MASSIVE_MODEL ) ;	// --- プレイヤー１のモデルパス
			SetWinloseAnimation( PLAYER_2, A_WIN_PLAY2, PATH_ANIM_HATTACKL_MASSIVE ) ;
			SetWinloseAnimation( PLAYER_2, A_LOSE_PLAY2, PATH_ANIM_DOWNL_MASSIVE ) ;
			break ;

		case USAGI_NUMBER :
			gc_Player[PLAYER_2].SetModelHandle( PATH_USAGI_MODEL , PATH_USAGI_MODELWITHSWORD ) ;	// --- プレイヤー２のモデルパス
			gc_Player[PLAYER_2].SetModelNumber( USAGI_NUMBER ) ;		// --- プレイヤー２のモデルナンバー
			gc_Player[PLAYER_2].SetModelWidth( USAGI_WIDTH ) ;

			// --- アニメーション
			SetAnimation( PLAYER_2, A_RUN_2P, PATH_ANIM_RUN_USAGI ) ;					// --- ウサギ　移動
			SetAnimation( PLAYER_2, A_STAYL_2P, PATH_ANIM_STAYL_USAGI ) ;				// --- ウサギ　左待機
			SetAnimation( PLAYER_2, A_STAYR_2P, PATH_ANIM_STAYR_USAGI ) ;				// ---		   右待機
			SetAnimation( PLAYER_2, A_JUMPL_2P, PATH_ANIM_JUMPL_USAGI ) ;				// ---
			SetAnimation( PLAYER_2, A_JUMPR_2P, PATH_ANIM_JUMPR_USAGI ) ;				// ---
			SetAnimation( PLAYER_2, A_HATTACKL_2P, PATH_ANIM_HATTACKL_USAGI ) ;				// ---
			SetAnimation( PLAYER_2, A_HATTACKR_2P, PATH_ANIM_HATTACKR_USAGI ) ;				// ---
			SetAnimation( PLAYER_2, A_LATTACKL_2P, PATH_ANIM_LATTACKL_USAGI ) ;				// ---
			SetAnimation( PLAYER_2, A_LATTACKR_2P, PATH_ANIM_LATTACKR_USAGI ) ;				// ---
			SetAnimation( PLAYER_2, A_GUARDL_2P, PATH_ANIM_GUARDL_USAGI ) ;				// ---
			SetAnimation( PLAYER_2, A_GUARDR_2P, PATH_ANIM_GUARDR_USAGI ) ;				// ---
			SetAnimation( PLAYER_2, A_FIGHTENEDL_2P, PATH_ANIM_FIGHTENEDL_USAGI ) ;				// ---
			SetAnimation( PLAYER_2, A_FIGHTENEDR_2P, PATH_ANIM_FIGHTENEDR_USAGI ) ;				// ---
			SetAnimation( PLAYER_2, A_DOWNL_2P, PATH_ANIM_DOWNL_USAGI ) ;				// ---
			SetAnimation( PLAYER_2, A_DOWNR_2P, PATH_ANIM_DOWNR_USAGI ) ;				// ---
			SetAnimation( PLAYER_2, A_BOOSTL_2P, PATH_ANIM_BOOSTL_USAGI ) ;
			SetAnimation( PLAYER_2, A_BOOSTR_2P, PATH_ANIM_BOOSTR_USAGI ) ;

			gc_Player[PLAYER_2].SetGdflg( TRUE ) ;
			SetAnimation( PLAYER_2, A_GREATDEATHL_2P, PATH_ANIM_GREATDEATHL_USAGI ) ;	// --- マッシブ　大必殺左
			SetAnimation( PLAYER_2, A_GREATDEATHR_2P, PATH_ANIM_GREATDEATHR_USAGI ) ;	// ---			 大必殺右
			gc_Player[PLAYER_2].SetGdflg( FALSE ) ;

			gc_winlose[PLAYER_2].SetModelHandle( PATH_USAGI_MODEL ) ;	// --- プレイヤー１のモデルパス
			SetWinloseAnimation( PLAYER_2, A_WIN_PLAY2, PATH_ANIM_HATTACKL_USAGI ) ;
			SetWinloseAnimation( PLAYER_2, A_LOSE_PLAY2, PATH_ANIM_DOWNL_USAGI ) ;
			break ;
	}
	/* ------------------------------------------------------------ +/
		モデルセット
	/+ ------------------------------------------------------------ */
	gc_Player[PLAYER_1].SetPosition( VGet( -100 , 0 , 0 ), VGet(0, 0, 0) ) ;					// --- プレイヤー１の座標
	gc_Player[PLAYER_2].SetPosition( VGet( 100 , 0 , 0 ),VGet(0, 0, 0) ) ;					// --- プレイヤー２の座標

	gf_Stage.SetModelHandle( PATH_STAGE ) ;										// --- ステージのモデルパス
	gf_Stage.SetPosition( VGet( 0.0f, -20.0f, 0.0f ) ) ;						// --- ステージの座標
	gf_Undersig[PLAYER_1].SetModelHandle( PATH_UNDERSIGN_1P ) ;					// --- 足元サイン
	gf_Undersig[PLAYER_2].SetModelHandle( PATH_UNDERSIGN_2P ) ;					// --- 足元サイン

	gc_Item.SetModelHandle( PATH_ITEM ) ;										// --- アイテム
	gc_Item.SetPosition( VGet(0.0f, 40.0f, 0.0f) ) ;							// --- アイテムの座標
	//gc_Item.SetModelScale( VGet(0.5f, 0.5f, 0.5f) ) ;


	/* ------------------------------------------------------------ +/
		アニメーションセット
	/+ ------------------------------------------------------------ */

	SetObjAnimation( A_STAY_ITEM, PATH_ANIM_STAY_ITEM ) ;

	/* ------------------------------------------------------------ +/
		ルートフレーム取得
	/+ ------------------------------------------------------------ */
	for ( int i = 0 ; i < MAX_PLAYER ; i++ ){
		gv_rootflm[i] = MV1SearchFrame( gc_Player[i].GetModelHandle(), "root" ) ;
		gv_rootflmwl[i] = MV1SearchFrame( gc_winlose[i].GetModelHandle(), "root" ) ;
	}

	// --- オブジェクト
	gv_rootflmobj[OBJECT_ITEM] = MV1SearchFrame( gc_Item.GetModelHandle(), "root" ) ;

	/* ------------------------------------------------------------ +/
		初期アニメーションセット
	/+ ------------------------------------------------------------ */
	// --- プレイヤー１
	gv_attachidx[PLAYER_1] = MV1AttachAnim( gc_Player[PLAYER_1].GetModelHandle(), 0, gv_hAnim1P[A_STAYR_1P] ) ;
	gv_anim_totaltime[PLAYER_1] = gv_anim_timestock1P[A_STAYR_1P] ;
	MV1SetFrameUserLocalMatrix( gc_Player[PLAYER_1].GetModelHandle(), gv_rootflm[PLAYER_1], MGetIdent() ) ;

	// --- プレイヤー２
	gv_attachidx[PLAYER_2] = MV1AttachAnim( gc_Player[PLAYER_2].GetModelHandle(), 0, gv_hAnim2P[A_STAYL_2P] ) ;
	gv_anim_totaltime[PLAYER_2] = gv_anim_timestock2P[A_STAYL_2P] ;
	MV1SetFrameUserLocalMatrix( gc_Player[PLAYER_2].GetModelHandle(), gv_rootflm[PLAYER_2], MGetIdent() ) ;

	// --- オブジェクト
	gv_attachidxobj[OBJECT_ITEM] = MV1AttachAnim( gc_Item.GetModelHandle(), 0, gv_hAnimobj[A_STAY_ITEM] ) ;
	gv_anim_totaltimeobj[OBJECT_ITEM] = gv_anim_timestockobj[A_STAY_ITEM] ;
	MV1SetFrameUserLocalMatrix( gc_Item.GetModelHandle(), gv_rootflmobj[OBJECT_ITEM], MGetIdent() ) ;

	/* ------------------------------------------------------------ +/
		エフェクトモデル
	/+ ------------------------------------------------------------ */

	// --- プレイヤー１で使うエフェクトモデルを取得
	gc_Lazer[PLAYER1_EFFECT].SetModel( PATH_FIRE ) ;					// --- レーザー
	gc_Fire_Weak[PLAYER1_EFFECT].SetModel( PATH_FIRE_WEAK ) ;			// --- 弱被弾
	gc_Fire_Strong[PLAYER1_EFFECT].SetModel( PATH_FIRE_STRONG ) ;		// --- 強被弾
	gc_Accelerated[PLAYER1_EFFECT].SetModel( PATH_FIRE_ACCELERATED ) ;	// --- ブースト

	// --- プレイヤー２で使うエフェクトモデルを取得
	gc_Lazer[PLAYER2_EFFECT].SetModel( PATH_FIRE ) ;					// --- レーザー
	gc_Fire_Weak[PLAYER2_EFFECT].SetModel( PATH_FIRE_WEAK ) ;			// --- 弱被弾
	gc_Fire_Strong[PLAYER2_EFFECT].SetModel( PATH_FIRE_STRONG ) ;		// --- 強被弾
	gc_Accelerated[PLAYER2_EFFECT].SetModel( PATH_FIRE_ACCELERATED ) ;	// --- ブースト

	gc_Gard[PLAYER1_EFFECT].SetModel( PATH_GARD_MASSIVE ) ;	// --- プレイヤー１マッシブガード
	gc_Gard[PLAYER2_EFFECT].SetModel( PATH_GARD_USAGI )	;	// --- プレイヤー１うさぎガード

	// --- エフェクト初期化
	gc_Lazer[PLAYER1_EFFECT].EffectInit() ;
	gc_Fire_Weak[PLAYER1_EFFECT].EffectInit() ;
	gc_Fire_Strong[PLAYER1_EFFECT].EffectInit() ;
	gc_Accelerated[PLAYER1_EFFECT].EffectInit() ;

	gc_Lazer[PLAYER2_EFFECT].EffectInit() ;
	gc_Fire_Weak[PLAYER2_EFFECT].EffectInit() ;
	gc_Fire_Strong[PLAYER2_EFFECT].EffectInit() ;
	gc_Accelerated[PLAYER2_EFFECT].EffectInit() ;

	// --- 全てのエフェクトを非表示にしておく
	MV1SetVisible( gc_Lazer[PLAYER1_EFFECT].GetModelHandle(), FALSE ) ;
	MV1SetVisible( gc_Lazer[PLAYER2_EFFECT].GetModelHandle(), FALSE ) ;
	MV1SetVisible( gc_Gard[PLAYER1_EFFECT].GetModelHandle(), FALSE ) ;
	MV1SetVisible( gc_Gard[PLAYER2_EFFECT].GetModelHandle(), FALSE ) ;
	MV1SetVisible( gc_Fire_Weak[PLAYER1_EFFECT].GetModelHandle(), FALSE ) ;
	MV1SetVisible( gc_Fire_Weak[PLAYER2_EFFECT].GetModelHandle(), FALSE ) ;
	MV1SetVisible( gc_Fire_Strong[PLAYER1_EFFECT].GetModelHandle(), TRUE ) ;
	MV1SetVisible( gc_Fire_Strong[PLAYER2_EFFECT].GetModelHandle(), FALSE ) ;
	MV1SetVisible( gc_Accelerated[PLAYER1_EFFECT].GetModelHandle(), FALSE ) ;
	MV1SetVisible( gc_Accelerated[PLAYER2_EFFECT].GetModelHandle(), FALSE ) ;

	gv_Gamemode = GM_Playgame ;

	return( TRUE ) ;
}

unsigned char blnkflg = 0x00 ;
BOOL itemflg = FALSE ;
int Scene_Playgame()
{
	// --- 点滅時間進行
	blnkflg++ ;
	if ( blnkflg > 64 ){
		blinkflg = 0 ;
	}

	// --- ゲーム画面のサウンドを再生
	ChangeVolumeSoundMem( 255 / 2 , gc_Main_Sound[2].GetModelHandle() ) ;
	while( CheckSoundMem( gc_Main_Sound[2].GetModelHandle() ) == 0 )
	{
		PlaySoundMem( gc_Main_Sound[2].GetModelHandle(), DX_PLAYTYPE_LOOP , TRUE) ;
	}

	/* +++ ゲーム中 +++ */
	GetDateTime( &gv_nowtimer ) ;		// --- タイマー

	/* タイマー初期セット */
	if ( gv_gamestarttim == FALSE )
	{
		gv_limittime = 60 ;
		gv_tmptimer = gv_nowtimer.Sec ;
		gv_gamestarttim = TRUE ;
	}

	/* プレイヤー１ */
	gc_Player[PLAYER_1].CheckKeyAction( ) ;
	gc_Player[PLAYER_1].UpdatePosDir( ) ;
	gc_Player[PLAYER_1].MainAction( ) ;

	/* プレイヤー２ */
	gc_Player[PLAYER_2].CheckKeyAction( ) ;
	gc_Player[PLAYER_2].UpdatePosDir( ) ;
	gc_Player[PLAYER_2].MainAction( ) ;

	gf_Undersig[PLAYER_1].SetRotPosition( gc_Player[PLAYER_1].GetPosition() ) ;
	gf_Undersig[PLAYER_2].SetRotPosition( gc_Player[PLAYER_2].GetPosition() ) ;

	DrawBox( 0, 0, WINDOW_SIZE_W, WINDOW_SIZE_H, GetColor(255, 255, 255), true ) ;
	DrawGraph( 0, 0, gv_gnbackimg, FALSE ) ;

//	if ( (blnkflg & 0x04) && gc_Player[PLAYER_1].GetInvinFlg() == FALSE )
		MV1DrawModel( gc_Player[PLAYER_1].GetModelHandle() ) ;		// --- プレイヤー１の表示

//	if ( (blnkflg & 0x04) && gc_Player[PLAYER_2].GetInvinFlg() == FALSE )
		MV1DrawModel( gc_Player[PLAYER_2].GetModelHandle() ) ;		// --- プレイヤー２の表示


	MV1DrawModel( gf_Stage.GetModelHandle() ) ;			// --- ステージの表示
	MV1DrawModel( gf_Undersig[PLAYER_1].GetModelHandle() ) ;		// --- 足元サインの表示
	MV1DrawModel( gf_Undersig[PLAYER_2].GetModelHandle() ) ;		// --- 足元サインの表示

	// --- プレイヤー１で表示するエフェクト
	MV1DrawModel( gc_Lazer[PLAYER1_EFFECT].GetModelHandle() ) ;				// --- レーザーの表示(プレイヤー１)
	MV1DrawModel( gc_Gard[PLAYER1_EFFECT].GetModelHandle() ) ;				// --- ガードの表示(プレイヤー１)
	MV1DrawModel( gc_Fire_Weak[PLAYER_1].GetModelHandle() ) ;				// --- 弱被弾の表示(プレイヤー１)
	MV1DrawModel( gc_Fire_Strong[PLAYER1_EFFECT].GetModelHandle() ) ;		// --- 強被弾の表示(プレイヤー１)
	MV1DrawModel( gc_Accelerated[PLAYER1_EFFECT].GetModelHandle() ) ;		// --- ブーストの表示(プレイヤー１)

	// --- プレイヤー２で表示するエフェクト
	MV1DrawModel( gc_Lazer[PLAYER2_EFFECT].GetModelHandle() ) ;				// --- レーザーの表示(プレイヤー２)
	MV1DrawModel( gc_Gard[PLAYER2_EFFECT].GetModelHandle() ) ;				// --- ガードの表示(プレイヤー２)
	MV1DrawModel( gc_Fire_Weak[PLAYER_2].GetModelHandle() ) ;				// --- 弱被弾の表示(プレイヤー２)
	MV1DrawModel( gc_Fire_Strong[PLAYER2_EFFECT].GetModelHandle() ) ;		// --- 強被弾の表示(プレイヤー２)
	MV1DrawModel( gc_Accelerated[PLAYER2_EFFECT].GetModelHandle() ) ;		// --- ブーストの表示(プレイヤー２)

	// --- レーザー
	gc_Lazer[PLAYER1_EFFECT].UpdatePosDir( PLAYER_1 ) ;
	gc_Lazer[PLAYER2_EFFECT].UpdatePosDir( PLAYER_2 ) ;

	// --- ガードエフェクト
	gc_Gard[PLAYER1_EFFECT].UpdatePosDir( PLAYER_1 ) ;
	gc_Gard[PLAYER2_EFFECT].UpdatePosDir( PLAYER_2 ) ;

	// --- 弱ポテト
	gc_Fire_Weak[PLAYER1_EFFECT].UpdatePosDir( PLAYER_1 ) ;
	gc_Fire_Weak[PLAYER2_EFFECT].UpdatePosDir( PLAYER_2 ) ;

	// --- 強ポテト
	gc_Fire_Strong[PLAYER1_EFFECT].UpdatePosDir( PLAYER_1 ) ;
	gc_Fire_Strong[PLAYER2_EFFECT].UpdatePosDir( PLAYER_2 ) ;

	// --- ブースト
	gc_Accelerated[PLAYER1_EFFECT].UpdatePosDir( PLAYER_1 ) ;
	gc_Accelerated[PLAYER2_EFFECT].UpdatePosDir( PLAYER_2 ) ;

	gc_Lazer[PLAYER1_EFFECT].HitFire() ;
	gc_Fire_Weak[PLAYER1_EFFECT].HitFire() ;		
	gc_Fire_Strong[PLAYER1_EFFECT].HitFire() ;
	gc_Accelerated[PLAYER1_EFFECT].HitFire() ;
	gc_Sound[PLAYER_1][0].UpdatePosDir() ;
	gc_Sound[PLAYER_2][0].UpdatePosDir() ;

	// --- 盾が壊れた時
	// if (シールドのHPが０の時)
	{

	}
	/*
	if (keyState[KEY_INPUT_L] == 1 )
	{
		// --- 透明度の指定
	//			MV1SetOpacityRate( gc_Fire.GetModelHandle() , 0.5f ) ;
		MV1SetOpacityRate( gc_Gard[PLAYER1_EFFECT].GetModelHandle() , 0.2f ) ;
		PlaySoundMem( gc_Sound[19].GetModelHandle(), DX_PLAYTYPE_BACK , FALSE) ;

	}else{
		MV1SetOpacityRate( gc_Gard[PLAYER1_EFFECT].GetModelHandle() , 1.0f ) ;
	}
	*/

	// -- killcounter
	DrawGraph( (gv_win_width/2)-210, 0, gv_gnkillimg, TRUE ) ;
	DrawGraph( (gv_win_width/2)-175, 23, gv_numimg[gc_Player[PLAYER_1].GetPlayerKillcnt() / 10], TRUE ) ;		// --- プレイヤー１
	DrawGraph( (gv_win_width/2)-100, 23, gv_numimg[gc_Player[PLAYER_1].GetPlayerKillcnt() % 10], TRUE ) ;		// --- プレイヤー１
	DrawGraph( (gv_win_width/2), 23, gv_numimg[gc_Player[PLAYER_2].GetPlayerKillcnt() / 10], TRUE ) ;			// --- プレイヤー２
	DrawGraph( (gv_win_width/2)+75, 23, gv_numimg[gc_Player[PLAYER_2].GetPlayerKillcnt() % 10], TRUE ) ;		// --- プレイヤー２

	if ( gv_limittime <= 20 )
	{
		DrawGraph( (gv_win_width/2)-200, -240, gv_gnkillcover, TRUE ) ;

		// --- アイテム表示
		MV1DetachAnim( gc_Item.GetModelHandle(), gv_attachidxobj[OBJECT_ITEM] ) ;

		if ( gc_Item.CheckHit() == 0x00 )
		{
			PlayObjAnim( OBJECT_ITEM ) ;
			gc_Item.SetModelScale( VGet( 0.5f, 0.5f, 0.5f ) ) ;
			MV1DrawModel( gc_Item.GetModelHandle() ) ;			// --- アイテムの表示
		}
		else if( itemflg == FALSE )
		{
			switch (gc_Item.CheckHit())
			{
				case 0x01 :
					gc_Player[PLAYER_1].SetGdflg( TRUE ) ;
					itemflg = TRUE ;
					break ;

				case 0x02 :
					gc_Player[PLAYER_2].SetGdflg( TRUE ) ;
					itemflg = TRUE ;
					break ;
			}
		}
	}


	// --- hpbar
	DrawGraph( 20, 40, gv_hpframeimg[0], TRUE ) ;
	DrawGraph( gv_win_width-750, 40, gv_hpframeimg[1], TRUE ) ;

	for ( int i = 0 ; i < gc_Player[PLAYER_1].GetPlayerHP() ; i++ ){
		DrawGraph( 24+i, 44, gv_hpgage[i], TRUE ) ;
	}

	for ( int i = 0 ; i < gc_Player[PLAYER_2].GetPlayerHP() ; i++ ){
		DrawGraph( WINDOW_SIZE_W-743+i, 45, gv_hpgage[720+i], TRUE ) ;
	}

	for ( int i = 0 ; i < gc_Player[PLAYER_1].GetPlayerStamina() ; i++ ){
		DrawGraph( 47+i, 125, gv_stmngage[i], TRUE ) ;
	}

	for ( int i = 0 ; i < gc_Player[PLAYER_2].GetPlayerStamina() ; i++ ){
		DrawGraph( gv_win_width-546+i, 125, gv_stmngage[502+i], TRUE ) ;
	}

	//if ( gv_hpmax[PLAYER_1] >= ) {	}

	// --- timer
	if ( gv_tmptimer != gv_nowtimer.Sec )
	{
		gv_tmptimer = gv_nowtimer.Sec ;
		gv_limittime-- ;
	}

	DrawGraph( (gv_win_width/2)-90, 150, gv_numimg[ gv_limittime / 10], TRUE ) ;		// --- 十の位
	DrawGraph( (gv_win_width/2)-15, 150, gv_numimg[ gv_limittime % 10], TRUE ) ;		// --- 一の位

	if ( gv_limittime <= 0 )
	{
		// --- ゲーム画面のサウンドを止める
		StopSoundMem(gc_Main_Sound[2].GetModelHandle() ) ;
		gc_winlose[PLAYER_1].SetPosition( VGet( -100 , 0 , -100 )) ;					// --- プレイヤー１の座標
		gc_winlose[PLAYER_2].SetPosition( VGet( 100 , 0 , -100 )) ;					// --- プレイヤー２の座標
		//gc_Player[PLAYER_1].SetPosition( VGet( -100 , 0 , -100 ), VGet(0, 0, 0) ) ;					// --- プレイヤー１の座標
		//gc_Player[PLAYER_2].SetPosition( VGet( 100 , 0 , -100 ),VGet(0, 0, 0) ) ;					// --- プレイヤー２の座標
		//gc_Player[PLAYER_1].UpdatePosDir( ) ;
		//gc_Player[PLAYER_2].UpdatePosDir( ) ;

		gv_Gamemode = GM_Result ;
	}

	return TRUE ;
}

int resultcnt = 0 ;
int killcover = 0 ;
int Scene_Result()
{
	// --- リザルト画面のサウンドを再生
	ChangeVolumeSoundMem( 255 / 2 , gc_Main_Sound[3].GetModelHandle() ) ;
	while( CheckSoundMem( gc_Main_Sound[3].GetModelHandle() ) == 0 )
	{
		PlaySoundMem( gc_Main_Sound[3].GetModelHandle(), DX_PLAYTYPE_LOOP ) ;
	}

	/* +++ リザルト画面 +++ */
	int killcnt_p1, killcnt_p2 ;

	DrawBox( 0, 0, WINDOW_SIZE_W, WINDOW_SIZE_H, GetColor(255, 255, 255), true ) ;
	DrawGraph( 0, 0, gv_gnbackimg, FALSE ) ;

	killcnt_p1 = gc_Player[PLAYER_1].GetPlayerKillcnt( ) ;
	killcnt_p2 = gc_Player[PLAYER_2].GetPlayerKillcnt( ) ;

	MV1DetachAnim( gc_winlose[PLAYER_1].GetModelHandle(), gv_attachidxobj[PLAYER_1] ) ;
	MV1DetachAnim( gc_winlose[PLAYER_2].GetModelHandle(), gv_attachidxobj[PLAYER_2] ) ;
	resultcnt++ ;

	gc_Player[PLAYER_1].MainAction( ) ;
	gc_Player[PLAYER_2].MainAction( ) ;

	gf_Undersig[PLAYER_1].SetRotPosition( gc_winlose[PLAYER_1].GetPosition() ) ;
	gf_Undersig[PLAYER_2].SetRotPosition( gc_winlose[PLAYER_2].GetPosition() ) ;
	MV1DrawModel( gf_Undersig[PLAYER_1].GetModelHandle() ) ;		// --- 足元サインの表示
	MV1DrawModel( gf_Undersig[PLAYER_2].GetModelHandle() ) ;		// --- 足元サインの表示

	MV1DrawModel( gf_Stage.GetModelHandle() ) ;			// --- ステージの表示
	MV1DrawModel( gc_winlose[PLAYER_1].GetModelHandle() ) ;		// --- プレイヤー１の表示
	MV1DrawModel( gc_winlose[PLAYER_2].GetModelHandle() ) ;		// --- プレイヤー２の表示
	// -- killcounter
	if (resultcnt >= 400)
	{
		DrawGraph( (gv_win_width/2)-210, 0, gv_gnkillimg, TRUE ) ;
		DrawGraph( (gv_win_width/2)-175, 23, gv_numimg[gc_Player[PLAYER_1].GetPlayerKillcnt() / 10], TRUE ) ;		// --- プレイヤー１
		DrawGraph( (gv_win_width/2)-100, 23, gv_numimg[gc_Player[PLAYER_1].GetPlayerKillcnt() % 10], TRUE ) ;		// --- プレイヤー１
		DrawGraph( (gv_win_width/2), 23, gv_numimg[gc_Player[PLAYER_2].GetPlayerKillcnt() / 10], TRUE ) ;			// --- プレイヤー２
		DrawGraph( (gv_win_width/2)+75, 23, gv_numimg[gc_Player[PLAYER_2].GetPlayerKillcnt() % 10], TRUE ) ;		// --- プレイヤー２
		if ( killcover <= 220 )
		{
			killcover += 4 ;
			DrawGraph( (gv_win_width/2)-200, -200 - killcover, gv_gnkillcover, TRUE ) ;
		}
		else{
			// --- 1P勝利
			if ( killcnt_p1 > killcnt_p2 )
			{
				PlayWinloseAnim( A_WIN_PLAY1 ) ;
				PlayWinloseAnim( A_LOSE_PLAY2 ) ;

				DrawGraph( 130, 700 , gv_win, TRUE ) ;
				DrawGraph( 1230, 700 , gv_lose, TRUE ) ;
			}

			// --- 2P勝利
			if ( killcnt_p1 < killcnt_p2 )
			{
				PlayWinloseAnim( A_WIN_PLAY2 ) ;
				PlayWinloseAnim( A_LOSE_PLAY1 ) ;

				DrawGraph( 930, 700 , gv_win, TRUE ) ;
				DrawGraph( 530, 700 , gv_lose, TRUE ) ;
			}

			if( (killcnt_p1 == killcnt_p2) )
			{
				if( gv_lastkill == PLAYER_1 )
				{
					PlayWinloseAnim( A_WIN_PLAY1 ) ;
					PlayWinloseAnim( A_LOSE_PLAY2 ) ;

					DrawGraph( 130, 700 , gv_win, TRUE ) ;
					DrawGraph( 1230, 700 , gv_lose, TRUE ) ;
				}
				if( gv_lastkill == PLAYER_2 )
				{
					PlayWinloseAnim( A_WIN_PLAY2 ) ;
					PlayWinloseAnim( A_LOSE_PLAY1 ) ;

					DrawGraph( 930, 700 , gv_win, TRUE ) ;
					DrawGraph( 530, 700 , gv_lose, TRUE ) ;
				}
			}
		}
	}
	else{
		DrawGraph( (gv_win_width/2)-210, 0, gv_gnkillimg, TRUE ) ;
		DrawGraph( (gv_win_width/2)-200, -200, gv_gnkillcover, TRUE ) ;
	}

	if ( (gv_PadStatus[PLAYER_1] & PAD_START ) || (gv_PadStatus[PLAYER_2] & PAD_START) ){
		gv_Gamemode = GM_End ;
	}

	return TRUE ;
}

int Scene_End()
{
	/* +++ ゲーム終了 +++ */

	return TRUE ;
}


/*- [EOF] -*/
