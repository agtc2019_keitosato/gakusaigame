/* ============================================================ +/ 

	LastEdit : 2020/09/25		NAME:佐藤圭桃
	LastEdit : ----/--/--		NAME:
	LastEdit : 2020/10/16		NAME:宮崎櫻
	___________________________________________________________

		Common.h
	___________________________________________________________

		全てのファイルで共通のヘッダーファイル

/+ ============================================================ */

/* ============================================================ +/
/+																+/
/+	共通ヘッダー												+/
/+																+/
/+ ============================================================ */
#include <Windows.h>
#include <stdio.h>
#include <DxLib.h>

#include "ConsoleWindow.h"
#include "Player.h"
#include "Object.h"

#include "Effect_Lazer.h"
#include "Effect_Gard.h"
#include "Effect_Weak_Fire.h"
#include "Effect_Strong_Fire.h"
#include "Effect_Accelerated.h"
#include "Sound.h"

/* ============================================================ +/
/+																+/
/+	Define														+/
/+																+/
/+ ============================================================ */
#define WINDOW_SIZE_W	1920		// --- ウィンドウサイズ　幅
#define WINDOW_SIZE_H	1080		// --- ウィンドウサイズ　高さ

#define WINDOW_MODE_WIN		TRUE	// --- ウィンドウモード　ウィンドウ
#define WINDOW_MODE_FULL	FALSE	// --- ウィンドウモード　フルスクリーン

#define MAX_PLAYER		2		// --- 最大プレイヤー数
#define MAX_OBJECT		2		// --- 最大オブジェクト数

#define MAX_ANIM		100		// --- 最大アニメーション数

#define PLAYER_1		0		// --- プレイヤー１
#define PLAYER_2		1		// --- プレイヤー２

#define OBJECT_ITEM		0

#define PLAYER_HP		720		// --- プレイヤーの最大ＨＰ
#define PLAYER_STAMINA	502.0f	// --- プレイヤーの最大スタミナ

#define MAX_EFFECT_ANIM 10		// --- エフェクト用
#define PLAYER1_EFFECT	0		// --- プレイヤー１
#define PLAYER2_EFFECT	1		// --- プレイヤー２
#define MAX_EFFECT 4

// --- サウンド
#define MAX_SOUND		50		// --- 最大サウンド数
#define MAX_BS			10		// --- BGM&SEの最大数

// --- モデル・アニメーション　パス

#define MASSIVE_NUMBER		2
	#define MASSIVE_WIDTH		140.0f																		// --- マッシブ横幅
	#define MASSIVE_HEIGHT		170.0f																		// --- マッシブ縦幅
	#define PATH_MASSIVE_MODEL				"..\\Release\\Models\\M_Chara_MassiveMecha.mv1"									// --- パス　マッシブ　メッシュ
	#define PATH_MASSIVE_MODELWITHSWORD		"..\\Release\\Models\\M_Chara_MassiveMechaWithSword.mv1"									// --- パス　マッシブモデル
	#define PATH_ANIM_STAYL_MASSIVE			"..\\Release\\Models\\MassiveAnim\\M_Chara_MassiveMecha@Wait_L.mv1"				// --- パス　マッシブ　待ち　　　　左
	#define PATH_ANIM_STAYR_MASSIVE			"..\\Release\\Models\\MassiveAnim\\M_Chara_MassiveMecha@Wait_R.mv1"				// --- パス　マッシブ　待ち　　　　右
	#define PATH_ANIM_RUN_MASSIVE			"..\\Release\\Models\\MassiveAnim\\M_Chara_MassiveMecha@Walk.mv1"				// --- パス　マッシブ　走り
	#define PATH_ANIM_JUMPL_MASSIVE			"..\\Release\\Models\\MassiveAnim\\M_Chara_MassiveMecha@Jump_Mid_L.mv1"			// --- パス　マッシブ　ジャンプ中　左
	#define PATH_ANIM_JUMPR_MASSIVE			"..\\Release\\Models\\MassiveAnim\\M_Chara_MassiveMecha@Jump_Mid_R.mv1"			// --- パス　マッシブ　ジャンプ中　右
	#define PATH_ANIM_HATTACKL_MASSIVE		"..\\Release\\Models\\MassiveAnim\\M_Chara_MassiveMecha@HAttack_End_L.mv1"		// --- パス　マッシブ　腕攻撃中　　左
	#define PATH_ANIM_HATTACKR_MASSIVE		"..\\Release\\Models\\MassiveAnim\\M_Chara_MassiveMecha@HAttack_End_R.mv1"		// --- パス　マッシブ　腕攻撃中　　右
	#define PATH_ANIM_LATTACKL_MASSIVE		"..\\Release\\Models\\MassiveAnim\\M_Chara_MassiveMecha@LAttack_End_L.mv1"		// --- パス　マッシブ　脚攻撃中　　左
	#define PATH_ANIM_LATTACKR_MASSIVE		"..\\Release\\Models\\MassiveAnim\\M_Chara_MassiveMecha@LAttack_End_R.mv1"		// --- パス　マッシブ　脚攻撃中　　右
	#define PATH_ANIM_GUARDL_MASSIVE		"..\\Release\\Models\\MassiveAnim\\M_Chara_MassiveMecha@Guard_mid_L.mv1"		// --- パス　マッシブ　ガード中　　左
	#define PATH_ANIM_GUARDR_MASSIVE		"..\\Release\\Models\\MassiveAnim\\M_Chara_MassiveMecha@Guard_mid_R.mv1"		// --- パス　マッシブ　ガード中　　右
	#define PATH_ANIM_FIGHTENEDL_MASSIVE	"..\\Release\\Models\\MassiveAnim\\M_Chara_MassiveMecha@Frightened_L.mv1"		// --- パス　マッシブ　怯み　　　　左
	#define PATH_ANIM_FIGHTENEDR_MASSIVE	"..\\Release\\Models\\MassiveAnim\\M_Chara_MassiveMecha@Frightened_R.mv1"		// --- パス　マッシブ　怯み　　　　右
	#define PATH_ANIM_DOWNL_MASSIVE			"..\\Release\\Models\\MassiveAnim\\M_Chara_MassiveMecha@Down_L.mv1"
	#define PATH_ANIM_DOWNR_MASSIVE			"..\\Release\\Models\\MassiveAnim\\M_Chara_MassiveMecha@Down_R.mv1"
	#define PATH_ANIM_BOOSTL_MASSIVE		"..\\Release\\Models\\MassiveAnim\\M_Chara_MassiveMecha@Boost_L.mv1"
	#define PATH_ANIM_BOOSTR_MASSIVE		"..\\Release\\Models\\MassiveAnim\\M_Chara_MassiveMecha@Boost_R.mv1"
	#define PATH_ANIM_GREATDEATHL_MASSIVE	"..\\Release\\Models\\MassiveAnim\\M_Chara_MassiveMecha@GreatDeath_L.mv1"
	#define PATH_ANIM_GREATDEATHR_MASSIVE	"..\\Release\\Models\\MassiveAnim\\M_Chara_MassiveMecha@GreatDeath_R.mv1"
	

#define USAGI_NUMBER		3																	// --- モデルナンバー
	#define USAGI_WIDTH			100.0f															// --- ウサギ横幅
	#define USAGI_HEIGHT		170.0f															// --- ウサギ縦幅
	#define PATH_USAGI_MODEL		"..\\Release\\Models\\M_Chara_UsagiΔ.mv1"					// --- パス　ウサギモデル
	#define PATH_USAGI_MODELWITHSWORD	"..\\Release\\Models\\M_Chara_UsagiΔWithSword.mv1"									// --- パス　マッシブモデル
	#define PATH_ANIM_STAYL_USAGI		"..\\Release\\Models\\UsagiAnim\\M_Chara_UsagiΔ@Wait_L.mv1"			// --- パス　ウサギアニメ　待ち
	#define PATH_ANIM_STAYR_USAGI		"..\\Release\\Models\\UsagiAnim\\M_Chara_UsagiΔ@Wait_R.mv1"			// --- パス　ウサギアニメ　待ち
	#define PATH_ANIM_RUN_USAGI			"..\\Release\\Models\\UsagiAnim\\M_Chara_UsagiΔ@Walk.mv1"				// --- パス　ウサギアニメ　走り
	#define PATH_ANIM_JUMPL_USAGI		"..\\Release\\Models\\UsagiAnim\\M_Chara_UsagiΔ@Jump_L.mv1"			// ---
	#define PATH_ANIM_JUMPR_USAGI		"..\\Release\\Models\\UsagiAnim\\M_Chara_UsagiΔ@Jump_R.mv1"			// ---
	#define PATH_ANIM_HATTACKL_USAGI	"..\\Release\\Models\\UsagiAnim\\M_Chara_UsagiΔ@HATTACK_L.mv1"		// ---
	#define PATH_ANIM_HATTACKR_USAGI	"..\\Release\\Models\\UsagiAnim\\M_Chara_UsagiΔ@HATTACK_R.mv1"		// ---
	#define PATH_ANIM_LATTACKL_USAGI	"..\\Release\\Models\\UsagiAnim\\M_Chara_UsagiΔ@LATTACK_L.mv1"		// ---
	#define PATH_ANIM_LATTACKR_USAGI	"..\\Release\\Models\\UsagiAnim\\M_Chara_UsagiΔ@LATTACK_R.mv1"		// ---
	#define PATH_ANIM_GUARDL_USAGI		"..\\Release\\Models\\UsagiAnim\\M_Chara_UsagiΔ@Guard_L.mv1"		// ---
	#define PATH_ANIM_GUARDR_USAGI		"..\\Release\\Models\\UsagiAnim\\M_Chara_UsagiΔ@Guard_R.mv1"		// ---
	#define PATH_ANIM_FIGHTENEDL_USAGI	"..\\Release\\Models\\UsagiAnim\\M_Chara_UsagiΔ@Fightened_L.mv1"	// ---
	#define PATH_ANIM_FIGHTENEDR_USAGI	"..\\Release\\Models\\UsagiAnim\\M_Chara_UsagiΔ@Fightened_R.mv1"	// ---
	#define PATH_ANIM_DOWNL_USAGI		"..\\Release\\Models\\UsagiAnim\\M_Chara_UsagiΔ@Down_L.mv1"	// ---
	#define PATH_ANIM_DOWNR_USAGI		"..\\Release\\Models\\UsagiAnim\\M_Chara_UsagiΔ@Down_R.mv1"	// ---
	#define PATH_ANIM_BOOSTL_USAGI		"..\\Release\\Models\\UsagiAnim\\M_Chara_UsagiΔ@Boost_L.mv1"	// ---
	#define PATH_ANIM_BOOSTR_USAGI		"..\\Release\\Models\\UsagiAnim\\M_Chara_UsagiΔ@Boost_R.mv1"	// ---
	#define PATH_ANIM_GREATDEATHL_USAGI		"..\\Release\\Models\\UsagiAnim\\M_Chara_UsagiΔ@GreatDeath_L.mv1"	// ---
	#define PATH_ANIM_GREATDEATHR_USAGI		"..\\Release\\Models\\UsagiAnim\\M_Chara_UsagiΔ@GreatDeath_R.mv1"	// ---



#define STANDERD_NUMBER		1																	// --- モデルナンバー
	#define STANDERD_WIDTH		140.0f															// --- スタンダード横幅
	#define STANDERD_HEIGHT		170.0f															// --- スタンダード縦幅
	#define PATH_STANDERD_MODEL			"..\\Release\\Models\\M_Chara_Standerd.mv1"				// --- パス　スタンダードモデル
	#define PATH_STANDERD_MODELWITHSWORD		"..\\Release\\Models\\M_Chara_StanderdWithSword.mv1"									// --- パス　マッシブモデル
	#define PATH_ANIM_STAYL_STANDERD		"..\\Release\\Models\\StanderdAnim\\M_Chara_Standerd@Wait_L.mv1"		// --- パス　スタンダードアニメ　待ち
	#define PATH_ANIM_STAYR_STANDERD		"..\\Release\\Models\\StanderdAnim\\M_Chara_Standerd@Wait_R.mv1"		// --- パス　スタンダードアニメ　待ち
	#define PATH_ANIM_RUN_STANDERD			"..\\Release\\Models\\StanderdAnim\\M_Chara_Standerd@Walk.mv1"			// --- パス　スタンダードアニメ　走り
	#define PATH_ANIM_JUMPL_STANDERD		"..\\Release\\Models\\StanderdAnim\\M_Chara_Standerd@Jump_L.mv1"		// ---
	#define PATH_ANIM_JUMPR_STANDERD		"..\\Release\\Models\\StanderdAnim\\M_Chara_Standerd@Jump_R.mv1"		// ---
	#define PATH_ANIM_HATTACKL_STANDERD		"..\\Release\\Models\\StanderdAnim\\M_Chara_Standerd@HATTACK_L.mv1"		// ---
	#define PATH_ANIM_HATTACKR_STANDERD		"..\\Release\\Models\\StanderdAnim\\M_Chara_Standerd@HATTACK_R.mv1"		// ---
	#define PATH_ANIM_LATTACKL_STANDERD		"..\\Release\\Models\\StanderdAnim\\M_Chara_Standerd@LATTACK_L.mv1"		// ---
	#define PATH_ANIM_LATTACKR_STANDERD		"..\\Release\\Models\\StanderdAnim\\M_Chara_Standerd@LATTACK_R.mv1"		// ---
	#define PATH_ANIM_GUARDL_STANDERD		"..\\Release\\Models\\StanderdAnim\\M_Chara_Standerd@Guard_L.mv1"		// ---
	#define PATH_ANIM_GUARDR_STANDERD		"..\\Release\\Models\\StanderdAnim\\M_Chara_Standerd@Guard_R.mv1"		// ---
	#define PATH_ANIM_FIGHTENEDL_STANDERD	"..\\Release\\Models\\StanderdAnim\\M_Chara_Standerd@Fightened_L.mv1"		// ---
	#define PATH_ANIM_FIGHTENEDR_STANDERD	"..\\Release\\Models\\StanderdAnim\\M_Chara_Standerd@Fightened_R.mv1"		// ---
	#define PATH_ANIM_DOWNL_STANDERD		"..\\Release\\Models\\StanderdAnim\\M_Chara_Standerd@Down_L.mv1"		// ---
	#define PATH_ANIM_DOWNR_STANDERD		"..\\Release\\Models\\StanderdAnim\\M_Chara_Standerd@Down_R.mv1"		// ---
	#define PATH_ANIM_BOOSTL_STANDERD		"..\\Release\\Models\\StanderdAnim\\M_Chara_Standerd@Boost_L.mv1"		// ---
	#define PATH_ANIM_BOOSTR_STANDERD		"..\\Release\\Models\\StanderdAnim\\M_Chara_Standerd@Boost_R.mv1"		// ---
	#define PATH_ANIM_GREATDEATHL_STANDERD		"..\\Release\\Models\\StanderdAnim\\M_Chara_Standerd@GreatDeath_L.mv1"		// ---
	#define PATH_ANIM_GREATDEATHR_STANDERD	"..\\Release\\Models\\StanderdAnim\\M_Chara_Standerd@GreatDeath_R.mv1"		// ---
	


#define PATH_STAGE			"..\\Release\\Models\\Stage.mv1"				// --- パス　ステージモデル
#define PATH_UNDERSIGN_1P	"..\\Release\\Models\\UnderSign_1P.mv1"				// --- パス　ステージモデル
#define PATH_UNDERSIGN_2P	"..\\Release\\Models\\UnderSign_2P.mv1"				// --- パス　ステージモデル

#define PATH_ITEM					"..\\Release\\Models\\Item.mv1"								// --- パス　アイテムモデル
	#define PATH_ANIM_STAY_ITEM		"..\\Release\\Models\\Item@Updown.mv1"
	#define ITEM_WIDTH		60
	#define ITEM_HEGHT		60

// --- エフェクト
#define PATH_FIRE			"..\\Release\\Models\\LAZER.mv1"
	#define PATH_FIRE_ANIM	"..\\Release\\Models\\LAZER_anim.mv1"

#define PATH_FIRE_WEAK		 "..\\Release\\Models\\Effect_fire_Weak.mv1"
	#define PATH_FIRE_WEAK_ANIM "..\\Release\\Models\\Effect_fire_Weak_anim.mv1"

#define PATH_FIRE_STRONG	 "..\\Release\\Models\\Effect_fire_Strong.mv1"
	#define PATH_FIRE_STRONG_ANIM "..\\Release\\Models\\Effect_fire_Strong_anim.mv1"

#define PATH_FIRE_ACCELERATED	 "..\\Release\\Models\\ACCELERATED.mv1"
	#define PATH_FIRE_ACCELERATED_ANIM "..\\Release\\Models\\ACCELERATED_anim.mv1"

#define PATH_GARD_DEFAULT	 "..\\Release\\Models\\Gard_default.mv1"
#define PATH_GARD_MASSIVE	 "..\\Release\\Models\\Gard_Massive.mv1"
#define PATH_GARD_USAGI		 "..\\Release\\Models\\Gard_Usagi.mv1"


// --- サウンド(BGM&SEでプレイヤーでつかわないサウンド)
#define PATH_SOUND_BGM_START				"..\\Release\\Models\\Sound\\BGM_start.ogg"				// --- gc_Main_Sound[0]
#define PATH_SOUND_BGM_CHARASELECT			"..\\Release\\Models\\Sound\\BGM_charaselect.ogg"		// --- gc_Main_Sound[1]
#define PATH_SOUND_BGM_GAMEPLAY				"..\\Release\\Models\\Sound\\BGM_game.ogg"				// --- gc_Main_Sound[2]
#define PATH_SOUND_BGM_RESULT				"..\\Release\\Models\\Sound\\BGM_result.ogg"			// --- gc_Main_Sound[3]
#define PATH_SOUND_SE_STARTBUTTON_DECISION	"..\\Release\\Models\\Sound\\Start_Button_Decision.ogg"	// --- gc_Main_Sound[4]
#define PATH_SOUND_SE_SELECT_END			"..\\Release\\Models\\Sound\\select_end.ogg"			// --- gc_Main_Sound[5]
#define PATH_SOUND_SE_POWERUP				"..\\Release\\Models\\Sound\\powerup.ogg"				// --- [PLAYER_1][20]
#define PATH_SOUND_SE_BOOST					"..\\Release\\Models\\Sound\\boost.ogg"					// --- [PLAYER_1][21]

// --- プレイヤー１，２で使用しているサウンド
#define PATH_SOUND_SE_PUNCH_SWING			"..\\Release\\Models\\Sound\\robot_punch_swing.ogg"		// --- gc_Sound[PLAYER_1][0]
#define PATH_SOUND_SE_PUNCH_HIT_1			"..\\Release\\Models\\Sound\\robot_hit1.ogg"			// --- gc_Sound[PLAYER_1][1]
#define PATH_SOUND_SE_KICK_SWING			"..\\Release\\Models\\Sound\\kick_swing.ogg"			// --- gc_Sound[PLAYER_1][2]
#define PATH_SOUND_SE_KICK_HIT				"..\\Release\\Models\\Sound\\robot_kick_hit.ogg"		// --- gc_Sound[PLAYER_1][3]
#define PATH_SOUND_SE_JUMP					"..\\Release\\Models\\Sound\\jump.ogg"					// --- gc_Sound[PLAYER_1][4]
#define PATH_SOUND_SE_JUMP_LANDING			"..\\Release\\Models\\Sound\\Landing.ogg"				// --- gc_Sound[PLAYER_1][5]
#define PATH_SOUND_SE_WALK_1				"..\\Release\\Models\\Sound\\robot_walk1.ogg"			// --- gc_Sound[PLAYER_1][6]
#define PATH_SOUND_SE_PLAYER_DOWN			"..\\Release\\Models\\Sound\\player_down.ogg"			// --- gc_Sound[PLAYER_1][7]
#define PATH_SOUND_SE_GARD					"..\\Release\\Models\\Sound\\Gard.ogg"					// --- gc_Sound[PLAYER_1][8]
#define PATH_SOUND_SE_GLASS_CRACK			"..\\Release\\Models\\Sound\\glass_crack.ogg"			// --- gc_Sound[PLAYER_1][9]
#define PATH_SOUND_SE_GLASS_BREAK			"..\\Release\\Models\\Sound\\glass_break.ogg"			// --- gc_Sound[PLAYER_1][10]
#define PATH_SOUND_SE_B						"..\\Release\\Models\\Sound\\scream1.ogg"				// --- gc_Sound[PLAYER_1][11]
#define PATH_SOUND_SE_G						"..\\Release\\Models\\Sound\\scream-woman1.ogg"			// --- gc_Sound[PLAYER_1][12]
#define PATH_SOUND_SE_LAZER					"..\\Release\\Models\\Sound\\Lazer.ogg"					// --- gc_Sound[PLAYER_1][13]
#define PATH_SOUND_SE_SELECT				"..\\Release\\Models\\Sound\\Select.ogg"				// --- gc_Sound[PLAYER_1][14]
#define PATH_SOUND_SE_SELECT_1				"..\\Release\\Models\\Sound\\select_1.ogg"				// --- gc_Sound[PLAYER_1][15]
#define PATH_SOUND_SE_CANCEL_MAIN			"..\\Release\\Models\\Sound\\cancel_main.ogg"			// --- gc_Sound[PLAYER_1][16]
#define PATH_SOUND_SE_CANCEL_1				"..\\Release\\Models\\Sound\\cancel1.ogg"				// --- gc_Sound[PLAYER_1][17]
#define PATH_SOUND_SE_CANCEL_2				"..\\Release\\Models\\Sound\\cancel2.ogg"				// --- gc_Sound[PLAYER_1][18]
#define PATH_SOUND_SE_STARTBUTTON			"..\\Release\\Models\\Sound\\Start_Button.ogg"			// --- gc_Sound[PLAYER_1][19]

// --- まだ使用していないサウンド(リザルトなどで使用)
#define PATH_SOUND_SE_PUNCH_HIT_2			"..\\Release\\Models\\Sound\\robot_hit2.ogg"			// --- [PLAYER_1][]
#define PATH_SOUND_SE_PUNCH_HIT_3			"..\\Release\\Models\\Sound\\robot_hit3.ogg"			// --- [PLAYER_1][]
#define PATH_SOUND_SE_CHARA_DOWN			"..\\Release\\Models\\Sound\\chara_down.ogg"			// --- [PLAYER_1][]
#define PATH_SOUND_SE_LAZER_1				"..\\Release\\Models\\Sound\\Lazer1.ogg"				// --- [PLAYER_1][]
#define PATH_SOUND_SE_RESULT_1				"..\\Release\\Models\\Sound\\result_1.ogg"				// --- [PLAYER_1][]
#define PATH_SOUND_SE_RESULT_1_1			"..\\Release\\Models\\Sound\\result_1_1.ogg"			// --- [PLAYER_1][]
#define PATH_SOUND_SE_RESULT_2				"..\\Release\\Models\\Sound\\result_2.ogg"				// --- [PLAYER_1][]
#define PATH_SOUND_SE_RESULT_END			"..\\Release\\Models\\Sound\\result_end.ogg"			// --- [PLAYER_1][]
#define PATH_SOUND_SE_WALK_2				"..\\Release\\Models\\Sound\\robot_walk2.ogg"			// --- [PLAYER_1][]
#define PATH_SOUND_SE_SUMMON_1				"..\\Release\\Models\\Sound\\Summon_1.ogg"				// --- [PLAYER_1][]
#define PATH_SOUND_SE_SUMMON_2				"..\\Release\\Models\\Sound\\Summon_2.ogg"				// --- [PLAYER_1][]
#define PATH_SOUND_SE_TIMEUP_AGO			"..\\Release\\Models\\Sound\\timeup_ago.ogg"			// --- [PLAYER_1][]
#define PATH_SOUND_SE_BATTLE_START			"..\\Release\\Models\\Sound\\battle_start.ogg"			// --- [PLAYER_1][]
#define PATH_SOUND_SE_BATTLE_END			"..\\Release\\Models\\Sound\\battle_end.ogg"			// --- [PLAYER_1][]
#define PATH_SOUND_SE_CANCEL_3				"..\\Release\\Models\\Sound\\cancel3.ogg"				// --- [PLAYER_1][]
#define PATH_SOUND_SE_SAPCE					"..\\Release\\Models\\Sound\\space.ogg"					// --- [PLAYER_1][]
#define PATH_SOUND_SE_SWORD_DRAW			"..\\Release\\Models\\Sound\\sword_draw.ogg"			// --- [PLAYER_1][]
#define PATH_SOUND_SE_SWORD_ATTACK			"..\\Release\\Models\\Sound\\sword_attack.ogg"			// --- [PLAYER_1][]

// --- エフェクトオブジェクト
extern Lazer	gc_Lazer[MAX_EFFECT] ;
extern Gard		gc_Gard[MAX_EFFECT] ;
extern FireWeak gc_Fire_Weak[MAX_EFFECT] ;
extern FireStrong gc_Fire_Strong[MAX_EFFECT] ;
extern Accelerated     gc_Accelerated[MAX_EFFECT] ;
extern Sound			gc_Sound[MAX_PLAYER][MAX_SOUND] ;
extern Sound			gc_Main_Sound[MAX_BS] ;

// --- ボタン
#define PAD_TRIANGLE	PAD_INPUT_A			// --- △ボタン
#define PAD_CIRCLE		PAD_INPUT_B			// --- ○ボタン
#define PAD_SQUARE		PAD_INPUT_X			// --- □ボタン
#define PAD_CROSS		PAD_INPUT_C			// --- ×ボタン
#define PAD_L1			PAD_INPUT_L			// --- Ｌ２ボタン
#define PAD_L2			PAD_INPUT_Y			// --- Ｌ２ボタン
#define PAD_R1			PAD_INPUT_R			// --- Ｒ１ボタン
#define PAD_R2			PAD_INPUT_Z			// --- Ｒ２ボタン
#define PAD_START		PAD_INPUT_M			// --- STARTボタン
#define PAD_M			PAD_INPUT_START		// --- SELECTボタン

#define PAD_UP			PAD_INPUT_UP		// --- ↑ボタン
#define PAD_DOWN		PAD_INPUT_DOWN		// --- ↓ボタン
#define PAD_LEFT		PAD_INPUT_LEFT		// --- ←ボタン
#define PAD_RIGHT		PAD_INPUT_RIGHT		// --- →ボタン


/* ============================================================ +/
/+																+/
/+	型宣言														+/
/+																+/
/+ ============================================================ */
enum { GM_Start, GM_CharSelect, GM_GameInitSet, GM_Playgame, GM_Result, GM_End } ;	// --- ゲームモード
enum { D_DOWN, D_LEFT, D_UP, D_RIGHT } ;								// --- キャラクター向き

enum { A_STAYL_1P, A_STAYR_1P, A_RUN_1P, A_JUMPL_1P, A_JUMPR_1P,			// --- アニメーション番号　1P
		A_HATTACKL_1P, A_HATTACKR_1P, A_LATTACKL_1P, A_LATTACKR_1P,
		A_GUARDL_1P, A_GUARDR_1P, A_BOOSTL_1P, A_BOOSTR_1P,
		A_FIGHTENEDL_1P, A_FIGHTENEDR_1P,
		A_DOWNL_1P, A_DOWNR_1P, A_GREATDEATHL_1P, A_GREATDEATHR_1P } ;

enum { A_STAYL_2P, A_STAYR_2P, A_RUN_2P, A_JUMPL_2P, A_JUMPR_2P,			// --- アニメーション番号　2P
		A_HATTACKL_2P, A_HATTACKR_2P, A_LATTACKL_2P, A_LATTACKR_2P,
		A_GUARDL_2P, A_GUARDR_2P, A_BOOSTL_2P, A_BOOSTR_2P,
		A_FIGHTENEDL_2P, A_FIGHTENEDR_2P,
		A_DOWNL_2P, A_DOWNR_2P, A_GREATDEATHL_2P, A_GREATDEATHR_2P } ;

enum { A_STAY_ITEM, A_WIN_PLAY1, A_LOSE_PLAY1, A_WIN_PLAY2, A_LOSE_PLAY2 } ;	// --- アニメーション番号　その他オブジェクト

enum { S_NON = -1, S_STAY, S_RUN, S_JUMP,S_LANECHANGE, S_BOOST, S_HATTACK, S_LATTACK,
		S_FIGHTENED, S_KNOCKBACK, S_DOWN, S_GUARD, S_GDL} ;

/* ============================================================ +/
/+																+/
/+	プロトタイプ宣言											+/
/+																+/
/+ ============================================================ */
// --- シーン
void SceneMain() ;			// --- メイン
int Scene_Start() ;			// --- スタート
int Scene_CharSelect() ;	// --- キャラ選択
int Scene_GameInitSet() ;
int Scene_Playgame() ;		// --- ゲーム中
int Scene_Result() ;		// --- リザルト
int Scene_End() ;			// --- ゲーム終了

// --- 汎用
int SetAnimation( int pno, int anim_type, TCHAR anim_path[] ) ;	// --- アニメーションデータセット	プレイヤー用
int PlayAnim( int pno ) ;										// --- アニメーション再生			プレイヤー用
int SetObjAnimation( int anim_type, TCHAR anim_path[] ) ;			// --- その他オブジェクトアニメーションデータセット
int PlayObjAnim( int ono ) ;										// --- アニメーション再生
int SetWinloseAnimation( int pno, int anim_type, TCHAR anim_path[] ) ;
int PlayWinloseAnim( int wlno ) ;										// --- アニメーション再生
int SelectAnim( int playno, int animno ) ;						// --- アニメーション選択
int SetCameraPos( void ) ;										// --- カメラ移動

/* ============================================================ +/
/+																+/
/+	外部参照宣言												+/
/+																+/
/+ ============================================================ */
// --- オブジェクト
extern Player		gc_Player[MAX_PLAYER] ;					// --- プレイヤーオブジェクト
extern Object		gf_Stage ;								// --- ステージオブジェクト
extern Object		gf_Undersig[MAX_PLAYER] ;				// --- 足元オブジェクト
extern Object		gc_Item ;								// --- アイテムオブジェクト
extern Object		gc_winlose[MAX_PLAYER] ;

// --- 変数
extern int			gv_WindowMode ;							// --- ウィンドウモード
extern int			gv_PadStatus[] ;						// --- キー状態
extern int			gv_Gamemode ;							// --- シーン管理

extern int			gv_rootflm[MAX_PLAYER] ;				// --- ルートフレーム
extern int			gv_hAnim1P[MAX_ANIM] ;					// --- アニメーションハンドル　1P
extern int			gv_hAnim2P[MAX_ANIM] ;					// --- アニメーションハンドル　2P
extern int			gv_attachidx[MAX_PLAYER] ;				// --- アタッチインデックス
extern float		gv_anim_totaltime[MAX_PLAYER] ;			// --- アニメーション総時間
extern float		gv_anim_timestock1P[MAX_ANIM] ;			// --- アニメーション別総時間ストック　1P
extern float		gv_anim_timestock2P[MAX_ANIM] ;			// --- アニメーション別総時間ストック　2P
extern float		gv_anim_playtime[MAX_PLAYER]  ;			// --- アニメーション進行時間

extern int			gv_rootflmwl[MAX_PLAYER] ;				// --- ルートフレーム
extern int			gv_hAnimwl[MAX_ANIM] ;					// --- アニメーションハンドル　1P
extern int			gv_attachidxwl[MAX_PLAYER] ;				// --- アタッチインデックス
extern float		gv_anim_totaltimewl[MAX_PLAYER] ;			// --- アニメーション総時間
extern float		gv_anim_timestockwl[MAX_ANIM] ;			// --- アニメーション別総時間ストック　1P
extern float		gv_anim_playtimewl[MAX_PLAYER]  ;			// --- アニメーション進行時間

// --- オブジェクト用アニメーション変数
extern int			gv_rootflmobj[MAX_OBJECT] ;				// --- ルートフレーム
extern int			gv_hAnimobj[MAX_ANIM] ;					// --- アニメーションハンドル
extern int			gv_attachidxobj[MAX_OBJECT] ;			// --- アタッチインデックス
extern float		gv_anim_totaltimeobj[MAX_OBJECT] ;		// --- アニメーション総時間
extern float		gv_anim_timestockobj[MAX_ANIM] ;		// --- アニメーション別総時間ストック
extern float		gv_anim_playtimeobj[MAX_OBJECT]  ;		// --- アニメーション進行時間

extern int			gv_win_width ;							// --- ウィンドウ横幅
extern int			gv_win_height ;							// --- ウィンドウ高さ
extern int			gv_gnbackimg ;							// ---  
extern int			gv_gnselectbackimg ;					// --- 
extern int			gv_gnfadeimg ;							// --- フェードアウト・イン用画像
extern int			gv_gnkillimg ;							// --- 
extern int			gv_numimg[12] ;							// --- 番号画像
extern int			gv_hpframeimg[2] ;						// --- ＨＰバーのフレーム
extern int			gv_hpgage[1450] ;						// --- ＨＰバーの中身
extern int			gv_stmngage[] ;							// --- スタミナゲージ
extern int			gv_selectarrowLimg[] ;					// ---
extern int			gv_selectarrowRimg[] ;					// ---
extern int			gv_selectcursor1P ;						// ---
extern int			gv_selectcursor2P ;						// ---
extern int			gv_selectok1P ;							// ---
extern int			gv_selectok2P ;							// ---
extern int			gv_selectyesno ;						// ---
extern int			gv_selectready ;						// --- 
extern int			gv_titleimg ;							// --- タイトル文字画像
extern int			gv_anybuttonimg ;						// --- "PRESS ANY BUTTON TO START"画像
extern int			gv_gnkillcover ;						// --- キルカウンターのカバー 
extern int			gv_win ;
extern int			gv_lose ;
extern int			gv_selectplayer[] ;						// --- セレクト画面　プレイヤー番号
extern int			gv_lastkill ;

extern BOOL			gv_gamestarttim ;						// --- ゲームスタートタイマー		
extern DATEDATA		gv_nowtimer ;							// --- タイマー
extern int			gv_limittime ;							// --- 
extern int			gv_tmptimer ;
extern int			gv_hpmax[MAX_PLAYER] ;
extern TCHAR		debug_str[] ;

extern BOOL reflg ;
extern BOOL reendflg ;


/*- [EOF] -*/
