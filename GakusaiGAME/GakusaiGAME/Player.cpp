/* ============================================================ +/ 

	LastEdit : 2020/09/25		NAME:佐藤圭桃
	LastEdit : ----/--/--		NAME:
	LastEdit : 2020/10/16		NAME:宮崎櫻
	___________________________________________________________

		Player.cpp
	___________________________________________________________

		プレイヤークラスの実装部

/+ ============================================================ */
#include <Windows.h>
#include <DxLib.h>

#include "Common.h"

BOOL waitanim_stay[MAX_PLAYER]		= { TRUE, TRUE } ;	// --- アニメーション切り替えフラグ　待ち
BOOL waitanim_atk[MAX_PLAYER]		= { TRUE, TRUE } ;	// --- アニメーション切り替えフラグ　攻撃
BOOL waitanim_run[MAX_PLAYER]		= { TRUE, TRUE } ;	// --- アニメーション切り替えフラグ　走り
BOOL waitanim_jump[MAX_PLAYER]		= { TRUE, TRUE } ;	// --- アニメーション切り替えフラグ　ジャンプ
BOOL waitanim_fightened[MAX_PLAYER] = { TRUE, TRUE } ;	// --- アニメーション切り替えフラグ　怯み
BOOL waitanim_downstart[MAX_PLAYER] = { TRUE, TRUE } ;	// --- アニメーション切り替えフラグ　ダウン開始
BOOL waitanim_down[MAX_PLAYER]		= { TRUE, TRUE } ;	// --- アニメーション切り替えフラグ　ダウン開始
BOOL waitanim_boost[MAX_PLAYER]		= { TRUE, TRUE } ;
BOOL waitanim_gdl[MAX_PLAYER]		= { TRUE, TRUE } ;

int tmp_ano[MAX_PLAYER] = { 0, 0 } ;	// --- デバッグ用　アニメーション番号

/* ------------------------------------------------------------ +/

	コンストラクタ
 + ------------------------------------------------------------ +
		-引数-
			void
		-返り値-
			void
/+ ------------------------------------------------------------ */
Player::Player( )
{
	// --- 初期セット
	hModel = 0 ;								// --- モデルハンドル
	hGModel = 0 ;
	ModelNo = 0 ;
	tmp_pos = pos = VGet( 0.0f, 0.0f, 0.0f ) ;	// --- 座標
	tmp_spd = spd = VGet( 0.0f, 0.0f, 0.0f ) ;	// --- 速度
	direction = D_DOWN ;						// --- 向き
	playno = PLAYER_1 ;							// --- プレイヤー番号 
	statusflg = S_STAY ;							// --- 状態フラグ
	waitflg = FALSE ;							// --- キー入力待ち時間
	laneno = 0 ;								// --- レーン番号
	hp = PLAYER_HP ;							// --- プレイヤーHP
	stamina = PLAYER_STAMINA ;					// --- プレイヤースタミナ
	killcount = 0 ;								// --- キル数
	gdflg = FALSE ;								// --- 大必殺フラグ
	gbreakflg = 0 ;								// --- ガードブレイクフラグ
	downflg = 0 ;								// --- 蓄積ダウンフラグ
	down_resettim = 0.0f ;						// --- 蓄積ダウンリセット時間
	down_maxtim = 0.0f ;						// --- 最大ダウン時間 
	invinflg = FALSE ;							// --- 無敵フラグ
	invintime = 0.0f ;							// --- 無敵時間
}

/* ------------------------------------------------------------ +/

	デスクトラクタ
 + ------------------------------------------------------------ +
		-引数-
			void
		-返り値-
			void
/+ ------------------------------------------------------------ */
Player::~Player( ){}

/* ------------------------------------------------------------ +/

	プレイヤー番号セット
 + ------------------------------------------------------------ +
		-引数-
			BOOL プレイヤー番号  PLAYER_1 : 0	PLAYER_2 : 1
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Player::SelectPlayer( BOOL no )
{
	playno = no ;

	if ( playno == PLAYER_1 ) dirinfo = D_RIGHT ;
	if ( playno == PLAYER_2 ) dirinfo = D_LEFT ;

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	モデルハンドルセット
 + ------------------------------------------------------------ +
		-引数-
			TCHAR[]	通常モデルファイルパス
			TCHAR[] 大必殺モデルファイルパス
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Player::SetModelHandle( TCHAR mpath[], TCHAR mgpath[] )
{
	hModel = MV1LoadModel( mpath ) ;
	hGModel = MV1LoadModel( mgpath ) ;
	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	モデルナンバーセット
 + ------------------------------------------------------------ +
		-引数-
			int 整数値
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Player::SetModelNumber( int PMno )
{
	ModelNo = PMno ;
	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	モデル横幅セット
 + ------------------------------------------------------------ +
		-引数-
			float 実数値
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Player::SetModelWidth( float PMwd )
{
	ModelWidth = PMwd ;
	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	座標セット
 + ------------------------------------------------------------ +
		-引数-
			VECTOR 座標
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Player::SetPosition( VECTOR ps, VECTOR tmp_ps )
{
	pos = ps ;
	tmp_pos = tmp_ps ;
	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	キル数セット
 + ------------------------------------------------------------ +
		-引数-
			int 整数値
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Player::SetKillcount( void )
{
	killcount++ ;
	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	ＨＰ減少セット
 + ------------------------------------------------------------ +
		-引数-
			int 整数値
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Player::SetHpDecrease( void )
{
	int tmp_hp = 0 ;	// --- ダメージ量

	if ( ModelNo == STANDERD_NUMBER )	tmp_hp = 103 ;
	if ( ModelNo == MASSIVE_NUMBER )	tmp_hp = 123 ;
	if ( ModelNo == USAGI_NUMBER )		tmp_hp = 82 ;

	hp -= tmp_hp ;

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	スタミナ減少セット
 + ------------------------------------------------------------ +
		-引数-
			int 整数値
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Player::SetStamDecrease( void )
{
	stamina -= 126.0f ;

	if ( stamina <= 0 )
	{
		stamina = 0.0f ;
	}

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	チャージカウントセット
 + ------------------------------------------------------------ +
		-引数-
			int 整数値
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Player::SetChargeCnt( int cnt )
{
	chargecnt = cnt ;

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	状態フラグセット
 + ------------------------------------------------------------ +
		-引数-
			int 状態	S_???
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Player::SetStatus( int stats ){

	statusflg = stats ;

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	向き情報セット
 + ------------------------------------------------------------ +
		-引数-
			int 向き情報	D_???
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Player::SetDirinfo( int dinfo )
{
	dirinfo = dinfo ;	// --- 向き情報セット

	return( TRUE ) ;
}


/* ------------------------------------------------------------ +/

	アクション
 + ------------------------------------------------------------ +
		-引数-
			void
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Player::MainAction( void )
{
	//wsprintf( debug_str, "--- PLAYER_1 ---\nSTATUS : %d\nPLAYNG_ANIM : %d\n--- PLAYER_2 ---\nSTAUTS : %d\nPLAYING_ANIM : %d",
	//	gc_Player[PLAYER_1].GetPlayerStatus(), 0, gc_Player[PLAYER_2].GetPlayerStatus(), 0 ) ;

	if ( gdflg == TRUE )
	{
		statusflg = S_GDL ;
	}

	if( gv_Gamemode == GM_Result )
	{
		direction = D_DOWN ;

		if ( playno == PLAYER_1 )
		{
			dirinfo = D_RIGHT ;
		}
		if ( playno == PLAYER_2 )
		{
			dirinfo = D_LEFT ;
		}
	}

	StaminaReset() ;
	if ( hp <= 0 )
	{
		hp = 0 ;
		HpReset() ;
		stamina = PLAYER_STAMINA ;
		statusflg = S_DOWN ;
	}
	else
	{
		switch( statusflg ){
			// --- 待ち状態
			case S_STAY :
				ActStay( ) ;
				printf( "%d:S_STAY\n", playno+1 ) ;
				break ;

			// --- 走り状態
			case S_RUN :
				ActRun( ) ;
				printf( "%d:S_RUN\n", playno+1 ) ;
				break ;

			// --- ジャンプ中
			case S_JUMP :
				ActJump( ) ;
				printf( "%d:S_JUMP\n", playno+1 ) ;
				break ;
		
			// --- レーン切り替え
			case S_LANECHANGE :
				ActLaneChange( dirinfo ) ;
				printf( "%d:S_LANECHANGE\n", playno+1 ) ;
				break ;

			// --- ブースト
			case S_BOOST :
				ActBst( ) ;
				printf( "%d:S_BOOST\n", playno+1 ) ;
				break ;

			// --- 腕攻撃
			case S_HATTACK :
				ActBeforeAttack( ) ;
				printf( "%d:S_HATTACK\n", playno+1 ) ;
				break ;

			// --- 脚攻撃
			case S_LATTACK :
				ActBeforeAttack( ) ;
				printf( "%d:S_LATTACK\n", playno+1 ) ;
				break ;

			// --- 怯み
			case S_FIGHTENED :
				ActFightened( ) ;
				printf( "%d:S_FIGHTENED\n", playno+1 ) ;
				break ;

			// --- ノックバック
			case S_KNOCKBACK :
				ActKnockBack( TRUE ) ;
				printf( "%d:S_KNOCKBACK\n", playno+1 ) ;
				break ;

			// --- ダウン中
			case S_DOWN :
				ActDown( ) ;
				printf( "%d:S_DOWN\n", playno+1 ) ;
				break ;

			// --- ガード
			case S_GUARD :
				ActGuard( ) ;
				printf( "%d:S_GUARD\n", playno+1 ) ;
				break ;

			// --- 大必殺
			case S_GDL :
				ActHissatsu2( ) ;
				printf( "%d:S_GDL\n", playno+1 ) ;
				break ;
		}
	}

	if ( downflg >= 4 ){
		statusflg = S_DOWN ;
		downflg = 0 ;
	}

	// --- ダウン時間遷移
	down_maxtim -= 0.1f ;		// --- 最大ダウン時間減少
	if ( down_maxtim <= 0.0f )	down_maxtim = 0.0f ;	// --- 最小リミット

	down_resettim -= 0.1f ;		// --- 蓄積ダウンリセット時間
	if ( down_resettim <= 0.0f ){
		down_resettim = 0.0f ;		// --- 時間リセット
		downflg = 0 ;		// --- フラグリセット
	}

	invintime -= 0.1f ;			// --- 無敵時間
	if ( invintime <= 0.0f ){
		invintime = 0.0f ;
		invinflg = FALSE ;
	}	
	// --- アニメ再生
	PlayAnim( playno ) ;

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	座標、向き更新
 + ------------------------------------------------------------ +
		-引数-
			void
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Player::UpdatePosDir( void )
{
	MV1SetPosition( hModel, pos ) ;
	MV1SetRotationXYZ( hModel, VGet(0.0f, 1.57f * direction, 0.0f) ) ;
	MV1SetScale( hModel, VGet(1.0f, 1.0f, 1.0f) ) ;

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	キー情報別アクション
 + ------------------------------------------------------------ +
		-引数-
			void
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Player::CheckKeyAction( void )
{
	/* ------------------------------------------------------------ +/
		↑ボタン　奥へレーンチェンジ
	/+ ------------------------------------------------------------ */
	if ( (gv_PadStatus[playno] & PAD_UP) &&
		 (waitflg == FALSE) &&
		 (laneno < 1) ){
		// --- 入力条件
		if ( (statusflg == S_RUN) || (statusflg == S_STAY) ){
			spd.y = 7.0f ;
			direction = D_UP ;
			laneno++ ;						// --- レーン番号を奥に
			tmp_pos = pos ;					// --- 入力時の位置保存
			statusflg = S_LANECHANGE ;		// --- 状態切り替え
			waitanim_stay[playno] = TRUE ; // --- アニメーション切り替えフラグ
		}
	}

	/* ------------------------------------------------------------ +/
		↓ボタン　手前へレーンチェンジ
	/+ ------------------------------------------------------------ */
	if ( (gv_PadStatus[playno] & PAD_DOWN) &&
		 (waitflg == FALSE) &&
		 ( laneno > -1 ) ){
		// --- 入力条件
		if ( (statusflg == S_RUN) || (statusflg == S_STAY) ){
			spd.y = 7.0f ;
			direction = D_DOWN ;
			laneno-- ;					// --- レーン番号を奥に
			tmp_pos = pos ;				// --- 入力時の位置保存
			statusflg = S_LANECHANGE ;	// --- 状態切り替え
			waitanim_stay[playno] = TRUE ;
		}
	}

	/* ------------------------------------------------------------ +/
		←ボタン　左に移動
	/+ ------------------------------------------------------------ */
	if ( gv_PadStatus[playno] & PAD_LEFT ){
		// --- 入力条件
		if ( (statusflg == S_RUN) || (statusflg == S_STAY) ){
			statusflg = S_RUN ;				// --- 状態切り替え
			direction = dirinfo = D_LEFT ;	// --- 方向情報セット
			waitanim_stay[playno] = TRUE ;
		}
	}

	/* ------------------------------------------------------------ +/
		→ボタン　右に移動
	/+ ------------------------------------------------------------ */
	if ( gv_PadStatus[playno] & PAD_RIGHT ){
		// --- 入力条件
		if ( (statusflg == S_RUN) || (statusflg == S_STAY) ){
			statusflg = S_RUN ;					// --- 状態切り替え
			direction = dirinfo = D_RIGHT ;	// --- 方向情報セット
			waitanim_stay[playno] = TRUE ;
		}
	}

	/* ------------------------------------------------------------ +/
		×ボタン(zキー)　ジャンプ
	/+ ------------------------------------------------------------ */
	if ( gv_PadStatus[playno] & PAD_CROSS ){
		// --- 入力条件
		if ( (statusflg == S_RUN) || (statusflg == S_STAY) &&
			(stamina >= 200) ){
			spd.y = 10.0f ;			// --- 初期スピードセット
			statusflg = S_JUMP ;	// --- 状態切り替え
			SetStamDecrease() ;
			waitanim_stay[playno] = TRUE ;
		}
	}

	/* ------------------------------------------------------------ +/
		□ボタン(xキー)　ブースト　
	/+ ------------------------------------------------------------ */
	if ( (gv_PadStatus[playno] & PAD_SQUARE) && (waitflg == FALSE) ){
		// --- 入力条件
		if ( (statusflg == S_RUN) || (statusflg == S_STAY) &&
			((stamina-126.0f) > 0) ){
			tmp_pos = pos ;			// --- 位置保存
			statusflg = S_BOOST ;	// --- 状態切り替え			
			waitflg = TRUE ;
			SetStamDecrease() ;
			waitanim_stay[playno] = TRUE ;
		}
	}

	/* ------------------------------------------------------------ +/
		○ボタン(aキー)　腕攻撃　
	/+ ------------------------------------------------------------ */
	if ( (gv_PadStatus[playno] & PAD_CIRCLE) && (waitflg == FALSE) ){
		// --- 入力条件
		if ( (statusflg == S_RUN) || (statusflg == S_STAY) ){
			direction = D_DOWN ;
			statusflg = S_HATTACK ;
			waitflg = TRUE ;
			waitanim_stay[playno] = TRUE ;
			waitanim_run[playno] = TRUE ;
		}
	}

	/* ------------------------------------------------------------ +/
		△ボタン(sキー)　脚攻撃　
	/+ ------------------------------------------------------------ */
	if ( (gv_PadStatus[playno] & PAD_TRIANGLE) && (waitflg == FALSE) ){
		// --- 入力条件
		if ( (statusflg == S_RUN) || (statusflg == S_STAY) ){
			direction = D_DOWN ;	
			statusflg = S_LATTACK ;
			waitflg = TRUE ;
			waitanim_stay[playno] = TRUE ;
			waitanim_run[playno] = TRUE ;
		}
	}

	/* ------------------------------------------------------------ +/
		R1ボタン(キー)　ガード
	/+ ------------------------------------------------------------ */
	if ( (gv_PadStatus[playno] & PAD_R1) && (waitflg == FALSE) ){
		// --- 入力条件
		if ( (statusflg == S_RUN) || (statusflg == S_STAY) &&
			(gbreakflg >= 4)){
			direction = D_DOWN ;
			statusflg = S_GUARD ;
			waitanim_stay[playno] = TRUE ;
		}
	}

	/* ------------------------------------------------------------ +/
		ニュートラル
	/+ ------------------------------------------------------------ */
	if ( gv_PadStatus[playno] == 0 ){
		waitflg = FALSE ;	// --- 待ちフラグ
	}

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	ヒットチェック
 + ------------------------------------------------------------ +
		-引数-
			void
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
BOOL Player::CheckHit( void )
{
	BOOL hitcheckflg = FALSE ;
	VECTOR tmp_pos1, tmp_pos2 ;
	float tmp_w1, tmp_w2 ;

	tmp_pos1 = VAdd(pos,spd) ;
	tmp_pos2 = VAdd(gc_Player[PLAYER_2].GetPosition() , gc_Player[PLAYER_2].GetSpeed()) ;
	tmp_w1 = (float)(ModelWidth / 2) ;
	tmp_w2 = (float)(gc_Player[PLAYER_2].GetModelWidth() / 2) ;

	/* ------------------------------------------------------------ +/
		プレイヤー１
	/+ ------------------------------------------------------------ */
	if ( playno == PLAYER_1 )
	{
		if ( gc_Player[PLAYER_2].GetPlayerStatus() != S_LANECHANGE ){
			tmp_pos1 = VAdd(pos,spd) ;
			tmp_pos2 = VAdd(gc_Player[PLAYER_2].GetPosition() , gc_Player[PLAYER_2].GetSpeed()) ;
			tmp_w1 = (float)(ModelWidth / 2) ;
			tmp_w2 = (float)(gc_Player[PLAYER_2].GetModelWidth() / 2) ;

			if( HitCheck_Capsule_Capsule(
				tmp_pos1, tmp_pos1, tmp_w1,
				tmp_pos2, tmp_pos2, tmp_w2) == TRUE)
			{
				if ( laneno == gc_Player[PLAYER_2].GetLaneNo() ){			// --- レーン番号が同じ
					if ( dirinfo == D_LEFT ){								// --- 左向きのとき自分より左側にいたら
						if ( tmp_pos1.x > tmp_pos2.x ){
							if( gc_Player[PLAYER_2].GetPlayerStatus() != S_DOWN ) hitcheckflg = TRUE ;	// --- ヒット
						}
					}
					else{													// --- 右向きのとき自分より右側にいたら
						if ( tmp_pos1.x < tmp_pos2.x){
							if( gc_Player[PLAYER_2].GetPlayerStatus() != S_DOWN ) hitcheckflg = TRUE ;	// --- ヒット
						}
					}
				}
			}
		}
	}

	/* ------------------------------------------------------------ +/
		プレイヤー２
	/+ ------------------------------------------------------------ */
	if ( playno == PLAYER_2 )
	{
		tmp_pos1 = VAdd(pos,spd) ;
		tmp_pos2 = VAdd(gc_Player[PLAYER_1].GetPosition() , gc_Player[PLAYER_1].GetSpeed()) ;
		tmp_w1 = (float)(ModelWidth / 2) ;
		tmp_w2 = (float)(gc_Player[PLAYER_1].GetModelWidth() / 2) ;
		
		if ( gc_Player[PLAYER_1].GetPlayerStatus() != S_LANECHANGE ){
			if( HitCheck_Capsule_Capsule(
				tmp_pos1, tmp_pos1, tmp_w1,
				tmp_pos2, tmp_pos2, tmp_w2) == TRUE)
			{
				if ( laneno == gc_Player[PLAYER_1].GetLaneNo() ){				// --- レーン番号が同じ
					if ( dirinfo == D_LEFT ){									// --- 左向きのとき自分より左側にいたら
						if ( tmp_pos1.x > tmp_pos2.x )
							if( gc_Player[PLAYER_1].GetPlayerStatus() != S_DOWN ) hitcheckflg = TRUE ;	// --- ヒット
					}
					else{														// --- 右向きのとき自分より右側にいたら
						if ( tmp_pos1.x < tmp_pos2.x ){
							if( gc_Player[PLAYER_1].GetPlayerStatus() != S_DOWN ) hitcheckflg = TRUE ;	// --- ヒット
						}
					}
				}
			}
		}
	}

	return hitcheckflg ;
}

/* ------------------------------------------------------------ +/

	ＨＰチェック
 + ------------------------------------------------------------ +
		-引数-
			void
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Player::HpReset( void )
{
	if ( playno == PLAYER_1 )
	{
		gc_Player[PLAYER_2].SetKillcount() ;
		hp = PLAYER_HP ;

		// --- 死亡時のサウンド
		PlaySoundMem( gc_Sound[PLAYER_1][11].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
	}
	if ( playno == PLAYER_2 )
	{
		gc_Player[PLAYER_1].SetKillcount() ;
		hp = PLAYER_HP ;

		// --- 死亡時のサウンド
		PlaySoundMem( gc_Sound[PLAYER_2][12].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
	}

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	スタミナチェック
 + ------------------------------------------------------------ +
		-引数-
			void
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Player::StaminaReset( void )
{
	stamina += 2.5f ;

	if ( stamina >= PLAYER_STAMINA )
	{
		stamina = PLAYER_STAMINA ;
	}

	return( TRUE ) ;
}

/* ============================================================ +/
/+																+/
/+	プレイヤーアクション										+/
/+																+/
/+ ============================================================ */

/* ------------------------------------------------------------ +/
	待ち
/+ ------------------------------------------------------------ */
int Player::ActStay( void )
{

	if ( waitanim_stay[playno] ){
		direction = D_DOWN ; // --- 向きセット　下
		MV1DetachAnim( hModel, gv_attachidx[playno] ) ;
		// --- プレイヤー１
		if ( playno == PLAYER_1 ){
			if ( dirinfo == D_LEFT )	tmp_ano[0] = SelectAnim( playno, A_STAYL_1P ) ;
			if ( dirinfo == D_RIGHT )	tmp_ano[0] = SelectAnim( playno, A_STAYR_1P ) ;
		}
		// --- プレイヤー２
		if ( playno == PLAYER_2 ){
			// --- アニメーションセット
			if ( dirinfo == D_LEFT )	tmp_ano[1] = SelectAnim( playno, A_STAYL_2P ) ;
			if ( dirinfo == D_RIGHT )	tmp_ano[1] = SelectAnim( playno, A_STAYR_2P ) ;
		}
		waitanim_stay[playno] = FALSE ;
	}

	spd = VGet( 0.0f, 0.0f, 0.0f) ; // --- 移動量なし

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/
	攻撃																													
/+ ------------------------------------------------------------ */
BOOL waithit = TRUE ;
int Player::ActBeforeAttack( void )
{
	// --- アニメーション
	if ( waitanim_atk[playno] ){
		MV1DetachAnim( hModel, gv_attachidx[playno] ) ;
		// --- 腕攻撃
		if ( statusflg == S_HATTACK ){
			if ( playno == PLAYER_1 ){	// --- プレイヤー１
				// --- プレイヤー１の殴(素振り)のサウンド
				PlaySoundMem( gc_Sound[PLAYER_1][0].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
				if ( dirinfo == D_LEFT )	tmp_ano[0] = SelectAnim( playno, A_HATTACKL_1P ) ;
				if ( dirinfo == D_RIGHT )	tmp_ano[0] = SelectAnim( playno, A_HATTACKR_1P ) ;
			}
			// --- プレイヤー２の殴(素振り)のサウンド
			PlaySoundMem( gc_Sound[PLAYER_2][0].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
			if ( playno == PLAYER_2 ){	// --- プレイヤー２
				if ( dirinfo == D_LEFT )	tmp_ano[1] = SelectAnim( playno, A_HATTACKL_2P ) ;
				if ( dirinfo == D_RIGHT )	tmp_ano[1] = SelectAnim( playno, A_HATTACKR_2P ) ;
			}
			waitanim_atk[playno] ;
		}

		// --- 脚攻撃
		if ( statusflg == S_LATTACK ){
			if ( playno == PLAYER_1 ){	// --- プレイヤー１
				// --- プレイヤー１の蹴り(素振り)のサウンド
				PlaySoundMem( gc_Sound[PLAYER_1][2].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
				if ( dirinfo == D_LEFT )	tmp_ano[0] = SelectAnim( PLAYER_1, A_LATTACKL_1P ) ;
				if ( dirinfo == D_RIGHT )	tmp_ano[0] = SelectAnim( PLAYER_1, A_LATTACKR_1P ) ;
			}
			if ( playno == PLAYER_2 ){	// --- プレイヤー２
				// --- プレイヤー２の蹴り(素振り)のサウンド
				PlaySoundMem( gc_Sound[PLAYER_2][2].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
				if ( dirinfo == D_LEFT )	tmp_ano[1] = SelectAnim( PLAYER_2, A_LATTACKL_2P ) ;
				if ( dirinfo == D_RIGHT )	tmp_ano[1] = SelectAnim( PLAYER_2, A_LATTACKR_2P ) ;
			}
		}
		waitanim_atk[playno] = FALSE ;
	}

	// --- ヒットチェック
	if ( CheckHit( ) && waithit ){
		// --- プレイヤー１
		if ( playno == PLAYER_1 ){
			// --- 殴が当たった時のサウンド
			if ( statusflg == S_HATTACK )
			{
				// --- 弱被弾エフェクト
				gc_Fire_Weak[PLAYER2_EFFECT].weak_flg = TRUE ;
				StopSoundMem( gc_Sound[PLAYER_1][0].GetModelHandle() ) ;
				PlaySoundMem( gc_Sound[PLAYER_1][1].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
			}
			// --- 蹴りが当たった時のサウンド
			if ( statusflg == S_LATTACK )
			{
				gc_Fire_Strong[PLAYER2_EFFECT].strong_flg = TRUE ;
				StopSoundMem( gc_Sound[PLAYER_1][2].GetModelHandle() ) ;
				PlaySoundMem( gc_Sound[PLAYER_1][3].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
			}
			if ( (gc_Player[PLAYER_2].GetPlayerStatus( ) != S_GUARD) ||
				 ((gc_Player[PLAYER_2].GetPlayerStatus( ) == S_GUARD) && dirinfo == gc_Player[PLAYER_2].GetDirinfo()) ){
				gc_Player[PLAYER_2].SetHpDecrease( ) ;		// --- HP減少
				if ( gc_Player[PLAYER_2].GetPlayerStatus() != S_JUMP )	gc_Player[PLAYER_2].SetStatus( S_FIGHTENED ) ;	// --- 相手のステータスをS_FIGHTENED(怯み)に
				else													gc_Player[PLAYER_2].SetStatus( S_DOWN ) ;
			}
			else{
				gc_Player[PLAYER_2].SetPosition( gc_Player[PLAYER_2].GetPosition( ), gc_Player[PLAYER_2].GetPosition( ) ) ;
				gc_Player[PLAYER_2].SetStatus( S_KNOCKBACK ) ;
				gbreakflg++ ;
			}
			if ( dirinfo == D_LEFT )	gc_Player[PLAYER_2].SetDirinfo( D_RIGHT ) ;	// --- 相手の向きを変える
			else						gc_Player[PLAYER_2].SetDirinfo( D_LEFT ) ;
		}
		// --- プレイヤー２
		if ( playno == PLAYER_2 ){
			if ( statusflg == S_HATTACK )
			{
				gc_Fire_Weak[PLAYER1_EFFECT].weak_flg = TRUE ;
				StopSoundMem( gc_Sound[PLAYER_2][0].GetModelHandle() ) ;
				PlaySoundMem( gc_Sound[PLAYER_2][1].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
			}
			// --- 蹴りが当たった時のサウンド
			if ( statusflg == S_LATTACK )
			{
				gc_Fire_Strong[PLAYER1_EFFECT].strong_flg = TRUE ;
				StopSoundMem( gc_Sound[PLAYER_2][2].GetModelHandle() ) ;
				PlaySoundMem( gc_Sound[PLAYER_2][3].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
			}
			if ( (gc_Player[PLAYER_1].GetPlayerStatus( ) != S_GUARD) ||
				 ((gc_Player[PLAYER_1].GetPlayerStatus( ) == S_GUARD) && dirinfo == gc_Player[PLAYER_1].GetDirinfo()) ){
				gc_Player[PLAYER_1].SetHpDecrease( ) ;
				if ( gc_Player[PLAYER_1].GetPlayerStatus() != S_JUMP )	gc_Player[PLAYER_1].SetStatus( S_FIGHTENED ) ;	// --- 相手のステータスをS_FIGHTENED(怯み)に
				else													gc_Player[PLAYER_1].SetStatus( S_DOWN ) ;
			}
			else{
				gc_Player[PLAYER_1].SetPosition( gc_Player[PLAYER_1].GetPosition( ), gc_Player[PLAYER_1].GetPosition( ) ) ;
				gc_Player[PLAYER_1].SetStatus( S_KNOCKBACK ) ;
				gbreakflg++ ;
			}
			if ( dirinfo == D_LEFT )	gc_Player[PLAYER_1].SetDirinfo( D_RIGHT ) ;	// --- 相手の向きを変える
			else						gc_Player[PLAYER_1].SetDirinfo( D_LEFT ) ;
		}
		waithit = FALSE ;
	}

	if ( gv_anim_playtime[playno] <= 0 ){
		statusflg = S_STAY ;			// --- 状態切り替え
		waitanim_atk[playno] = TRUE ;	// --- 攻撃アニメーションセットフラグ
		waithit = TRUE ;
	}

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/
	走る																														
/+ ------------------------------------------------------------ */
int Player::ActRun( void )
{
	// --- アニメーションセット
	if ( waitanim_run[playno]  ){
		MV1DetachAnim( hModel, gv_attachidx[playno] ) ;
		if ( playno == PLAYER_1 )	tmp_ano[0] = SelectAnim( PLAYER_1, A_RUN_1P ) ;	// --- プレイヤー１
		if ( playno == PLAYER_2 )	tmp_ano[1] = SelectAnim( PLAYER_2, A_RUN_2P ) ;	// --- プレイヤー２

		waitanim_run[playno] = FALSE ;
	}

	// --- キャラの向きで移動量分岐
	switch( dirinfo ){
		// --- 左
		case D_LEFT :
			spd.x = -10.0f ;	// --- X軸(-)
			break ;
		// --- 右
		case D_RIGHT :
			spd.x = 10.0f ;	// --- X軸(+)
			break ;
	}

	pos.x += spd.x ;

	if ( ((gv_PadStatus[playno] & PAD_LEFT) <= 0) && ((gv_PadStatus[playno] & PAD_RIGHT) <= 0) ){
		statusflg = S_STAY ;	 // --- 何もキーが押されてなかったら待機へ
		waitanim_run[playno] = TRUE ;
	}

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/
	ジャンプ																														
/+ ------------------------------------------------------------ */
int Player::ActJump( void )
{
	// ---　アニメーションセット

	if ( waitanim_jump[playno] ){
		MV1DetachAnim( hModel, gv_attachidx[playno] ) ;
		// --- プレイヤー１
		if ( playno == PLAYER_1 ){
			// --- プレイヤー１のジャンプサウンド
			PlaySoundMem( gc_Sound[PLAYER_1][4].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
			if ( dirinfo == D_LEFT )	tmp_ano[0] = SelectAnim( playno, A_JUMPL_1P ) ;
			if ( dirinfo == D_RIGHT )	tmp_ano[0] = SelectAnim( playno, A_JUMPR_1P ) ;
		}
		// --- プレイヤー２
		if ( playno == PLAYER_2 ){
			// --- プレイヤー２のジャンプサウンド
			PlaySoundMem( gc_Sound[PLAYER_2][4].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
			if ( dirinfo == D_LEFT )	tmp_ano[1] = SelectAnim( playno, A_JUMPL_2P ) ;
			if ( dirinfo == D_RIGHT )	tmp_ano[1] = SelectAnim( playno, A_JUMPR_2P ) ;
		}
		waitanim_jump[playno] = FALSE ;
	}

	// --- ジャンプ軌道
	spd.y -= 0.4f ;
	pos.y += spd.y ;
	if ( pos.y <= 0.0f ){
		spd.y = 0.0f ;
		statusflg = S_STAY ;
		waitanim_jump[playno] = TRUE ;
		waitanim_stay[playno] = TRUE ;
		waitanim_run[playno]=TRUE ;
		// --- プレイヤー１、２のジャンプ後の着地サウンド
		PlaySoundMem( gc_Sound[PLAYER_1][5].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
		PlaySoundMem( gc_Sound[PLAYER_2][5].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
	}

	// --- ジャンプ中移動
	if ( gv_PadStatus[playno] & PAD_INPUT_LEFT ){
		spd.x = -10.0f ;
		direction = D_DOWN ;
	}
	else if ( gv_PadStatus[playno] & PAD_INPUT_RIGHT ){
		spd.x = 10.0f ;
		direction = D_DOWN ;
	}
	else{
		spd.x= 0.0f ;
	}
	pos.x += spd.x ;

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/
	レーン切り替え
/+ ------------------------------------------------------------ */
int Player::ActLaneChange( int dir ){
	// --- アニメーション
	MV1DetachAnim( hModel, gv_attachidx[playno] ) ;
	// --- プレイヤー１
	if ( playno == PLAYER_1 ){
		// --- プレイヤー２がレーンを切り替えた時
		while( CheckSoundMem( gc_Sound[PLAYER_1][4].GetModelHandle() ) == 0 )
		{
			PlaySoundMem( gc_Sound[PLAYER_1][4].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
		}
		if ( dirinfo == D_LEFT )	tmp_ano[0] = SelectAnim( PLAYER_1, A_JUMPL_1P ) ;
		if ( dirinfo == D_RIGHT )	tmp_ano[0] = SelectAnim( PLAYER_1, A_JUMPR_1P ) ;
	}
	// --- プレイヤー２
	if ( playno == PLAYER_2 ){
		while( CheckSoundMem( gc_Sound[PLAYER_2][4].GetModelHandle() ) == 0 )
		{
			PlaySoundMem( gc_Sound[PLAYER_2][4].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
		}
		if ( dirinfo == D_LEFT )	tmp_ano[1] = SelectAnim( PLAYER_2, A_JUMPL_2P ) ;
		if ( dirinfo == D_RIGHT )	tmp_ano[1] = SelectAnim( PLAYER_2, A_JUMPR_2P ) ;
	}

	// --- ジャンプ軌道
	spd.y -= 0.4f ;
	pos.y += spd.y ;
	if ( pos.y <= 0.0f ){
		spd.y = 0.0f ;
		statusflg = S_STAY ;
		waitanim_stay[playno] = TRUE ;
		waitanim_run[playno] = TRUE ;

		// --- レーン先で着地したときのサウンド
		PlaySoundMem( gc_Sound[PLAYER_1][5].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
		PlaySoundMem( gc_Sound[PLAYER_2][5].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
	}

	// --- 移動
	switch( direction ){
		case D_UP :
			spd.z = 4.0f ;
			break ;

		case D_DOWN :
			spd.z = -4.0f ;
			break ;
	}

	waitflg = TRUE ;
	pos.z += spd.z ;

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/
	ブースト																														
/+ ------------------------------------------------------------ */
int Player::ActBst( void )
{
	// --- ブーストエフェクト
	if ( gv_PadStatus[PLAYER_1] & PAD_SQUARE )
	{
		gc_Accelerated[PLAYER1_EFFECT].accel_flg = TRUE ;
	}
	if ( gv_PadStatus[PLAYER_2] & PAD_SQUARE )
	{
		gc_Accelerated[PLAYER2_EFFECT].accel_flg = TRUE ;
	}

	// --- プレイヤー１、２のジャンプ後の着地サウンド
	while( CheckSoundMem( gc_Sound[PLAYER_1][21].GetModelHandle() ) == 0 )
	{
		PlaySoundMem( gc_Sound[PLAYER_1][21].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
	}
	while( CheckSoundMem( gc_Sound[PLAYER_2][21].GetModelHandle() ) == 0 )
	{
		PlaySoundMem( gc_Sound[PLAYER_2][21].GetModelHandle(), DX_PLAYTYPE_BACK , TRUE) ;
	}

	direction = D_DOWN ;
	if ( waitanim_boost[playno] ){
		MV1DetachAnim( hModel, gv_attachidx[playno] ) ;
		// --- プレイヤー１
		if ( playno == PLAYER_1 ){
			if ( dirinfo == D_LEFT )	tmp_ano[0] = SelectAnim( PLAYER_1, A_BOOSTL_1P ) ;
			if ( dirinfo == D_RIGHT )	tmp_ano[0] = SelectAnim( PLAYER_1, A_BOOSTR_1P ) ;
		}
		// --- プレイヤー２
		if ( playno == PLAYER_2 ){
			if ( dirinfo == D_LEFT )	tmp_ano[1] = SelectAnim( PLAYER_2, A_BOOSTL_2P ) ;
			if ( dirinfo == D_RIGHT )	tmp_ano[1] = SelectAnim( PLAYER_2, A_BOOSTR_2P ) ;
		}
		waitanim_boost[playno] = FALSE ;
	}

	switch ( dirinfo ){
		// --- 左方向
		case D_LEFT :
			if ( pos.x <= (tmp_pos.x-170.0f) ){
				spd.x = 0.0f ;
			}
			else {
				spd.x = -20.0f ;
			}
			break ;

		// --- 右方向
		case D_RIGHT :
			if ( pos.x >= (tmp_pos.x+170.0f) ){
				spd.x = 0.0f ;
			}
			else {
				spd.x = 20.0f ;
			}
			break ;
	}

	waitflg = TRUE ;
	pos.x += spd.x ;

	if ( gv_anim_playtime[playno] <= 0.0f ){
		waitanim_stay[playno] = TRUE ;
		waitanim_run[playno] = TRUE ;
		waitanim_boost[playno] = TRUE ;
		statusflg = S_STAY ;
	}

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/
	怯み																														
/+ ------------------------------------------------------------ */
int Player::ActFightened( void )
{
	direction = D_DOWN ;
	// --- アニメーション
	if ( waitanim_fightened[playno] ){
		MV1DetachAnim( hModel, gv_attachidx[playno] ) ;
		if ( playno == PLAYER_1 ){
			if ( dirinfo == D_LEFT )	tmp_ano[0] = SelectAnim( PLAYER_1, A_FIGHTENEDL_1P ) ;
			if ( dirinfo == D_RIGHT )	tmp_ano[0] = SelectAnim( PLAYER_1, A_FIGHTENEDR_1P ) ;
		}
		if ( playno == PLAYER_2 ){
			if ( dirinfo == D_LEFT )	tmp_ano[1] = SelectAnim( PLAYER_2, A_FIGHTENEDL_2P ) ;
			if ( dirinfo == D_RIGHT )	tmp_ano[1] = SelectAnim( PLAYER_2, A_FIGHTENEDR_2P ) ;
		}
		waitanim_fightened[playno] = FALSE ;

		if ( playno == PLAYER_1 && (gc_Player[PLAYER_2].GetPlayerStatus() ==  S_LATTACK) ){
			downflg += 2 ;
			down_resettim = 3.0f ;
			tmp_pos = pos ;			// --- 位置を保存
			ActKnockBack( FALSE ) ; // --- ノックバック移動
		}
		else if ( playno == PLAYER_2 && (gc_Player[PLAYER_1].GetPlayerStatus() ==  S_LATTACK) ){
			downflg += 2 ;
			down_resettim = 3.0f ;
			tmp_pos = pos ;			// --- 位置を保存
			ActKnockBack( FALSE ) ; // --- ノックバック移動
		}
		else{
			downflg += 1 ;
			down_resettim = 3.0f ;
		}
	}
	

	if ( gv_anim_playtime[playno] <= 0.0f ){
		statusflg = S_STAY ;
		waitanim_fightened[playno] = TRUE ;
		waitanim_stay[playno] = TRUE ;
		waitanim_run[playno] = TRUE ;
	}

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/
	ノックバック																														
/+ ------------------------------------------------------------ */
int Player::ActKnockBack( BOOL nextflg )
{
	direction = D_DOWN ;

	if ( dirinfo == D_LEFT ){
		spd.x = 10.0f ;			// --- スピードセット

		// --- 移動
		if ( pos.x <= tmp_pos.x+50.0f ){
			pos.x += spd.x ;
		}
		else{
			if ( nextflg ){
				statusflg = S_STAY ;
				waitanim_stay[playno] = TRUE ;
				waitanim_run[playno] = TRUE ;
			}
		}
	}
	if ( dirinfo == D_RIGHT ){
		spd.x = -10.0f ;
		// --- 移動
		if ( pos.x >= tmp_pos.x-50.0f ){
			pos.x += spd.x ;
		}
		else{
			if ( nextflg ){
				statusflg = S_STAY ;
				waitanim_stay[playno] = TRUE ;
				waitanim_run[playno] = TRUE ;
			}
		}
	}

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/
	ダウン開始																														
/+ ------------------------------------------------------------ */
int Player::ActDown( void )
{
	direction = D_DOWN ;
	// --- アニメーション
	if ( waitanim_downstart[playno] ){
		MV1DetachAnim( hModel, gv_attachidx[playno] ) ;
		if ( playno == PLAYER_1 ){
			if ( dirinfo == D_LEFT )	tmp_ano[0] = SelectAnim( PLAYER_1, A_DOWNL_1P ) ;
			if ( dirinfo == D_RIGHT )	tmp_ano[0] = SelectAnim( PLAYER_1, A_DOWNR_1P ) ;
		}
		if ( playno == PLAYER_2 ){
			if ( dirinfo == D_LEFT )	tmp_ano[1] = SelectAnim( PLAYER_2, A_DOWNL_2P ) ;
			if ( dirinfo == D_RIGHT )	tmp_ano[1] = SelectAnim( PLAYER_2, A_DOWNR_2P ) ;
		}
		waitanim_downstart[playno] = FALSE ;
		down_maxtim = 10.0f ;
	}

	spd.y -= 0.4f ;
	pos.y += spd.y ;
	if ( pos.y <= 0 )	pos.y = 0 ;

	if ( gv_Gamemode != GM_Result ){
		if ( (down_maxtim <= 0.0f) ){
			waitanim_downstart[playno] = TRUE ;
			waitanim_down[playno] = TRUE ;
			waitanim_stay[playno] = TRUE ;
			waitanim_run[playno] = TRUE ;
			statusflg = S_STAY ;
			invinflg = TRUE ;
			invintime = 5.0f ;
		}
	}

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/
	ガード																														
/+ ------------------------------------------------------------ */
int Player::ActGuard( void )
{
	// --- アニメーション
	MV1DetachAnim( hModel, gv_attachidx[playno] ) ;
	// --- プレイヤー１
	if ( playno == PLAYER_1 ){
		// --- プレイヤー１ガード時のサウンド
		MV1SetVisible( gc_Gard[PLAYER1_EFFECT].GetModelHandle(), TRUE ) ;
		if ( gc_Gard[PLAYER1_EFFECT].gardflg == TRUE ) {
			while( CheckSoundMem( gc_Sound[PLAYER_1][8].GetModelHandle() ) == 0 )
			{
				PlaySoundMem( gc_Sound[PLAYER_1][8].GetModelHandle(), DX_PLAYTYPE_BACK , FALSE) ;
				gc_Gard[PLAYER1_EFFECT].gardflg = FALSE ;
			}
		}

		if ( dirinfo == D_LEFT )	tmp_ano[0] = SelectAnim( PLAYER_1, A_GUARDL_1P ) ;
		if ( dirinfo == D_RIGHT )	tmp_ano[0] = SelectAnim( PLAYER_1, A_GUARDR_1P ) ;
	}
	// --- プレイヤー２
	if ( playno == PLAYER_2 ){
		// --- プレイヤー２ガード時のサウンド
		MV1SetVisible( gc_Gard[PLAYER2_EFFECT].GetModelHandle(), TRUE ) ;
		if ( gc_Gard[PLAYER2_EFFECT].gardflg == TRUE ) {
			while( CheckSoundMem( gc_Sound[PLAYER_2][8].GetModelHandle() ) == 0 )
			{
				PlaySoundMem( gc_Sound[PLAYER_2][8].GetModelHandle(), DX_PLAYTYPE_BACK , FALSE) ;
				gc_Gard[PLAYER2_EFFECT].gardflg = FALSE ;
			}
		}

		if ( dirinfo == D_LEFT )	tmp_ano[1] = SelectAnim( PLAYER_2, A_GUARDL_2P ) ;
		if ( dirinfo == D_RIGHT )	tmp_ano[1] = SelectAnim( PLAYER_2, A_GUARDR_2P ) ;
	}

	if ( (gv_PadStatus[playno] & PAD_R1) == 0 ){
		statusflg = S_STAY ;
		waitanim_stay[playno] = TRUE ;
		waitanim_run[playno] = TRUE ;
		gbreakflg = 0 ;
	}

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/
	大必殺																														
/+ ------------------------------------------------------------ */
int Player::ActHissatsu2( void )
{
	if ( waitanim_gdl[playno] ){
		MV1DetachAnim( hGModel, gv_attachidx[playno] ) ;

		if ( playno == PLAYER_1 ){
			if ( dirinfo == D_LEFT )
			{
				tmp_ano[0] = SelectAnim( playno, A_GREATDEATHL_1P ) ;
			}
			if ( dirinfo == D_RIGHT )
			{
				tmp_ano[0] = SelectAnim( playno, A_GREATDEATHR_1P ) ;
			}

			waitanim_gdl[playno] = FALSE ;
		}

		if ( playno == PLAYER_2 ){
			if ( dirinfo == D_LEFT )
			{
				tmp_ano[0] = SelectAnim( playno, A_GREATDEATHL_2P ) ;
			}
			if ( dirinfo == D_RIGHT )
			{
				tmp_ano[0] = SelectAnim( playno, A_GREATDEATHR_2P ) ;
			}

			waitanim_gdl[playno] = FALSE ;
		}
	}

	direction = D_DOWN ;
	if ( dirinfo == D_LEFT )
	{
		MV1SetPosition( hGModel ,VGet(200.0f, 0.0f, 0.0f)) ;
		pos.x = 200.0f ;
	}
	if ( dirinfo == D_RIGHT )
	{
		MV1SetPosition( hGModel ,VGet(-200.0f, 0.0f, 0.0f)) ;
		pos.x = -200.0f ;
	}

	if (gv_anim_totaltime[playno] - 0.1f <= gv_anim_playtime[playno])
	{
		//// --- レーザー呼び出し
		//gc_Lazer[PLAYER1_EFFECT].lflg = TRUE ;		// --- レーザーを放つ
		//ChangeVolumeSoundMem( 255 , gc_Sound[PLAYER_1][13].GetModelHandle() ) ;
		//PlaySoundMem( gc_Sound[PLAYER_1][13].GetModelHandle(),  DX_PLAYTYPE_BACK , TRUE) ;
	}

	if ( gv_anim_playtime[playno] <= 0 )
	{
		SetKillcount() ;
		if(playno == PLAYER_1)		gc_Player[PLAYER_2].SetStatus(S_DOWN) ;
		if(playno == PLAYER_2)		gc_Player[PLAYER_1].SetStatus(S_DOWN) ;

		statusflg = S_STAY ;
		gdflg = FALSE ;
		waitanim_gdl[playno] = TRUE ;
		waitanim_stay[playno] = TRUE ;
		waitanim_run[playno] = TRUE ;
	}

	return( TRUE ) ;
}

/* ------------------------------------------------------------ +/

	大必殺用フラグセット
 + ------------------------------------------------------------ +
		-引数-
			int 状態
		-返り値-
			int 整数値
/+ ------------------------------------------------------------ */
int Player::SetGdflg( BOOL flg ){

	gdflg = flg ;

	return( TRUE ) ;
}
/* ============================================================ +/
/+																+/
/+	ゲッター													+/
/+																+/
/+ ============================================================ */
// --- モデルハンドル取得
int Player::GetModelHandle( void ){

	if ( gdflg == FALSE ){
		return hModel ; 
	}else{
		return hGModel ;
	}
}

// --- モデルナンバー取得
int Player::GetModelNumber( void ){ return ModelNo ; }

// --- モデル横幅取得
float Player::GetModelWidth( void ){ return (float)ModelWidth ; }

// --- 座標取得
VECTOR Player::GetPosition( void ){ return pos ; }

// --- 速度取得
VECTOR Player::GetSpeed( void ){ return spd ; }

// --- ステータス取得
int Player::GetPlayerStatus( void ){ return statusflg ; }

// --- ＨＰ取得
int Player::GetPlayerHP( void ){ return hp ; }

// --- スタミナ取得
float Player::GetPlayerStamina( void ){ return stamina ; }

// --- キル数取得
int Player::GetPlayerKillcnt( void ){ return killcount ; }

// --- 溜めカウント取得
int Player::GetChargeCnt( void ){ return chargecnt ; }

// --- 向き情報取得
int Player::GetDirinfo( void ){ return dirinfo ; }

// --- 回転向き取得
int Player::GetDirection( void ){ return direction ; }

// --- レーン番号取得
int Player::GetLaneNo( void ){ return laneno ; }

// --- 無敵フラグ取得
int Player::GetInvinFlg( void ){ return invinflg ; }

/*- [EOF] -*/
